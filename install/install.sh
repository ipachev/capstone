#!/bin/sh

check_usage () {
    if [[ -z $1 ]]; then
        echo "Usage: $0 <install path>"
        exit -1
    fi
}

PREREQS=('python3' 'virtualenv' 'git')
check_prereqs () {
    error=false
    for prereq in ${PREREQS[@]}; do
        if hash $prereq 2>/dev/null; then
            echo "Found $prereq"
        else
            echo "ERROR: Missing $prereq. Please install it!"
            error=true
        fi
    done
    if $error; then
        exit -1
    fi
}

prepare_dir () {
    if [ -d $1 ] || mkdir $1; then
        echo "Installing into $1"
        virtualenv $1 -p python3 1>/dev/null
    else
        echo "ERROR: Unable to create directory $1"
        exit -1
    fi
}

install_into_dir () {
    launcher_dir=./capstone/install/launcher
    cd $1
    rm -rf capstone
    git clone https://bitbucket.org/ipachev/capstone.git
    echo "Installing CAPSTONE version $(cat ./capstone/install/version.txt)"
    mkdir ./capstone/logs
    ./bin/pip install -r ./capstone/install/requirements.txt 1>/dev/null
    if [ $? -eq 0 ]; then
        cp $launcher_dir/run_* .
        chmod +x ./run_*
    else
        echo "ERROR: Seems there is a problem with your python installation!"
        exit -1
    fi
}

print_success() {
    echo
    echo
    echo
    echo "INSTALL SUCCESSFUL"
    echo "To run the S3nse, run $1/run_all.sh"
}

check_usage $1
check_prereqs
prepare_dir $1
install_into_dir $1
print_success $1