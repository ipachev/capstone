#!/usr/bin/env python3

import os, sys
cdti = './capstone/src/main/cdti/CdtiApp.py'

with open(cdti) as f:
    code = compile(f.read(), cdti, 'exec')
    os.chdir('./capstone/src/main/cdti')
    sys.path.append('../..')
    exec(code, globals(), locals())
