#!/usr/bin/env python3

import sys,os
sp = './capstone/src/main/saa/SurveillanceProcessor.py'

with open(sp) as f:
    code = compile(f.read(), sp, 'exec')
    os.chdir('./capstone')
    sys.path.append('./src')
    exec(code, globals(), locals())
