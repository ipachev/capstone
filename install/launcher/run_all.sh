#!/bin/bash

function check_usage () {
    if [[ -z $1 ]]; then
        echo "Usage: $0 <simulation path>"
        exit -1
    fi
}

function stay_alive () {
	(while true ; do sleep 300 ; done )
}

function load_virtualenv () {
	cd $(dirname "$1")
	source ./bin/activate
}

function cleanup () {
        kill $server_pid
        kill $sp_pid
        kill $cdti_pid
        exit 0
}

function run_all () {
	python3 run_server.py "$1" > server.out &
	server_pid=%%
	sleep 1
	python3 run_sp.py > sp.out &
	sp_pid=%%
	sleep 3
	python3 run_cdti.py > cdti.out &
	cdti_pid=%%
}

trap cleanup INT SIGINT TERM SIGTERM QUIT

load_virtualenv $0
check_usage $1
run_all $1
stay_alive


