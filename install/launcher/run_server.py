#!/usr/bin/env python3

import sys
server = './capstone/src/main/sim/Main.py'
sys.path.append('./capstone')
sys.path.append('./capstone/src')

with open(server) as f:
    code = compile(f.read(), server, 'exec')
    exec(code, globals(), locals())
