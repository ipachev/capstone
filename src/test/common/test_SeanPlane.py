import unittest
from collections import namedtuple

from main.saa.c8.Plane import Plane

Vector = namedtuple('Vec', ['id', 'timestamp', 'type', 'latitude', 'longitude', 'altitude', 'north', 'east', 'down',
                         'azimuth', 'relative_altitude', 'range', 'elevation', 'bearing'])

def Vec(id, type, timestamp=None, latitude=None, longitude=None, altitude=None, north=None, east=None,
        down=None, azimuth=None, range=None, relative_altitude=None, elevation=None, bearing=None):
    return Vector(id, timestamp, type, latitude, longitude, altitude, north, east, down, azimuth, relative_altitude, range, elevation, bearing)


class test_Seanplane(unittest.TestCase):
    def test_ismatch(self):
        v1 = Vec('x1', 'radar')
        p1 = Plane.from_vec(v1)
        p1.add_id('tail', v1.id)

        self.assertFalse(p1.is_match('tail', 'x2'))
        self.assertTrue(p1.is_match('tail', 'x1'))
        self.assertFalse(p1.is_match('radar', 'x1'))