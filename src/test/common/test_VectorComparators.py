import unittest
import numpy as np

from main.common.VectorComparators import VectorComparator as vc

class test_VectorMath(unittest.TestCase):

    def test_delta_vector(self):
        v1 = np.array([23, .5, 18.4, 10])
        v2 = np.array([25, -3, 0, 94])

        expected = np.array([2., 3.5, 18.4, 84.])
        result = vc.delta_vector(v1, v2)

        self.assertTrue(np.array_equal(expected, result))


