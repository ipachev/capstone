import unittest
from math import radians
from main.common.EarthReferenceFrame import *


class test_EarthReferenceFrame(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_magnitude(self):
        m = magnitude(3.0, 4.0, 0.0)
        self.assertEqual(round(m, 10), 5.0)

    def test_add_vectors(self):
        a = (2.0, 4.0, -1.0)
        b = (-2.0, -4.0, 1.0)
        x, y, z = add_vectors(a, b)
        self.assertEqual(x, 0.0)
        self.assertEqual(y, 0.0)
        self.assertEqual(z, 0.0)

    def test_scale_vectors(self):
        a = (2.0, 4.0, -1.0)
        x, y, z = scale_vector(2.0, a)
        self.assertEqual(x, 4.0)
        self.assertEqual(y, 8.0)
        self.assertEqual(z, -2.0)

    def test_geocentric_radius(self):
        r_equator = geocentric_radius(0.0)
        r_polar = geocentric_radius(90, 20)
        self.assertEqual(r_equator, equatorial_radius_feet)
        self.assertEqual(r_polar, polar_radius_feet)

    def test_geo_to_xyz(self):
        x, y, z = geo_to_xyz(90.0, 45.4440, 0)
        self.assertEqual(z, polar_radius_feet)

    def test_xyz_to_geo(self):
        lat_in, long_in, alt_in = 35.45, 45.4440, 4500
        x, y, z = geo_to_xyz(lat_in, long_in, alt_in)
        lat_results, long_results, alt_results = xyz_to_geo(x, y, z)
        self.assertAlmostEqual(lat_results, lat_in)
        self.assertAlmostEqual(long_results, long_in)
        self.assertAlmostEqual(alt_results, alt_in)

    def test_north_vector(self):
        lat_in, long_in, alt_in = 0, 45.4440, 4500
        x, y, z = north_vector(lat_in, long_in)
        self.assertEqual(x, 0.0)
        self.assertEqual(y, 0.0)
        self.assertEqual(z, 1.0)

    def test_down_vector(self):
        lat_in, long_in = 45.0, -45.0
        x, y, z = down_vector(lat_in, long_in)
        self.assertAlmostEqual(x, -0.5, 5)
        self.assertAlmostEqual(y, 0.5, 5)
        self.assertAlmostEqual(z, -0.707106781187, 5)

    def test_east_vector(self):
        lat_in, long_in = 45.0, 0.0
        x, y, z = east_vector(lat_in, long_in)
        self.assertEqual(x, 0.0)
        self.assertEqual(y, 1.0)
        self.assertEqual(z, 0.0)

    def test_heading_vector(self):
        lat_in, long_in, north_velocity, east_velocity = 0.0, 0.0, 300.0, 400.0
        x, y, z = heading_vector(lat_in, long_in, north_velocity, east_velocity)
        self.assertEqual(x, 0.0)
        self.assertEqual(y, 0.8)
        self.assertEqual(z, 0.6)
