import unittest
from main.common.EarthReferenceFrame import *


class test_ReferenceFrame(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_from_earth(self):
        ox, oy, oz = geo_to_xyz(0.0001, -0.0001, 600)
        rf = ReferenceFrame(geo_to_xyz(0, 0, 0), heading_vector(0, 0, 300, 300), down_vector(0, 0))
        rox, roy, roz = rf.from_original_xyz(ox, oy, oz)
        self.assertAlmostEqual(rox, -51.0, 0)
        self.assertAlmostEqual(roy, 0.0, 0)
        self.assertAlmostEqual(roz, 600, 3)

    def test_to_earth(self):
        ox, oy, oz = geo_to_xyz(25.0001, -0.0001, 600)
        rf = ReferenceFrame(geo_to_xyz(0, 0, 0), heading_vector(0, 0, 300, 300), down_vector(0, 0))
        rox, roy, roz = rf.from_original_xyz(ox, oy, oz)
        ax, ay, az = rf.to_original_xyz(rox, roy, roz)
        self.assertAlmostEqual(ax, ox, 5)
        self.assertAlmostEqual(ay, oy, 5)
        self.assertAlmostEqual(az, oz, 5)


