import unittest
import numpy as np
from math import radians
from main.common.MatrixMath import *


class test_MatrixMath(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_rotate_x(self):
        v = vec(0, 1, 0)
        rot_x = rotate_x(radians(90))
        result = rot_x.dot(v)
        rx, ry, rz = round(result[0], 10), round(result[1], 10), round(result[2], 10)
        self.assertEqual(rx, 0.0)
        self.assertEqual(ry, 0.0)
        self.assertEqual(rz, 1.0)

    def test_rotate_y(self):
        v = vec(1, 0, 0)
        rot_y = rotate_y(radians(90))
        result = rot_y.dot(v)
        rx, ry, rz = round(result[0], 10), round(result[1], 10), round(result[2], 10)
        self.assertEqual(rx, 0.0)
        self.assertEqual(ry, 0.0)
        self.assertEqual(rz, -1.0)

    def test_rotate_z(self):
        v = vec(1, 0, 0)
        rot_z = rotate_z(radians(90))
        result = rot_z.dot(v)
        rx, ry, rz = round(result[0], 10), round(result[1], 10), round(result[2], 10)
        self.assertEqual(rx, 0.0)
        self.assertEqual(ry, 1.0)
        self.assertEqual(rz, 0.0)
