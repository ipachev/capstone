import math
import unittest

import protobuf.tcas_pb2, protobuf.radar_pb2, protobuf.ownship_pb2
import numpy as np
import nvector as nv
from main.common.MathHelpers import MathHelpers as mh
from collections import namedtuple
from main.saa.c8.Vectorizers import Tcasvectorizer, Radarvectorizer, Adsbvectorizer, Ownvectorizer
import random

wgs84 = nv.FrameE(name='WGS84')


packet = namedtuple('packet', [ 'id', 'absolute_id', 'range', 'azimuth', 'elevation', 'latitude', 'longitude', 'altitude',
                               'yaw', 'pitch', 'roll', 'north', 'east', 'down', 'timestamp'])

t_packet = namedtuple('packet', [ 'id', 'absolute_id', 'range', 'altitude', 'bearing', 'ownship_latitude',
                                  'ownship_longitude', 'ownship_altitude', 'roll', 'pitch', 'yaw'])



class test_MathHelpers(unittest.TestCase):
    def test_xyz_2_ned(self):
        lat = 23
        long = 90
        alt = 232323

        x, y, z = mh.lla_2_xyz(lat, long, alt)

        n = 100
        e = 94
        d = 23

        dx, dy, dz = mh.ned_2_vel(x, y , z, n, e, d)

        self.assertSequenceEqual((100.00000000000001, e, d), mh.vel_2_ned(x, y, z, dx, dy, dz))

    def test_closest_point_of_approach(self):

        p0 = np.array([3, 3, 0])
        p1 = np.array([3, 0, 1])
        q0 = np.array([-1, 0, 0])
        q1 = np.array([0, 0, 0])
        print(mh.closest_point_approach(p0, p1, q0, q1))




    def test_scrambling(self):
        def scramble(report):
            lat = report.latitude + random.uniform(-.0001, .0001)
            long = report.longitude + random.uniform(-.0001, .0001)
            alt = report.altitude + random.randint(-50, 50)
            north = report.north + random.uniform(-20, 20)
            east = report.east + random.uniform(-20, 20)
            down = report.down + random.uniform(-20, 20)
            range = report.range + random.uniform(-50, 50)
            azimuth = report.azimuth + random.uniform(2.0, 2.0)
            ele = report.elevation + random.uniform(-2.5, 2.5)

            return packet(report.id, report.absolute_id, range, azimuth, ele, lat, long, alt, report.yaw, report.pitch, report.roll,
                          north, east, down, report.timestamp)


        def nmi_2_feet(distance):
            return distance / 0.000164579




        # r_vectorizer = Radarvectorizer()
        # for r in [nmi_2_feet(d) for d in [1, 5, 10, 50, 100]]:
        #     own_lat, own_long, own_alt = 35.3050053, -120.66249419999997, 24250
        #     r1 = packet('1', '1', r, 0, 0, own_lat, own_long, own_alt, 0, 0, 0, 100, 100, 100, 1)
        #     r2 = packet('2', '2', r, 2, 2.5, own_lat + .001, own_long + .001, own_alt, 0, 0, 0, 100, 100, 100, 1)
        #
        #     # r2 = scramble(r2)
        #
        #     v1 = r_vectorizer.work(r1)
        #     v2 = r_vectorizer.work(r2)
        #
        #     dist = np.array([v1.x - v2.x, v1.y - v2.y, v1.z - v2.z])
        #     dist = np.sqrt(dist.dot(dist))
        #
        #     print(dist * 0.000164579)
        #
        t_vectorizer = Tcasvectorizer()


        for r in [nmi_2_feet(d) for d in [1, 5, 10, 50, 100]]:
            own_lat, own_long, own_alt = 35.3050053, -120.66249419999997, 24250
            r1 = t_packet('1', '1', r, 24250, 0, own_lat, own_long, own_alt, 0, 0, 0)
            r2 = t_packet('2', '2', r, 24250, 1, own_lat, own_long, own_alt, 0, 0, 0)

            # r2 = scramble(r2)

            v1 = t_vectorizer.work(r1)
            v2 = t_vectorizer.work(r2)

            dist = np.array([v1.x - v2.x, v1.y - v2.y, v1.z - v2.z])
            dist = np.sqrt(dist.dot(dist))

            print(dist * 0.000164579)
