import unittest
import math
import time
from main.common import VectorMath
import pyproj
from nvector import Nvector, ECEFvector, GeoPoint

import numpy as np
from measurement.measures import Distance

import nvector as nv

wgs84 = nv.FrameE(name='WGS84')


class test_VectorMath(unittest.TestCase):

    def test_calculate_dive_angle(self):
        self.assertEquals(VectorMath.calculate_dive_angle(0, 200, -100),
                          (math.pi / 2) - math.acos(1 / math.sqrt(5)))
        self.assertAlmostEquals(VectorMath.calculate_dive_angle(250, 200, -100),
                                (math.pi / 2) - math.acos(2 / (3 * math.sqrt(5))))
        self.assertAlmostEquals(VectorMath.calculate_dive_angle(0, 0, 100), -(math.pi / 2))

    def test_determine_location(self):
        print(VectorMath.determine_location(42.35, -71.06, 10000, 100, 100, 100, 2))

    def test_geo_from_radar(self):

        # http://cosinekitty.com/compass.html
        lat = -32
        long = 120
        alt = 500

        a1, a2, a3 = VectorMath.geo_from_radar(5000, -1, -10, lat, long, alt, 100, 0)
        self.assertAlmostEquals(a1, -31.986467337973863)
        self.assertAlmostEquals(a2, 119.99720026828484)
        self.assertAlmostEquals(a3, 413.3373705103133)


    def test_tcas_to_std(self):

        print(VectorMath.geo_from_tcas(60, 45, 0, 0, 0, 0, -100, -100))

        print(VectorMath.geo_from_tcas(100.0, 45, 0, 89.999 + 0.002, -120.34, 0, 100, -100))



    def test_radar(self):
        latitude = 34.301678
        longitude = -120.66038
        altitude = 12000

        own_lat = 35.301678
        own_long = -120.66038
        own_alt = 12000

        azimuth = SensorEquations.calculate_azimuth(own_lat, own_long, latitude, longitude)
        self.assertAlmostEquals(azimuth, 180)

        range = SensorEquations.calculate_range(own_lat, own_long, own_alt, latitude, longitude, altitude) * 6076.12
        self.assertLess(abs(range - 364169.9475), 500) # within 500 feet from 110km away

        elevation = SensorEquations.calculate_elevation(range, own_alt, altitude)
        self.assertLess(abs(elevation) - .5, .0001) # within margin error

        # latitude=36.300446622104886, longitude=-120.59269373675383, altitude=8796.1415446273513,
        # azimuth=179.99786376953125, relative_altitude=-3203.8584553726487, range=363981.21875,
        # elevation=-0.5043390393257141

        new_lat, new_long, new_alt = VectorMath.geo_from_radar(range, elevation, azimuth, own_lat, own_long, own_alt)

        self.assertLess(abs(new_lat - latitude), .0001) # within error range
        self.assertLess(abs(new_long - longitude), .0001) # within error range

    def test_convert(self):
        lat = 35
        lon = -120
        alt = 10000.00000

        latlong = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
        ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')

        x, y, z = pyproj.transform(latlong, ecef, lon, lat, alt, radians=False)

        print('{} {} {}'.format(x, y, z))

        p = GeoPoint(35, -120, 10000, frame=wgs84)

        x, y, z = p.to_ecef_vector().pvector

        print('{} {} {}'.format(x, y, z))


    def test_distance(self):

        dist = VectorMath.distance(35.01, -120.1, Distance(m=1000).ft,
                                      35.2, -120.01, Distance(m=4000).ft)


    def test_diff_position(self):
        x, y, z = VectorMath.diff_position(35.285988, -120.675925, 10000, 35.286036, -120.679637, 10000)
        print(VectorMath.distance(35.285988, -120.675925, 10000, 35.286036, -120.679637, 10000).ft)
        print(x.ft, y.ft, z.ft)



    def test_path(self):

        path = nv.GeoPath(wgs84.GeoPoint(35.1, -120.1, z=Distance(m=1000).ft, degrees=True),
                          wgs84.GeoPoint(35.0, -120.3, z=Distance(m=2000).ft, degrees=True))

        t0 = 10.
        t1 = 20.
        ti = 19.999

        ti_n = (ti - t0) / (t1 - t0)

        interpolated = path.interpolate(ti_n).to_geo_point()

        lat, long, z = interpolated.latitude_deg, interpolated.longitude_deg, interpolated.z

        print('{} {} {}'.format(lat, long, z))

    def test_findlocation(self):
        for i in range(50):
            VectorMath.find_location(0, 0, 0, 0, 12 + i, 0, 12.0 + i)
        t = time.time()
        print(VectorMath.find_location(0, 0, 0, 0, 900, 0, 1000.0))
        print (time.time() - t)

    def test_predictTime(self):
        test = np.array([5.208573580684739, 6.728117551692577, 3.1583306840964687, 23.469987817308038, 4.723796533973564])
        classifier = skRandomForest()

        for i in range(50):
            classifier['adsb'].predict(test.reshape(1, -1))

        t = time.time()
        print(classifier['adsb'].predict(test.reshape(1, -1)))
        print(time.time() - t)

        t = time.time()
        print(classifier['adsb'].predict_proba(test.reshape(1, -1)))
        print(time.time() - t)
