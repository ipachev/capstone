import string
import numpy as np
import nvector as nv
from main.common.Plane import Plane
from main.sim.sensor.Radar import Radar as RadarSim
from main.saa.c8.Vectorizers import Radarvectorizer as RadarSAA
from main.sim.builders import RadarConfig
from main.common.MathHelpers import MathHelpers as mh

own_lat = 35.304461
own_long = -120.671314
own_alt = 10000.0
own_north = -1
own_east = -1
own_down = 0

intruder_lat = 39.404461
intruder_long = -123.971314
intruder_alt = 10000.0


radar_sim = RadarSim(RadarConfig(), None, None)
radar_sim._get_ = lambda x: 12

radar_saa = RadarSAA()


def print_vector(vec, expected):
    left_spacing = 32
    vec_data = np.array([vec.x, vec.y, vec.z])
    expected_data = np.array(expected)
    diff = expected_data - vec_data
    error = np.sqrt(diff.dot(diff))
    fmt_num = "%16.2f"

    fmt_str = "x=%s, y=%s, z=%s" % (fmt_num, fmt_num, fmt_num)
    s = "Vectorizer (Test Value)".ljust(left_spacing)
    s += fmt_str % (vec.x, vec.y, vec.z)
    s += "\n" + "Real Value".ljust(left_spacing)
    s += fmt_str % expected
    print(s)
    s = "diff (Real Value - Test Value)".ljust(left_spacing)
    s += fmt_str % (diff[0], diff[1], diff[2])
    s += "\n" + "Distance from Real Value ".ljust(left_spacing + 2) + fmt_num % error
    print(s)


def print_packet(packet):
    print(packet)

ownship = Plane('own', own_lat, own_long, own_alt, own_north, own_east, own_down)
intruder = Plane('intruder', intruder_lat, intruder_long, intruder_alt, 1, 1, 0)
radar_report = radar_sim.create_report(0, intruder, ownship)
radar_vector = radar_saa.work(radar_report)

print_packet(radar_report)
print_vector(radar_vector, mh.lla_2_xyz(intruder_lat, intruder_long, intruder_alt))
