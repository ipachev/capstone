import pickle
import random
import unittest
import time
from multiprocessing import Pool, cpu_count, Queue, Manager

from main.saa.SurveillanceProcessor import c8
from main.saa.Worldstate import Worldstate, WorldstateContainer
from main.saa.c8.Vector import Vector
from main.saa.c8.TestHelpers import find_worldstate, score_worldstate
from main.saa.c8 import Helpers
from pprint import pprint
import warnings



class test_Worldstate(unittest.TestCase):

    def test_add_and_copy_world_state(self):
        # creates initial worldstate
        self.worldstate = Worldstate()

        # sets intruders to [[0], [1], [2], ... , [10]]
        for x in range (0, 10):
            intruder = []
            intruder.append(x)
            self.worldstate.add(intruder)

        self.assertEqual(10, len(self.worldstate.intruders))

        intruders_copy = self.worldstate.copy_intruders()

        ndx = 0
        # tests intruders copy
        for intruder in intruders_copy:
            self.assertEqual(intruder, self.worldstate.intruders[ndx])
            ndx += 1

        ws_intruders, ws_score = self.worldstate.copy()

        ndx = 0
        for intruder in ws_intruders:
            self.assertEqual(intruder, self.worldstate.intruders[ndx])
            ndx += 1

        self.assertEqual(ws_score, self.worldstate.score)
        self.worldstate.score = 1

        self.assertEqual(1, self.worldstate.score)

    def test_worlstate_container(self):
        # creates a dummy manager
        self.manager = Manager()
        self.queue = self.manager.Queue()
        # self.queue = Queue()
        self.worldstates = []

        num_ws = 50
        num_intruders = 5

        self.ws_container = WorldstateContainer(self.queue)

        self.ws_container.start()


        score = 0
        # creates 50 worldstates with scores from 0 -> 1
        for ws_ndx in range(num_ws):

            intruders = []

            # Adds 5 intruders to each worldstate
            for intruder_ndx in range (0, num_intruders):
                vec = Vector(intruder_ndx, 'adsb', absolute_id=str(intruder_ndx))
                vec2 = Vector(intruder_ndx + 1, 'tcas', absolute_id=str(intruder_ndx + 1))

                intruders.append([vec, vec2])

            ws_x = Worldstate(intruders)

            self.assertEqual(num_intruders, len(ws_x.intruders))
            score += .02

            # sets worldstate score and increments by .02
            ws_x.score = score

            self.ws_container.add(ws_x)
            self.worldstates.append(ws_x)


        while self.ws_container.get_count() != num_ws:
            pass

        self.ws_container.update()
        self.ws_container.cleanup()

        # self.assertEqual(num_ws, self.manager.Queue.size((self.ws_container.queue)))
        print("Returning: " + str(self.ws_container.first()))
        print("Expected: " + str(self.worldstates[num_ws - 1]))

        self.assertAlmostEqual((self.ws_container.first()).score, score)


    def test_iterative_worldstate_perfect_prediction(self):
        '''
        this is a long test. 33 seconds. verifies that worldstate container works (in theory).
        '''
        def predict(a, b):
            if a.absolute_id == b.absolute_id:
                return 1
            else:
                return 0

        manager = Manager()
        queue = manager.Queue()
        pool = Pool(cpu_count())
        f = open('test_ws.p', 'rb')
        reports = [p for p in list(pickle.load(f).values()) if p.absolute_id != 'OWNSHIP']

        expected = [ [ [ reports[0] ] ] ]

        for report, n in zip(reports[1:], range(len(reports) - 1)):
            ws = [[vec for vec in intruder] for intruder in expected[n]]
            found = False
            for intruder in ws:
                for vec in intruder:
                    if predict(report, vec):
                        intruder.append(report)
                        found = True
                        break
                if found:
                    break

            if not found:
                ws.append([report])

            expected.append(ws)

        for n in range(1, len(reports)):
            best = find_worldstate(reports[:n], pool=pool, queue=queue)
            expected_intruders = expected[n - 1]
            expected_score = score_worldstate(expected_intruders)
            expected_ws = Worldstate(expected_intruders, expected_score)
            self.assertEqual(best, expected_ws)

        f.close()

    def test_iterative_worldstate_actual_prediction(self):
        '''
        this is a long test. 33 seconds. verifies that worldstate container works (in theory).
        '''
        def predict(a, b):
            if a.absolute_id == b.absolute_id:
                return 1
            else:
                return 0

        manager = Manager()
        queue = manager.Queue()
        pool = Pool(cpu_count())

        f = open('test_ws.p', 'rb')

        reports = [p for p in list(pickle.load(f).values()) if p.absolute_id != 'OWNSHIP']

        expected = [ [ [ reports[0] ] ] ]

        for report, n in zip(reports[1:], range(len(reports) - 1)):
            ws = [[vec for vec in intruder] for intruder in expected[n]]
            found = False
            for intruder in ws:
                for vec in intruder:
                    if predict(report, vec):
                        intruder.append(report)
                        found = True
                        break
                if found:
                    break

            if not found:
                ws.append([report])

            expected.append(ws)



        n = 8
        table = Helpers.build_prediction_table(reports[:n], pool)

        best = Helpers.find_worldstate(reports[:n], table=table, pool=pool, queue=queue)
        expected_intruders = expected[n - 1]
        expected_score = Helpers.score_worldstate(expected_intruders, table)
        expected_ws = Worldstate(expected_intruders, expected_score)
        print(best, expected_ws)
        self.assertEqual(best, expected_ws)

        f.close()

    def test_radar_processing(self):
        reports = pickle.load(open('test_ws.p', 'rb'))
        pool = Pool(1)
        manager = Manager()
        queue = manager.Queue()

        radar_reports = [report for report in reports.values() if report.type=='radar']
        # ownship_reports = [report for report in reports.values() if report.type=='OWNSHIP']

        ownship_report = Vector('OWNSHIP', 'own', timestamp=int(time.time()), x=0.0, y=0.0,
                     z=0.0, dx=1.0, dy=0.0, dz=0.0)
        # table = Helpers.build_prediction_table(radar_reports, pool)
        # best = Helpers.find_worldstate(radar_reports, table=table, pool=pool, queue=queue)

        self.svc_x = c8()

        for report in radar_reports:
            self.svc_x.work(ownship_report)
            self.svc_x.work(report)

        ownship, intruders = self.svc_x.worldstate()

        print(len(radar_reports))
        print('\n'.join([str(plane) for plane in intruders]))





    def test_helpers(self):
        manager = Manager()
        queue = manager.Queue()
        pool = Pool(1)

        reports = [p for p in list(pickle.load(open('test_ws.p', 'rb')).values())
                   if p.absolute_id != 'OWNSHIP']

        random.shuffle(reports)
        sample = reports[:5]

        table = Helpers.build_prediction_table(sample, pool)
        ws = Helpers.find_worldstate(sample, table, pool, queue)


        print(ws)
