import unittest

import time

from multiprocessing import Queue

from multiprocessing import Lock, Pool, cpu_count, Manager

from main.saa.c8 import Helpers
from main.saa.Worldstate import Worldstate, WorldstateContainer
from main.saa.c8.Vector import Vector
from main.saa.c8.Plane import Ownship

from main.saa.c8.TheSpa import TheSpa

class test_Coral8(unittest.TestCase):
    def test_score_worldstate(self):
        ws = Worldstate()

        ownship = Ownship.from_vec(Vector('ownship', 'own', timestamp=1457055026.673389,
                                          latitude=31.95, longitude=-120, altitude=12000, north=0.01, east=0, down=0))

        intruders = []

        # 5.54674384681
        # 5.54674384681
        # 5.54674384681

        spa = TheSpa()

        rvec = Vector(absolute_id='a3', id=2, timestamp=1457055025.555798, type='radar', latitude=None, longitude=None, altitude=None, north=400.0, east=0, down=-10.0, azimuth=-81.73729471935664, relative_altitude=None, range=6108.347404865131, elevation=-6.568630029869437, bearing=None)
        massaged_rvec = spa.radar_massage(rvec, ownship)
        tvec = Vector(absolute_id='a1', id=3, timestamp=None, type='tcas', latitude=None, longitude=None, altitude=None, north=None, east=None, down=None, azimuth=None, relative_altitude=106, range=2.7859036922454834, elevation=None, bearing=-6.43722677230835)
        massaged_tvec = spa.tcas_massage(tvec, ownship)

        intruders.append([Vector(absolute_id='a1', id='a1', timestamp=1457055026.673389, type='adsb', latitude=35.35040854058298, longitude=-120.6587439860137, altitude=12590, north=150.0, east=150.0, down=-90.0, azimuth=None, relative_altitude=None, range=None, elevation=None, bearing=None),
                          massaged_tvec])

        intruders.append([massaged_rvec,
                          Vector(absolute_id='a3', id='a3', timestamp=1457055025.938894, type='adsb', latitude=35.29994590204881, longitude=-120.64038000000001, altitude=13007, north=400.0, east=0, down=-10.0, azimuth=None, relative_altitude=None, range=None, elevation=None, bearing=None)])

        intruders.append([Vector(absolute_id='a2', id='a2', timestamp=1457055025.094728, type='adsb', latitude=35.34019563303314, longitude=-120.66556740226892, altitude=11519, north=-290.0, east=-30.0, down=-10.0, azimuth=None, relative_altitude=None, range=None, elevation=None, bearing=None)])

        print(Helpers.score_worldstate(intruders, ownship))

        tmp = intruders[1]
        intruders[1] = intruders[2]
        intruders[2] = tmp
        print(Helpers.score_worldstate(intruders, ownship))
        intruders.reverse()

        start = time.time()

        print(Helpers.score_worldstate(intruders, ownship))

        print('took: {} seconds'.format(time.time() - start))

    def test_build_worldstates(self):
        spa = TheSpa()
        ownship = Ownship.from_vec(Vector('ownship', 'own', timestamp=1457055026.673389,
                                  latitude=31.95, longitude=-120, altitude=12000, north=0.01, east=0, down=0))
        rvec = Vector(absolute_id='a3', id=2, timestamp=1457055025.555798, type='radar', latitude=None, longitude=None, altitude=None, north=400.0, east=0, down=-10.0, azimuth=-81.73729471935664, relative_altitude=None, range=6108.347404865131, elevation=-6.568630029869437, bearing=None)
        massaged_rvec = spa.radar_massage(rvec, ownship)
        tvec = Vector(absolute_id='a1', id=3, timestamp=None, type='tcas', latitude=None, longitude=None, altitude=None, north=None, east=None, down=None, azimuth=None, relative_altitude=106, range=2.7859036922454834, elevation=None, bearing=-6.43722677230835)
        massaged_tvec = spa.tcas_massage(tvec, ownship)
        avec = Vector(absolute_id='a3', id='a3', timestamp=1457055025.938894, type='adsb', latitude=35.29994590204881, longitude=-120.64038000000001, altitude=13007, north=400.0, east=0, down=-10.0, azimuth=None, relative_altitude=None, range=None, elevation=None, bearing=None)

        ws_cont = Helpers.build_worldstates([massaged_rvec, massaged_tvec, avec])

        scores = list(map(lambda ws: Helpers.score_worldstate(ws, ownship), ws_cont))

        max_idx = scores.index(max(scores))

        print(ws_cont[max_idx])

        # for ws in ws_cont:
        #     print(len(ws))
        #     for intruder in ws:
        #         print("   ",list(map(lambda intruder: intruder.type, intruder)))
        #     # print([r.type for r in [w for w in list([intruder for intruder in ws])]])


    def test_Experimental(self):

        manager = Manager()
        q = manager.Queue(maxsize=20000)
        pool = Pool(cpu_count())
        spa = TheSpa()
        ownship = Ownship.from_vec(Vector('ownship', 'own', timestamp=1457055026.673389,
                                  latitude=31.95, longitude=-120, altitude=12000, north=0.01, east=0, down=0))
        rvec = Vector(absolute_id='x4', id=2, timestamp=1457055025.555798, type='radar', latitude=None, longitude=None, altitude=None, north=400.0, east=0, down=-10.0, azimuth=-81.73729471935664, relative_altitude=None, range=6108.347404865131, elevation=-6.568630029869437, bearing=None)
        massaged_rvec = spa.radar_massage(rvec, ownship)
        tvec = Vector(absolute_id='a1', id=3, timestamp=None, type='tcas', latitude=None, longitude=None, altitude=None, north=None, east=None, down=None, azimuth=None, relative_altitude=106, range=2.7859036922454834, elevation=None, bearing=-6.43722677230835)
        massaged_tvec = spa.tcas_massage(tvec, ownship)
        avec = Vector(absolute_id='a2', id='a3', timestamp=1457055025.938894, type='adsb', latitude=35.29994590204881, longitude=-120.64038000000001, altitude=13007, north=400.0, east=0, down=-10.0, azimuth=None, relative_altitude=None, range=None, elevation=None, bearing=None)
        avec2 = Vector(absolute_id='x4', id='a4', timestamp=1457055025.938894, type='adsb', latitude=35.29994590204881, longitude=-120.64038000000001, altitude=13007, north=400.0, east=0, down=-10.0, azimuth=None, relative_altitude=None, range=None, elevation=None, bearing=None)
        avec3 = Vector(absolute_id='r1', id='r1', timestamp=1457055025.938894, type='adsb', latitude=35.29994590204881, longitude=-120.64038000000001, altitude=13007, north=400.0, east=0, down=-10.0, azimuth=None, relative_altitude=None, range=None, elevation=None, bearing=None)

        reports = [massaged_rvec, massaged_tvec, avec, avec2]

        table = Helpers.build_prediction_table(reports, ownship, pool)

        ws = Helpers.find_worldstate(reports, table, pool, q)


