import math
import unittest

from measurement.measures import Distance
from nvector import GeoPoint, Nvector

import protobuf.tcas_pb2, protobuf.radar_pb2, protobuf.ownship_pb2
import numpy as np
import nvector as nv
from main.common.MathHelpers import MathHelpers
from main.common.Plane import Plane
from main.sim.sensor.Tcas import Tcas as tcas
from main.sim.sensor.Radar import Radar as radar

from main.saa.c8.Vectorizers import Tcasvectorizer, Radarvectorizer, Adsbvectorizer, Ownvectorizer

wgs84 = nv.FrameE(name='WGS84')


class test_Vectorizers(unittest.TestCase):

    def test_tcas_radar2(self):
        intruder = Plane('', 32, -120, 1000, 40, 0, 0)
        ownship = Plane('', 30, -120, 100000, 1, 0, 0)

        bearing, t_range = MathHelpers.find_bearing_and_range(intruder, ownship)



        rel_alt = tcas.calculate_altitude(intruder, ownship)


        packet = protobuf.tcas_pb2.TcasReport()
        packet.range = t_range
        packet.bearing = bearing
        packet.altitude = rel_alt
        packet.ownship_latitude = 30
        packet.ownship_longitude = -120
        packet.ownship_altitude = 100000
        packet.roll = 0
        packet.pitch = 0
        packet.yaw = math.degrees(math.atan2(0, 1))

        tcas_vectorizer = Tcasvectorizer()

        vec_tcas = tcas_vectorizer.work(packet)

        lat, long, alt = MathHelpers.xyz_2_lla(vec_tcas.x, vec_tcas.y, vec_tcas.z)
        x, y, z = MathHelpers.lla_2_xyz(32, -120, 1000)

        result = np.array([vec_tcas.x - x, vec_tcas.y - y, vec_tcas.z - z])

        mag_result = math.sqrt(result.dot(result))
        res = (vec_tcas.x, vec_tcas.y, vec_tcas.z)
        exp = (x, y, z)
        for q in range(3):
            self.assertAlmostEqual(exp[q], res[q], 1)




    def test_tcas(self):
        own = Plane('', 0, 0, 0, 0, 0, 0)
        intruder = Plane('', 1, 0, 0, 0, 0, 0)
        packet = protobuf.tcas_pb2.TcasReport()
        packet.bearing, packet.range = MathHelpers.find_bearing_and_range(intruder, own)
        packet.bearing = 0
        packet.altitude = 0
        packet.ownship_latitude = 0
        packet.ownship_longitude = 0
        packet.ownship_altitude = 0
        packet.roll = 0
        packet.pitch = 0
        packet.yaw = 0

        tcas_vectorizer = Tcasvectorizer()

        vec = tcas_vectorizer.work(packet)

        expected = MathHelpers.lla_2_xyz(1, 0, 0)
        res = (vec.x, vec.y, vec.z)
        for q in range(3):
            self.assertAlmostEqual(expected[q], res[q], 2)

    def test_radar(self):
        packet = protobuf.radar_pb2.RadarReport()
        packet.range = 6076.109345663785
        packet.azimuth = 0
        packet.elevation = 0
        packet.latitude = 0
        packet.longitude = 0
        packet.altitude = 0
        packet.roll = 0
        packet.pitch = 0
        packet.yaw = 0

        radar_vectorizer = Radarvectorizer()

        vec = radar_vectorizer.work(packet)

        self.assertSequenceEqual((20925646.32545932, 0, 6076.1093456637846), (vec.x, vec.y, vec.z))

    def test_radar2(self):
        packet = protobuf.radar_pb2.RadarReport()
        packet.range = 6076.109345663785
        packet.azimuth = 10
        packet.elevation = 40
        packet.latitude = 0
        packet.longitude = -120
        packet.altitude = 0
        packet.roll = 0
        packet.pitch = 0
        packet.yaw = 0

        radar_vectorizer = Radarvectorizer()

        vec = radar_vectorizer.work(packet)

        lat, long, alt = MathHelpers.xyz_2_lla(vec.x, vec.y, vec.z)
        #
        # print(lat, long, alt)

        self.assertSequenceEqual((-10460170.367245531, -18119163.047022868, 4583.8564260050734), (vec.x, vec.y, vec.z))

    def test_own(self):
        packet = protobuf.ownship_pb2.OwnshipReport()
        packet.ownship_latitude = 0
        packet.ownship_longitude = 0
        packet.ownship_altitude = 0
        packet.north = 0
        packet.east = 0
        packet.down = 0


        own_vectorizer = Ownvectorizer()

        vec = own_vectorizer.work(packet)


        self.assertSequenceEqual((20925646.32545932, 0, 0), (vec.x, vec.y, vec.z))

    def test_own_vel(self):
        packet = protobuf.ownship_pb2.OwnshipReport()
        packet.ownship_latitude = 0
        packet.ownship_longitude = 0
        packet.ownship_altitude = 0
        packet.north = 2050
        packet.east = 100
        packet.down = 0


        own_vectorizer = Ownvectorizer()

        vec = own_vectorizer.work(packet)


        self.assertSequenceEqual((0, 100, 2050), (vec.dx, vec.dy, vec.dz))

