import unittest
from main.saa.server import CdtiServer
from unittest.mock import Mock, call
from socket import SOL_SOCKET, SO_REUSEADDR
import time


class test_CdtiServer(unittest.TestCase):
    def create_fake_cdti_client(self, connection, address):
        self.assertIs(self.newsock, connection)
        self.assertIs(self.address, address)
        client = Mock()
        client.start = Mock()
        client.send = Mock()
        client.stop = Mock()
        self.clients.append(client)
        return client

    def setUp(self):
        self.sock = Mock()
        self.clients = []
        self.sock.setsockopt = Mock()
        self.sock.bind = Mock()
        self.sock.listen = Mock()
        self.sock.close = Mock()

        self.fake_sock_name = "fakesock"
        self.sock.getsockname = Mock(return_value=self.fake_sock_name)
        self.newsock = Mock()
        self.address = Mock()

        self.serialize = Mock()

        self.sock.accept = Mock(return_value=(self.newsock, self.address))

        self.host = ''
        self.port = 5000
        daemon = True
        name = "TestCdtiServer"
        self.log = Mock()
        self.log.info = Mock()

        self.server = CdtiServer(host=self.host, port=self.port, sock=self.sock, daemon=daemon,
                                 client_func=self.create_fake_cdti_client,
                                 serialize_func=self.serialize, name=name, log=self.log)

    def testConstructor(self):
        self.assertEqual(self.server.thread.daemon, True)
        self.assertEqual(self.server.thread.name, "TestCdtiServer")
        self.sock.setsockopt.assert_called_once_with(SOL_SOCKET, SO_REUSEADDR, 1)
        self.sock.bind.assert_called_once_with((self.host, self.port))
        self.assertIs(self.server.sock, self.sock)
        self.assertIs(self.server.log, self.log)
        self.sock.listen.assert_called_once_with(0)
        self.assertEqual(self.create_fake_cdti_client, self.server.create_new_client)

    def testStartStop(self):
        self.server.start()
        self.assertTrue(self.server.running)
        time.sleep(0.05)
        self.server.stop()
        self.assertFalse(self.server.running)

        self.sock.accept.assert_called_with()
        self.log.info.assert_has_calls([call("Starting"),
                                        call("Accepted new client %s" % str(self.address)),
                                        call("Stopping")], any_order=True)

        self.assertListEqual(self.clients, self.server.clients)

        for client in self.clients:
            client.start.assert_called_once_with()
            client.stop.assert_called_once_with()

    def testSend(self):
        test_text = "tacos"
        sent_test_text = test_text + " sent"
        self.serialize.return_value = sent_test_text

        self.testStartStop()
        self.server.running = True
        self.server.send(test_text)

        self.serialize.assert_called_once_with(test_text)
        for client in self.clients:
            client.send.assert_called_once_with(sent_test_text)






