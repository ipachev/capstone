import unittest
import time

from main.saa.simple_cluster.BadBubble import BadBubble
from main.saa.simple_cluster.Vector import Vector

class test_BadBubble(unittest.TestCase):
    """
    Test class for the Bad Bubble correlation algorithm.

    for easy lookup of whats in vector:
    def Vector(id, type, timestamp=None, latitude=None, longitude=None, altitude=None, north=None, east=None,
           down=None, azimuth=None, range=None, relative_altitude=None, elevation=None, bearing=None):
    """

    def test_sensor_to_plane(self):
        #initializes Bad bubble
        self.bb = BadBubble()

        #creates a vector of type adsb.
        vec = Vector(1, 'radar', timestamp=int(time.time()), latitude=0.0, longitude=0.0,
                      altitude=0.0, north=1.0, east=0.0, down=0.0)

        #calls work
        self.bb.work(vec)

        #tests that vector was added to the adsb/intruder map
        self.assertEqual(1, len(self.bb.radar_map))
        self.assertEqual(1, len(self.bb.intruders_map))

        #creates a vector of type adsb.
        vec = Vector(2, 'adsb', timestamp=int(time.time()), latitude=100.0, longitude=100.0,
                      altitude=0.0, north=1.0, east=0.0, down=0.0)

        #calls work
        self.bb.work(vec)

        #tests that vector was added to the adsb/intruder map
        self.assertEqual(1, len(self.bb.adsb_map))
        self.assertEqual(1, len(self.bb.radar_map))
        self.assertEqual(2, len(self.bb.intruders_map))

        # creates a vector of type radar
        # NOTE: can use lat,long,alt since transformation would be done in vectorizer
        vec = Vector(3, 'adsb', timestamp=int(time.time()), latitude=0.0, longitude=0.0,
                      altitude=0.0, north=1.0, east=0.0, down=0.0)

        #calls work
        self.bb.work(vec)

        #tests that vector was added to the radar/intruder map and not adsb map
        self.assertEqual(1, len(self.bb.radar_map))
        self.assertEqual(2, len(self.bb.adsb_map))
        #should be matched with original vector
        self.assertEqual(2, len(self.bb.intruders_map))

        first_plane = self.bb.intruders_map.get(1)
        #ensures that vec3 was matched with vec1
        self.assertTrue(3 in first_plane.ids)

        # creates ownship vector
        vec = Vector("OWNSHIP", 'own', timestamp=int(time.time()), latitude=0.0, longitude=0.0,
                     altitude=0.0, north=1.0, east=0.0, down=0.0)

        # calls work with ownship
        self.bb.work(vec)

        # appends TCA00 to beginning of ID
        vec = Vector(4, 'tcas', range=0, relative_altitude=0,
                     bearing=0)

        # calls work with incoming tcas vector
        self.bb.work(vec)

        self.assertEqual(1, len(self.bb.tcas_map))
        self.assertEqual(2, len(self.bb.intruders_map))

        first_plane = self.bb.intruders_map.get(1)

        #ensures that TCA004 was matched with vec1 and vec3
        self.assertTrue('TCA004' in first_plane.ids)




