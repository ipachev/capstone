import unittest
from main.saa.c8.C8 import Coral8
import time

class test_Coral8(unittest.TestCase):
    def test_weights(self):
        c = Coral8()

        l = c.determine_weights(['tcas', 'adsb_radar'])
        expected = [.2, .8]

        for x in range(len(l)):
            self.assertAlmostEquals(l[x], expected[x])

        l = c.determine_weights(['tcas', 'adsb_radar', 'adsb'])

        expected = [.0, .3, .7]

        for x in range(len(l)):
            self.assertAlmostEquals(l[x], expected[x])

        l = c.determine_weights(['radar_tcas', 'radar_adsb'])

        expected = [.2, .8]

        for x in range(len(l)):
            self.assertAlmostEquals(l[x], expected[x])

        l = c.determine_weights(['tcas'])

        expected = [1]

        for x in range(len(l)):
            self.assertAlmostEquals(l[x], expected[x])