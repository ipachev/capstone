import unittest
import socket as _socket
import threading as _threading
import lib.protobuf.tcas_pb2 as _tcas
import time as _time
import lib.protobuf.adsb_pb2 as _adsb
import lib.protobuf.radar_pb2 as _radar
import lib.protobuf.ownship_pb2 as _ownship
from main.saa.sensor import Tcas, AdsB, Radar, Ownship, start_sensors, await_sensors_shutdown


class ReportSender(_threading.Thread):
    def __init__(self, socket, reports, name="Report Sender"):
        super().__init__(name=name)
        self.name = name
        self.sock = socket
        self.report_list = reports

    def run(self):
        self.send_reports()
        self.sock.close()
        print("done sending")

    def send_reports(self):
        for report in self.report_list:
            self.send_report(report)
            _time.sleep(0.3)

    def send_report(self, report):
        size = report.ByteSize()
        buffer = bytearray(size + 4)
        buffer[:4] = int(size).to_bytes(4, byteorder='big', signed=False)
        buffer[4:] = report.SerializeToString()
        bytes_sent = 0
        buffer_size = len(buffer)
        while bytes_sent < buffer_size:
                bytes_sent = bytes_sent + self.sock.send(buffer[bytes_sent:])


class ReportSenderServer(_threading.Thread):
    def __init__(self, host, port, report_list):
        super().__init__(name="ReportSenderServer")
        self.host = host
        self.port = port
        self.report_list = report_list
        self.sock = _socket.socket(_socket.AF_INET, _socket.SOCK_STREAM)
        self.sock.bind((self.host, self.port))
        self.sock.listen(100)

    def accept(self):
        connection, address = self.sock.accept()
        sender = ReportSender(connection, self.report_list)
        print('CONNECT', address)
        sender.start()

    def run(self):
        self.accept()


def create_tcas_report():
    report = _tcas.TcasReport()
    report.id = int(9)
    report.range = int(400)
    report.altitude = int(25000)
    report.bearing = int(100)
    return report


class test_Sensor(unittest.TestCase):
    def setUp(self):
        tcas_reports = [create_tcas_report(), create_tcas_report()]
        self.tcas_server = ReportSenderServer("localhost", 5001, tcas_reports)
        self.tcas = Tcas()
        self.tcas.verbose = True

    def testEverything(self):
        self.tcas_server.start()

        self.tcas.start()
        for i in range(100):
            print_status = _threading.Timer(0.1*i, self.print_tcas_status)
            print_status.start()

        self.tcas_server.join()
        self.tcas.join()

    def print_tcas_status(self):
        # The Tcas constructor, sensor_status() and read() methods will be used by the S.P.
        status = self.tcas.sensor_status()
        print(status)
        if status == "active":
            print("reading tcas:" + str(self.tcas.read()))
