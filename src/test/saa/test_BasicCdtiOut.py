import unittest
from unittest.mock import Mock
from main.saa.CdtiOut import CdtiOut

class test_BasicCdtiOut(unittest.TestCase):
    def setUp(self):
        self.sock = Mock()
        self.input_svc = Mock()
        port = 5000
        self.cdti_out = CdtiOut(self.input_svc, self.sock, port)

    def testCreatePacket(self):
        ownship = Mock()
        ownship.latitude = 120
        ownship.longitude = 130
        ownship.altitude = 10000
        ownship.north = 100
        ownship.down = 5
        ownship.east = 20

        intruder = Mock()
        intruder.north = 110
        intruder.down = -5
        intruder.east = 10
        intruder.latitude = 120.01
        intruder.longitude = 130.01
        intruder.altitude = 11000
        intruder.ids = {}
        intruder.ids["adsb"] = "ZZZQQQ33"
        intruders = [intruder]

        packet = CdtiOut.create_packet(ownship, intruders)


