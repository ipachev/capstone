import unittest
from main.saa.Vectorizer import Vectorizer
from queue import Queue, Empty
from unittest.mock import patch
import time

class TestVectorizer(Vectorizer):
    def work(self, packet):
        return packet

    def run(self):
        while not self.exiting:
            try:
                packet = self.in_queue.get_nowait()
                vector = self.work(packet)
                if vector:
                    self.send_vector(vector)
            except Empty:
                pass

class test_Vectorizer(unittest.TestCase):
    def setUp(self):
        self.vec = TestVectorizer()
        self.vec.log_file = "test"
        self.input = Queue()
        self.output = Queue()

    def testSetInputOutput(self):
        self.vec.set_input(self.input)
        self.assertIs(self.vec.in_queue, self.input)
        self.vec.set_output(self.output)
        self.assertIs(self.vec.out_queue, self.output)

    def testStartStop(self):
        self.testSetInputOutput()
        self.vec.start()
        self.assertTrue(self.vec.worker.is_alive())
        self.vec.exit()
        time.sleep(0.1)
        self.assertFalse(self.vec.worker.is_alive())

    def testRun(self):
        self.testSetInputOutput()
        self.vec.start()
        self.input.put("taco")
        time.sleep(0.1)
        self.assertEqual("taco", self.output.get())
        self.vec.exit()

    def testLog(self):
        with patch("pickle.dump") as dump:
            self.vec.dump_log()
            dump.assert_called_once_with([], "test")

