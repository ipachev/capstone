import unittest
import math
from measurement.measures import Distance as dist
from src.main.sim.scenario.Simulator import Simulator as sim
from src.main.common.MathHelpers import MathHelpers as mh
from src.main.common.Plane import Plane


def deg_min_sec_2_decimal(deg, min, sec):
    return math.radians(deg + min/60 + sec/3600)


def dms_deg(deg, min, sec):
    return deg + min/60 + sec/3600


def dms_rad(deg, min, sec):
    return math.radians(dms_deg(deg, min, sec))


class test_Simulator(unittest.TestCase):
    def test_sim1(self):
        p_A_n, p_A_e, p_A_d = dist(m=707485.632), dist(m=1749005.712), dist(m=19.192)
        p_B_n, p_B_e, p_B_d = dist(m=699419.638), dist(m=1756988.763), dist(m=14.182)


        A_lat_decimal_rad = deg_min_sec_2_decimal(35, 20, 21.74126)
        A_long_decimal_rad = -deg_min_sec_2_decimal(120, 45, 40.81320)
        A_ellps_alt = p_A_d.ft

        B_lat_decimal_rad = deg_min_sec_2_decimal(35, 16, 07.10108)
        B_long_decimal_rad = -deg_min_sec_2_decimal(120, 40, 16.30251)
        B_ellps_alt = p_B_d.ft

        B_A_n, B_A_e, B_A_d = p_A_n - p_B_n, p_A_e - p_B_e, p_A_d - p_B_d

        convergence_angle = -deg_min_sec_2_decimal(1, 31, 21.4)
        q = math.atan2(B_A_e.ft, B_A_n.ft)
        p = convergence_angle + q
        d = math.sqrt(B_A_n.ft*B_A_n.ft + B_A_e.ft*B_A_e.ft + B_A_d.ft*B_A_d.ft)

        n, e, down = d*math.cos(p), d*math.sin(p), B_A_d.ft

        lat, long, alt = math.degrees(B_lat_decimal_rad), math.degrees(B_long_decimal_rad), B_ellps_alt

        test = {
            "initial-conditions": [
                {
                    "altitude": alt,
                    "tail": "OWNSHIP",
                    "latitude": lat,
                    "velocity": {
                        "east": e,
                        "north": n,
                        "down": down
                    },
                    "longitude": long
                }
            ],
            "checkpoints": []
        }

        simulator = sim(test)


        expected_lat, expected_long, expected_alt = math.degrees(A_lat_decimal_rad), math.degrees(A_long_decimal_rad), A_ellps_alt
        expected_plane = Plane('', expected_lat, expected_long, expected_alt, 0, 0, 0)
        plane = simulator.get_ownship_at_time(1)

        dx, dy, dz = mh.find_relative_pos(expected_plane, plane)
        print(dx, dy, dz)
        print(math.sqrt(dx*dx + dy*dy + dz*dz))

    def test_sim2(self):
        p_A_n, p_A_e, p_A_d = dist(m=707485.632), dist(m=1749005.712), dist(m=19.192)
        p_B_n, p_B_e, p_B_d = dist(m=699419.638), dist(m=1756988.763), dist(m=14.182)


        A_lat_decimal_rad = deg_min_sec_2_decimal(35, 20, 21.74126)
        A_long_decimal_rad = -deg_min_sec_2_decimal(120, 45, 40.81320)
        A_ellps_alt = p_A_d.ft

        B_lat_decimal_rad = deg_min_sec_2_decimal(35, 16, 07.10108)
        B_long_decimal_rad = -deg_min_sec_2_decimal(120, 40, 16.30251)
        B_ellps_alt = p_B_d.ft

        B_A_n, B_A_e, B_A_d = p_A_n - p_B_n, p_A_e - p_B_e, p_A_d - p_B_d

        convergence_angle = -dms_rad(1, 31, 21.4)
        cos_conv = math.cos(convergence_angle)
        sin_conv = math.sin(convergence_angle)

        n = cos_conv*B_A_n.ft + -sin_conv*B_A_e.ft
        e = sin_conv*B_A_n.ft + cos_conv*B_A_e.ft

        down = B_A_d.ft

        lat, long, alt = math.degrees(B_lat_decimal_rad), math.degrees(B_long_decimal_rad), B_ellps_alt

        test = {
            "initial-conditions": [
                {
                    "altitude": alt,
                    "tail": "OWNSHIP",
                    "latitude": lat,
                    "velocity": {
                        "east": e,
                        "north": n,
                        "down": down
                    },
                    "longitude": long
                }
            ],
            "checkpoints": [
                # {
                #     "tail": "OWNSHIP",
                #     "time": 20,
                #     "velocity": {
                #         "east": e/10,
                #         "north": n/10,
                #         "down": down/10
                #     }
                # }
            ]
        }

        simulator = sim(test)


        expected_lat, expected_long, expected_alt = math.degrees(A_lat_decimal_rad), math.degrees(A_long_decimal_rad), A_ellps_alt
        expected_plane = Plane('', expected_lat, expected_long, expected_alt, 0, 0, 0)
        plane = simulator.get_ownship_at_time(1)

        dx, dy, dz = mh.find_relative_pos(expected_plane, plane)
        print(dx, dy, dz)
        print(math.sqrt(dx*dx + dy*dy + dz*dz))