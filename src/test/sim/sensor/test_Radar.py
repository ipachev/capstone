import math
import unittest

import numpy as np
import nvector as nv
from main.common.MathHelpers import MathHelpers

from main.common.Plane import Plane
from main.sim.sensor.Radar import Radar

wgs84 = nv.FrameE(name='WGS84')


class test_Radar(unittest.TestCase):
    def test_math_helpers_rel_pos(self):
        pass

    def test_find_relative(self):
        ownship = Plane("OWNSHIP", 0, 0, 0, 1, 0, 0)
        intruder = Plane("INTRUDER", 1, 0, 3166.01, 1, 0, 0)
        x, y, z = MathHelpers.find_relative_pos(ownship, intruder)
        self.assertAlmostEqual(362814.9606, math.sqrt(x*x + y*y + z*z), -1)

        ownship = Plane("OWNSHIP", 0, 0, 1, 1, 0, 0)
        intruder = Plane("INTRUDER", 1, 1.000153, 6354.987, 1, 0, 0)
        # check if range is correct to within 1 foot
        x, y, z = MathHelpers.find_relative_pos(ownship, intruder)
        self.assertAlmostEqual(514908.1365, math.sqrt(x*x + y*y + z*z), -1)


    def test_calculate_fields1(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 0, 0, 0, 1, 0, 0)
        intruder = Plane("INTRUDER", 1, 0, 3166.01, 1, 0, 0)
        self.assertAlmostEqual(0, Radar.calculate_azimuth(intruder, ownship))
        self.assertAlmostEqual(0, Radar.calculate_elevation(intruder, ownship), 4)
        # check if range is correct to within 1 foot
        self.assertAlmostEqual(362814.9606, Radar.calculate_range(intruder, ownship), -1)

    def test_calculate_azimuth1(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 0, 0, 0, 1, 1, 0)
        intruder = Plane("INTRUDER", 1, .993458305025, 6354.987, 1, 0, 0)
        self.assertAlmostEqual(0, Radar.calculate_azimuth(intruder, ownship))
        # check if range is correct to within 1 foot
        self.assertAlmostEqual(513175.853, Radar.calculate_range(intruder, ownship), -1)

    def test_calculate_elevation(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 0, 0, 0, 1, 1, 0)
        intruder = Plane("INTRUDER", 1, 0, 3166.01*3, 1, 0, 0)
        self.assertAlmostEqual(1, Radar.calculate_elevation(intruder, ownship), 2)
        # check if range is correct to within 1 foot

    def test_calculate_elevation2(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 0, -120, 0, 1, 1, 0)
        intruder = Plane("INTRUDER", 1, -120, 3166.01*3, 1, 0, 0)
        self.assertAlmostEqual(1, Radar.calculate_elevation(intruder, ownship), 2)
        # check if range is correct to within 1 foot

    def test_calculate_azimuth2(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 35, -120, 3143.04, 1, 0, 0)
        intruder = Plane("INTRUDER", 35.87, -120.2, 6354.987, 1, 0, 0)
        self.assertAlmostEqual(-10.551, Radar.calculate_azimuth(intruder, ownship), 1)
        # check if range is correct to within 1 foot
        self.assertAlmostEqual(322326.115, Radar.calculate_range(intruder, ownship), -1)

    def test_calculate_elevation3(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 0, -120, 0, 1, 1, 0)
        intruder = Plane("INTRUDER", 1, -120, 3166.01*3, 1, 0, 0)
        self.assertAlmostEqual(1, Radar.calculate_elevation(intruder, ownship), 2)
        # check if range is correct to within 1 foot

    def test_calculate_elevation4(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 0, 0, 0, 100, 0, 0)

        x, y, z = MathHelpers.find_xyz(ownship)
        vel = MathHelpers.ned_2_vel(x, y, z, 100, 0, 0)
        self.assertAlmostEqual(100, vel[2])
        self.assertAlmostEqual(100, np.sqrt(np.sum(np.dot(vel, vel))))

    def test_calculate_elevation5(self):
        # Plane(self.tail, lat, long, alt, north, east, down)
        ownship = Plane("OWNSHIP", 32, -129, 0, 100, 0, 0)

        x, y, z = MathHelpers.find_xyz(ownship)
        vel = np.array(MathHelpers.ned_2_vel(x, y, z, 100, 0, 0))

        self.assertAlmostEqual(100, np.sqrt(vel.dot(vel)))




# x0 = np.array(geo_to_xyz(1,0,3166.01))
# x1 = np.array(geo_to_xyz(0,0,0))
# x2 = x1 + np.array(north_vector(0,0))
# print(x0, x1, x2)
# tmp = x2-x1
# m = magnitude(tmp[0],tmp[1],tmp[2])
# t = np.dot((x1 - x0),(x2-x1))/(m*m)
# tmp = x1 - x0
# d = magnitude(tmp[0],tmp[1],tmp[2])
# print('d=', d)
# # print(t)
# # print(sin(radians(1)) * x1[0] - x0[2])
# # print(x1[0] - sqrt(x0[0]*x0[0] + x0[2]*x0[2]))
#
# numerator = np.cross((x2 - x1), (x1 - x0))
# mag = magnitude(numerator[0], numerator[1], numerator[2])
#
# L = mag / m
#
# print(d + 2416.79)
# print(degrees(asin(L / d)))
# print(xyz_to_geo(20924925.20644802, 0, cos(radians(.5))*d))