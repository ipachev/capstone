import math
import unittest

import numpy as np
import nvector as nv
from measurement.measures import Distance

from main.common.MathHelpers import MathHelpers as MH

from main.common.Plane import Plane
from main.sim.sensor.Radar import Radar

wgs84 = nv.FrameE(name='WGS84')


def GeoPoint(lat, long, alt):
    alt_meters = Distance(ft=alt).m
    return nv.GeoPoint(lat, long, alt_meters)


class test_Radar2(unittest.TestCase):
    def build_ship_geo(self, lat, long, alt, n=1, e=0, d=0):
        return Plane('', lat, long, alt, n, e, d)

    def build_ship_xyz(self, x, y, z, dx, dy, dz):
        lat, long, alt = MH.xyz_2_lla(x, y, z)
        n, e, d = MH.vel_2_ned(x, y, z, dx, dy, dz)
        return self.build_ship_geo(lat, long, alt, n, e, d)

    def calculate_elv_azm_r(self, intruder, ownship):
        elv = Radar.calculate_elevation(intruder, ownship)
        azm = Radar.calculate_azimuth(intruder, ownship)
        r = Radar.calculate_range(intruder, ownship)
        return elv, azm, r

    def test_simple1(self):
        ownship = self.build_ship_geo(0, 0, 0, 1, 0, 0)
        x, y, z = MH.find_xyz(ownship)
        intruder = self.build_ship_xyz(x, y + 1, z + 1, -1, 0, 0)
        elv, azm, r = self.calculate_elv_azm_r(intruder, ownship)

        self.assertAlmostEqual(0, elv, 6)
        self.assertAlmostEqual(45, azm)
        self.assertAlmostEqual(math.sqrt(2), r)

    def test_simple2(self):
        ownship = self.build_ship_geo(0, 0, 0, 1, 0, 0)
        x, y, z = MH.find_xyz(ownship)
        intruder = self.build_ship_xyz(x, y - 1, z + 1, -1, 0, 0)
        elv, azm, r = self.calculate_elv_azm_r(intruder, ownship)

        self.assertAlmostEqual(0, elv, 6)
        self.assertAlmostEqual(-45, azm)
        self.assertAlmostEqual(math.sqrt(2), r)

    def test_simple3(self):
        ownship = self.build_ship_geo(0, 0, 0, 1, 0, 0)
        x, y, z = MH.find_xyz(ownship)
        intruder = self.build_ship_xyz(x, y - 1, z - 1, -1, 0, 0)
        elv, azm, r = self.calculate_elv_azm_r(intruder, ownship)

        self.assertAlmostEqual(0, elv, 6)
        self.assertAlmostEqual(-135, azm)
        self.assertAlmostEqual(math.sqrt(2), r)

    def test_simple4(self):
        ownship = self.build_ship_geo(0, 0, 0, 1, 0, 0)
        x, y, z = MH.find_xyz(ownship)
        intruder = self.build_ship_xyz(x, y + 1, z - 1, -1, 0, 0)
        elv, azm, r = self.calculate_elv_azm_r(intruder, ownship)

        self.assertAlmostEqual(0, elv, 6)
        self.assertAlmostEqual(135, azm)
        self.assertAlmostEqual(math.sqrt(2), r)

    def test_simple5(self):
        # ownship at cal poly
        ownship = self.build_ship_geo(35.303788, -120.671744, 387, 1, 0, 0)

        g_EO_E = GeoPoint(ownship.latitude, ownship.longitude, ownship.altitude)
        n_EO_E = g_EO_E.to_nvector()
        R_EO = n_EO_E.frame.R_Ee
        v_EO_E = n_EO_E.to_ecef_vector().pvector.T[0]
        p_OI_O = R_EO.dot(np.array([0, 1, -1])) + v_EO_E
        print(p_OI_O)
        intruder = self.build_ship_xyz(p_OI_O[0], p_OI_O[1], p_OI_O[2], -1, 0, 0)
        print(intruder.latitude, intruder.longitude, intruder.altitude)
        elv, azm, r = self.calculate_elv_azm_r(intruder, ownship)

        self.assertAlmostEqual(0, elv, 6)
        self.assertAlmostEqual(135, azm)
        self.assertAlmostEqual(math.sqrt(2), r)

    def test_nv_math(self):
        # This finds an ECEFVector defined using a position vector relative to the reference frame with its origin at an
        # arbitrary GeoPoint. The direction of x, y, and z are defined by the NED vector at the given GeoPoint combined
        # with yaw, pitch, and roll angles.

        # Our position vector is relative to our reference frame. This example is intentionally simple.
        # (x = 3, y = 5, z = 8)
        # Note that these units are in meters.
        p_GP0_G = np.array([3., 5., 8.])

        # We need a GeoPoint for the origin. This one is at cal poly.
        G = reference_frame_origin = GeoPoint(35.303788, -120.671744, Distance(ft=387).m)
        # We need the raw to ECEFVector that points at G in WGS86
        ecef_origin_vector = G.to_ecef_vector().pvector.T[0]

        # Next we need yaw, pitch and roll angles
        yaw, pitch, roll = 90., 0., 0.

        # We need a rotation matrix that can take points in G and calculate new x, y, and z components
        # that line up with the WGS84.
        R_EG = nv.FrameB(G, yaw, pitch, roll, degrees=True).R_EN

        # with our rotation matrix and origin vector we can now find the coordinates of our position vector in WGS84.
        # First we rotate...
        p_GP0_G_meters = MH.vector_1d_feet_to_meters(p_GP0_G)
        rotated_point = R_EG.dot(p_GP0_G_meters)
        # Then we translating.
        p_EPO_E_meters = rotated_point + ecef_origin_vector
        p_EPO_E = MH.vector_1d_meters_to_feet(p_EPO_E_meters)

        lat, long, alt = 35.303788, -120.671744, 387.
        geo_to_wgs84 = MH.geo_frame_to_wgs84_func(lat, long, alt, yaw, pitch, roll)
        wgs84_to_geo = MH.wgs84_to_geo_frame_func(lat, long, alt, yaw, pitch, roll)

        def compare_floats(expected, actually):
            assert len(expected) == len(actually)
            for q in range(len(expected)):
                self.assertAlmostEqual(expected[q], actually[q], -3)

        compare_floats(p_EPO_E, geo_to_wgs84(p_GP0_G))
        compare_floats(p_GP0_G, wgs84_to_geo(p_EPO_E))
