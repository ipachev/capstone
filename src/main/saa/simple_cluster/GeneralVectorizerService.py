from main.saa.VectorizerService import VectorizerService
from main.saa.simple_cluster.GeneralVectorizer import Adsbvectorizer, Radarvectorizer, Tcasvectorizer, Ownvectorizer

class GeneralVectorizerService(VectorizerService):
    def __init__(self):
        super().__init__()
        self.vectorizers["adsb"] = Adsbvectorizer()
        self.vectorizers["radar"] = Radarvectorizer()
        self.vectorizers["tcas"] = Tcasvectorizer()
        self.vectorizers["ownship"] = Ownvectorizer()
