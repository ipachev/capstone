import math
import time
from copy import deepcopy
from threading import Lock

from main.common.VectorMath import geo_from_tcas, find_location
from main.saa.simple_cluster.Vector import Vector
from main.saa.simple_cluster.Plane import Plane, Ownship
from main.saa.ServiceX import ServiceX


class BadBubble(ServiceX):
    """
    NOTE: This class/algorithm is mainly used for testing and is purposefully not very good at correlating.

    The BadBubble class acts as a correlation algorithm (or ServiceX) that creates "bubbles"
    around intruding aircraft locations based on sensor data inaccuracy ranges.

    After creation of the bubbles the algorithm tries to correlate the sensor information
    based on type of sensor, ID, computed longitude/latitude, and direction.

    Location of intruding plane object is prioritized in the order of: ADSB, TCAS, Radar.
    """
    def __init__(self):
        """
        Create a BadBubble. (correlator)
        Domain is defined at the top of the file
        last_size is a local var used for determining when to write.
        :return:
        """
        super().__init__()
        self.data_lock = Lock()  # Data lock so threads dont fukk up
        self.ownship = None      # Current location of ownship.
        self.workers = {'own': self.ownship_work, 'radar': self.std_work, 'tcas': self.tcas_work,
                        'adsb': self.std_work}  # calls the type of work necessary for each sensor type

        self.intruders_map = {}  # A map of the intruder planes {ID : plane}
        self.adsb_map = {}   # A map of ADS-B reports {ID : vector}
        self.radar_map = {}  # A map of radar reports {ID : vector}
        self.tcas_map = {}   # A map of TCAS reports {ID : vector}

        self.bubble_range = .1  # The radius in which sensors will be correlated. Not currently sure if this should be
        #                         referring to lat/long or nautical miles.

    def work(self, vec):
        """
        Do the appropriate work depending on which type of vector is being read.
            ADSB --> std_work
            TCAS --> tcas_work
            RADAR --> std_work

        Vector = ('Vec', ['id', 'timestamp', 'type', 'latitude', 'longitude', 'altitude', 'north', 'east', 'down',
                            'azimuth', 'relative_altitude', 'range', 'elevation', 'bearing'])

        :param vec: a vector just received from the Vectorizer
        :return: nothing
        """
        with self.data_lock:
            # does the appropiate work for the vector type
            self.workers[vec.type](vec)

    def ownship_work(self, vec):
        """
        Ownship vectors aren't logged, they are only used to update the status of ownship
        :param vec: the ownship vector
        """
        #Checks to see if there is already an ownship
        if self.ownship:
            self.ownship.update(vec)
        #creates instance of ownship if currently None
        else:
            self.ownship = Ownship.from_vec(vec)

    def print_adsb_map(self):
        """
        Prints out the current map of all adsb sensor vectors that have been reported. (should have no repeats)
        :return:
        """
        print("\nADSB Reports:")
        for id,vec in self.adsb_map.items():
            print(vec)

    def print_radar_map(self):
        """
        Prints out the current map of all radar sensor vectors that have been reported. (should have no repeats)
        :return:
        """
        print("\nRadar Reports:")
        for id, vec in self.radar_map.items():
            print(vec)

    def print_tcas_map(self):
        """
        Prints out the current map of all tcas sensor vectors that have been reported. (should have no repeats)
        :return:
        """
        print("\nTCAS Reports:")
        for id, vec in self.tcas_map.items():
            print(vec)


    def print_intruders_map(self):
        """
        Prints out the current map of all intruder planes that have been reported. (should have no repeats)
        Options:
            prints corresponding IDs per plane, print which sensors (T/F), best ID
        :return:
        """
        print("\nIntruders Reports:")
        for id, plane in self.intruders_map.items():
            print("\t" + str(plane.ids))   # prints set of IDs
            #print("ATR: " + str(plane.adsb) + "," + str(plane.tcas)  + "," + str(plane.radar) + "\n") # prints which sensors
            print("\tBest ID:\t" + str(plane.best_id) + "\n")     # prints best ID

    def sensor_to_plane(self, vec):
        """
        Sensor to plane combines the adsb_to_plane, radar_to_plane, tcas_to_plane methods.
            Not sure if better or worse.

        Steps:
            1. Runs through each plane in intruder map to see if it has matching ID
                - sets found to True and updates the plane appropirately.
            2. If not found in map checks to see if vector is within range of other intruders
                - uses appropriate comparator (tcas/radar/adsb)
            3. If not deemed an existing plane --> adds a NEW plane to intruders map

        :param vec:     Vector to transform into plane.
        :return:
        """
        found = False   # Used to check if match was found.

        # Runs through every plane in the intruders map
        for id, plane in self.intruders_map.items():
            #checks to see if there is a matching ID existing in intruders
            if plane.is_match(vec.id):
                found = True
                # updates the planes location if appropriate sensor
                if vec.type == 'adsb'\
                        or (vec.type == 'tcas' and not plane.adsb)\
                        or (vec.type == 'radar' and not plane.adsb and not plane.tcas):
                    plane.update_from_vec(vec)

        # If not found.. adds to planes.
        if not found:
                # Checks type of vector --> uses appropriate comparator
                # if no match found adds NEW plane to intruders map (same for all types of sensors)
                if vec.type == 'adsb':
                    if not self.adsb_comparator(vec):
                        plane_to_add = Plane.from_vec(vec)
                        plane_to_add.add_sensor(vec)
                        self.intruders_map.update({vec.id : plane_to_add})
                elif vec.type == 'tcas':
                    if not self.tcas_comparator(vec):
                        plane_to_add = Plane.from_vec(vec)
                        plane_to_add.add_sensor(vec)
                        self.intruders_map.update({vec.id : plane_to_add})
                elif vec.type == 'radar':
                    if not self.radar_comparator(vec):
                        plane_to_add = Plane.from_vec(vec)
                        plane_to_add.add_sensor(vec)
                        self.intruders_map.update({vec.id : plane_to_add})

    def adsb_comparator(self, vec):
        """
        Searches through all the known intruders and compares the adsb plane.
        :param vec:
        :return: True if found a plane to correlate with. False if no match.
        """
        found = False
        max_lat_diff = 0.0004
        max_long_diff = 0.0004
        max_alt_diff = 50
        error_lat_long = math.sqrt(math.pow(max_lat_diff, 2) + math.pow(max_long_diff, 2))

        for id, plane in self.intruders_map.items():
            if plane.north is not None and plane.east is not None and plane.down is not None:
                print('fuck1')
                lat, long, alt = find_location(plane.latitude, plane.longitude, plane.altitude,
                                               plane.north, plane.east, plane.down,
                                               vec.timestamp - plane.time_of_applicability)
                # plane.determine_location(vec.timestamp)
            else:
                lat, long, alt = plane.latitude, plane.longitude, plane.altitude

            alt_diff = vec.altitude - alt
            lat_diff = vec.latitude - lat
            long_diff = vec.longitude - long

            total_diff = math.sqrt(math.pow(lat_diff, 2) + math.pow(long_diff, 2))

            if error_lat_long >= total_diff and max_alt_diff >= alt_diff:
                plane.update_from_vec(vec)
                plane.add_sensor(vec)
                self.intruders_map.update({id : plane})
                found = True
        #if it got here we did not find anything matching

        if not found:
            print("NEW PLANE: No plane within range of adsb sensor: " + str(vec.id))

        return found

    def radar_comparator(self, vec):
        """
        Searches through all the known intruders and compares the radar plane.
        :param vec:
        :return: True if found a plane to correlate with. False if no match.
        """
        found = False
        max_lat_diff = 0.005
        max_long_diff = 0.005
        max_alt_diff = 50
        error_lat_long = math.sqrt(math.pow(max_lat_diff, 2) + math.pow(max_long_diff, 2))

        for id, plane in self.intruders_map.items():
            # lat, long, alt = determine_location(plane, vec.timestamp)
            if plane.north is not None and plane.east is not None and plane.down is not None:
                print('fuck2')
                lat, long, alt = find_location(plane.latitude, plane.longitude, plane.altitude,
                                               plane.north, plane.east, plane.down,
                                               vec.timestamp - plane.time_of_applicability)
                #plane.determine_location(vec.timestamp)
            else:
                lat, long, alt = plane.latitude, plane.longitude, plane.altitude

            alt_diff = vec.altitude - alt
            lat_diff = vec.latitude - lat
            long_diff = vec.longitude - long

            total_diff = math.sqrt(math.pow(lat_diff, 2) + math.pow(long_diff, 2))

            if error_lat_long >= total_diff: #and max_alt_diff >= alt_diff:
                #adds id to plane
                plane.add_id(vec.id)
                plane.radar = True
                self.intruders_map.update({id : plane})
                found = True
        #if it got here we did not find anything matching

        if not found:
            print("NEW PLANE: No plane within range of radar sensor: " + str(vec.id))

        return found

    def tcas_comparator(self, vec):
        """
        Searches through all the known intruders and compares the tcas plane.
        :param vec:
        :return: True if found a plane to correlate with. False if no match.
        """
        # TODO: On bigger simulation, TCAS breaks because it adds a plane object
        # TODO: into the intruders_map with no direction (N/E/D).
        found = False
        max_lat_diff = 0.01
        max_long_diff = 0.01
        max_alt_diff = 125
        error_lat_long = math.sqrt(math.pow(max_lat_diff, 2) + math.pow(max_long_diff, 2))

        for id, plane in self.intruders_map.items():
            if plane.north is not None and plane.east is not None and plane.down is not None:
                print('fuck3: ' + str(time.time() - 1))
                lat, long, alt = find_location(plane.latitude, plane.longitude, plane.altitude,
                                               plane.north, plane.east, plane.down,
                                               (time.time() - 1) - plane.time_of_applicability)
                #lat, long, alt = plane.determine_location(int(time.time() - 1))
            else:
                lat, long, alt = plane.latitude, plane.longitude, plane.altitude

            alt_diff = vec.altitude - alt #plane.altitude
            lat_diff = vec.latitude - lat #plane.latitude
            long_diff = vec.longitude - long #plane.longitude

            total_diff = math.sqrt(math.pow(lat_diff, 2) + math.pow(long_diff, 2))

            if error_lat_long >= total_diff: # and max_alt_diff >= math.fabs(alt_diff)
                plane.add_id(vec.id)
                plane.tcas = True
                # updates the vector if the intruder does not have adsb
                if not plane.adsb:
                    plane.update_from_vec(vec)
                self.intruders_map.update({id : plane})
                found = True
        #if it got here we did not find anything matching

        if not found:
            print("NEW PLANE: No plane within range of tcas sensor: " + str(vec.id))

        return found

    def std_work(self, vec):
        """
        Implementation using the general vectorizer.

        Steps:
            1. Adds each vector to appropriate sensor map.
            2. Transforms sensor vector to plane.
            3. Prints out sensor maps and intruders map.

        :param vec:
        :return:
        """
        #Checks which sensor the vector is coming from an adds/or updates appropriate map
        if vec.type == 'adsb':
            self.adsb_map.update({vec.id : vec})
            #self.adsb_to_plane(vec)
        elif vec.type == 'radar':
            self.radar_map.update({vec.id : vec})
            #self.radar_to_plane(vec)
        elif vec.type == 'tcas':
            self.tcas_map.update({vec.id : vec})
            #self.tcas_to_plane(vec)

        self.sensor_to_plane(vec)

        # prints out respective reports.
        self.print_adsb_map()
        self.print_radar_map()
        self.print_tcas_map()
        self.print_intruders_map()


    def tcas_work(self, vec):
        """
        TCAS vectors must be handled here rather than in Vectorizer because they need info about Ownship
        This poses a problem: TCAS vectors don't have a timestamp, and even if they did, we don't have a 'history' of
        ownship.... So we'll just process TCAS when we get them, and use whatever the most recent value of Ownship is.
        Maybe we should format the tcas vectors when we get them and process them with the rest of the vectors later?
        :param vec:
        :return:
        """
        # Ensures there is a current ownship in order to do transformation map
        if self.ownship != None:
            # does the math to convert TCAS's sensor data to include lat, long, alt
            lat, long, alt = geo_from_tcas(vec.range, vec.bearing, vec.relative_altitude,
                                self.ownship.latitude, self.ownship.longitude, self.ownship.altitude,
                                           self.ownship.north, self.ownship.east)

            #standardizes tcas vector
            vec = Vector("TCA00" + str(vec.id), vec.type, latitude=lat, longitude=long, altitude=alt)


            # calls standard work after converting the TCAS vector
            self.std_work(vec)



    def worldstate(self):
        """
        Get the current world state
        :return: a COPY of current planes
        """
        if self.ownship:
            with self.data_lock:
                #copies the intruders map into planes
                planes = deepcopy(self.intruders_map)
                ownship = deepcopy(self.ownship)

            new_planes = []

            # adds all planes from into intruder map
            for id, plane in planes.items():
                common = plane.convert_to_common()
                new_planes.append(common)

            #converts ownship to common plane
            common_own = ownship.convert_to_common()
            # print(new_planes)

            return common_own, new_planes
        else:
            return deepcopy(self.ownship), []

    def new_instance(self, values, match):
        values.append(match)
        return values

    # INDIVIDUAL SENSOR TO PLANE METHODS BELOW...
    # JUST IN CASE DECIDE TO DO SOMETHING FUCKY WITH THE TRANSFORMATIONS AND NEED INDIVIDUAL METHODS
    #
    # def radar_to_plane(self, vec):
    #     """
    #
    #     :param vec:
    #     :return:
    #     """
    #     found = False
    #     # Runs through every plane in the intruders map
    #     for id, plane in self.intruders_map.items():
    #         #checks to see if there is a matching ID existing in intruders
    #         if plane.is_match(vec.id):
    #             # updates the planes location if there is a match and sets found to true.
    #             # plane.update_from_vec(vec)
    #             found = True
    #             # If the plane doesn't have adsb or tcas --> update plane
    #             if not plane.adsb and not plane.tcas:
    #                 plane.update_from_vec(vec)
    #
    #     # If not found.. adds to planes.
    #     if not found:
    #         # If not in range of radar_comparator add the plane to intruders
    #         if not self.radar_comparator(vec):
    #             plane_to_add = Plane.from_vec(vec)
    #             plane_to_add.radar = True
    #             plane_to_add.best_id = vec.id
    #             self.intruders_map.update({vec.id : plane_to_add})
    #
    # def tcas_to_plane(self, vec):
    #     """
    #
    #     :param vec:
    #     :return:
    #     """
    #     found = False
    #     # Runs through every plane in the intruders map
    #     for id, plane in self.intruders_map.items():
    #         #checks to see if there is a matching ID existing in intruders
    #         if plane.is_match(vec.id):
    #             # updates the planes location if there is a match and sets found to true.
    #             found = True
    #             #if plane doesn't have adsb update the plane with tcas information
    #             if not plane.adsb:
    #                 plane.update_from_vec(vec)
    #
    #     # If not found.. adds to planes.
    #     if not found:
    #             if not self.tcas_comparator(vec):
    #                 plane_to_add = Plane.from_vec(vec)
    #                 plane_to_add.tcas = True
    #                 plane_to_add.best_id = vec.id
    #                 self.intruders_map.update({vec.id : plane_to_add})
    #
    # def adsb_to_plane(self, vec):
    #     """
    #     Converts an adsb vector into a plane and adds (or updates) to intruders map.
    #
    #     :param vec:
    #     :return:
    #     """
    #     found = False
    #     # Runs through every plane in the intruders map
    #     for id, plane in self.intruders_map.items():
    #         #checks to see if there is a matching ID existing in intruders
    #         if plane.is_match(vec.id):
    #             # updates the planes location if there is a match and sets found to true.
    #             plane.update_from_vec(vec)
    #             found = True
    #
    #     # If not found.. adds to planes.
    #     if not found:
    #         if not self.adsb_comparator(vec):
    #             plane_to_add = Plane.from_vec(vec)
    #             plane_to_add.adsb = True
    #             plane_to_add.best_id = vec.id
    #             self.intruders_map.update({vec.id : plane_to_add})

    # -------------------------------------------(UNUSED CODE)---------------------------------------------------