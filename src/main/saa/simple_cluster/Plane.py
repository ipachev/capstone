import numpy as np
import geopy
import math

from geopy.distance import VincentyDistance
from measurement.measures import Distance
from main.common import VectorMath
from main.common.Plane import Plane as _Plane
from main.common.VectorMath import find_location


class Plane:
    def __init__(self, id, timestamp, latitude, longitude, altitude, north, east, down):
        self.ids = set()
        self.ids.add(id)
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.north = north
        self.east = east
        self.down = down
        self.time_of_applicability = timestamp
        self.best_id = None
        self.adsb = False
        self.tcas = False
        self.radar = False

    @classmethod
    def from_vec(cls, vec):
        return cls(vec.id, vec.timestamp, vec.latitude, vec.longitude, vec.altitude, vec.north,
                   vec.east, vec.down)

    def update_from_vec(self, vec):
        self.update(vec.id, vec.latitude, vec.longitude, vec.altitude, vec.north, vec.east, vec.down, vec.timestamp)

    def add_sensor(self, vec):
        """
        Checks which type (adsb/tcas/radar) the vector is and sets that field in plane (self) to true.
        Sets best_id to the vector's id if:
            1. is adsb
            2. is tcas and doesn't have adsb
            3. is radar and no other sensors
        :param vec:
        :return:
        """
        if vec.type == 'adsb':
            self.adsb = True
            self.best_id = vec.id
        elif vec.type == 'radar':
            self.radar = True
            if not self.adsb and not self.tcas:
                self.best_id = vec.id
        elif vec.type == 'tcas':
            self.tcas = True
            if not self.adsb:
                self.best_id = vec.id


    def update(self, id, lat, long, alt, north, east, down, time):
        self.latitude = lat
        self.longitude = long
        self.altitude = alt
        self.north = north
        self.east = east
        self.down = down
        self.time_of_applicability = time
        self.ids.add(id)

    def convert_to_common(self):
        """
        Converts simple cluster plane into a common plane.
        :return: common plane.
        """
        #tail_number, latitude, longitude, altitude, north, east, down, id=None, adsb=True, radar=True, tcas=True
        plane = _Plane(self.best_id, self.latitude, self.longitude, self.altitude,
                           self.north, self.east, self.down, id = str(self.best_id), adsb = self.adsb, radar = self.radar, tcas = self.tcas)

        return plane

    def add_id(self, id):
        self.ids.add(id)

    def is_match(self, id):
        return id in self.ids

    def determine_location(self, time):
        # time_elapsed = time - self.time_of_applicability
        # vel_northeast = np.array([self.north, self.east])
        # bearing = math.atan(self.north/self.east)
        #
        # # calculate the new lat/long
        # origin = geopy.Point(self.latitude, self.longitude)
        # # get distance travelled north/east in ft, then convert to km
        # distance_ft = time_elapsed * np.sqrt(vel_northeast.dot(vel_northeast))
        # distance_km = Distance(ft=distance_ft).km
        #
        # destination = VincentyDistance(kilometers=distance_km).destination(origin, bearing)
        #
        # # calculate the new altitude in feet
        # dive_angle = VectorMath.calculate_dive_angle(self.north, self.east, self.down)
        # distance_down = self.down * time_elapsed
        # destination_alt = self.altitude + (distance_down * math.sin(dive_angle))
        print(self.best_id)
        latitude, longitude, altitude = \
            find_location(self.latitude, self.longitude, self.altitude, self.north, self.east, self.down, time)

        return latitude, longitude, altitude



class Ownship():
    def __init__(self, latitude=None, longitude=None, altitude=None, north=None, east=None, down=None):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.north = north
        self.east = east
        self.down = down

    @classmethod
    def from_vec(cls, vector):
        return cls(latitude=vector.latitude, longitude=vector.longitude, altitude=vector.altitude,
                   north=vector.north, east=vector.east, down=vector.down)

    def update(self, vec):
        self.latitude = vec.latitude
        self.longitude = vec.longitude
        self.altitude = vec.altitude
        self.north = vec.north
        self.east = vec.east
        self.down = vec.down

    def convert_to_common(self):
        """
        Converts ownship into a common plane.
        :return: common plane.
        """
        ownship = _Plane("OWNSHIP", self.latitude, self.longitude, self.altitude,
                           self.north, self.east, self.down, id = "OWNSHIP")

        return ownship