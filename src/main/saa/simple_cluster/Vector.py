from collections import namedtuple

vec = namedtuple('Vec', ['id', 'timestamp', 'type', 'latitude', 'longitude', 'altitude', 'north', 'east', 'down',
                         'azimuth', 'relative_altitude', 'range', 'elevation', 'bearing'])


def Vector(id, type, timestamp=None, latitude=None, longitude=None, altitude=None, north=None, east=None,
           down=None, azimuth=None, range=None, relative_altitude=None, elevation=None, bearing=None):
    return vec(id, timestamp, type, latitude, longitude, altitude, north, east, down, azimuth,
               relative_altitude, range, elevation, bearing)
