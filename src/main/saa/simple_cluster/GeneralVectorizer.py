from main.saa.Vectorizer import Vectorizer
from main.saa.simple_cluster.Vector import Vector
from main.common import VectorMath

class Adsbvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an ADSB protobuf packet
        :param packet adsb protobuf packet
        :return: adsb vectors
        """

        adsb_vector = Vector(packet.tail_number,
                             'adsb', timestamp=packet.timestamp, latitude=packet.latitude, longitude=packet.longitude,
                          altitude=packet.altitude, north=packet.north, east=packet.east, down=packet.down)

        return adsb_vector


class Tcasvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an tcas protobuf packet
        :param packet tcas protobuf packet
        :return: adsb vector
        """
        tcas_vector = Vector(packet.id, 'tcas', range=packet.range, relative_altitude=packet.altitude,
                          bearing=packet.bearing)

        return tcas_vector


class Radarvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an radar protobuf packet
        :param packet radar protobuf packet
        :return: adsb vector
        """

        lat, long, alt = VectorMath.geo_from_radar(packet.range, packet.elevation, packet.azimuth,
                                                   packet.latitude, packet.longitude, packet.altitude,
                                                   packet.north, packet.east)

        radar_vec = Vector(packet.id, 'radar', timestamp=packet.timestamp,
                           latitude=lat, longitude=long, altitude=alt,
                           north=packet.north, east=packet.east, down=packet.down,
                           azimuth=packet.azimuth, range=packet.range, elevation=packet.elevation)

        return radar_vec


class Ownvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an own protobuf packet
        :param packet own protobuf packet
        :return: adsb vector
        """

        own_vec = Vector("OWNSHIP", 'own',
                      timestamp=packet.timestamp, latitude=packet.ownship_latitude, longitude=packet.ownship_longitude,
                      altitude=packet.ownship_altitude, north=packet.north, east=packet.east, down=packet.down)

        return own_vec