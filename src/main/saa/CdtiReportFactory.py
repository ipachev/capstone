import protobuf.cdti_pb2 as _cdti
from main.common.VectorMath import distance, nautical_conversion_ft


def create_report(timestamp, ownship, intruders):
    report = create_cdti_report(timestamp)

    own_pos = ownship.latitude, ownship.longitude, ownship.altitude
    own_vel = ownship.north, ownship.east, ownship.down
    create_plane_report("0", position=own_pos, velocity=own_vel, cdti_plane=report.ownship)

    for plane in intruders:
        pos = plane.latitude, plane.longitude, plane.altitude
        vel = plane.north or 0.0, plane.east or 0.0, plane.down or 0.0
        dist = distance(ownship.latitude, ownship.longitude, ownship.altitude, plane.latitude, plane.longitude, plane.altitude).ft*nautical_conversion_ft
        sev = find_plane_severity(dist)
        create_plane_report(plane.id, position=pos, velocity=vel, cdti_plane=report.planes.add(), severity=sev)

    return report


def find_plane_severity(distance):
    lookup = [(4.7, _cdti.CDTIPlane.RESOLUTION), (6.4, _cdti.CDTIPlane.TRAFFIC)]
    for elm in lookup:
        max_distance, severity = elm
        if distance < max_distance:
            return severity
    return _cdti.CDTIPlane.PROXIMATE


def create_plane_report(plane_id, position, velocity, cdti_plane, severity=_cdti.CDTIPlane.PROXIMATE):
    cdti_plane.adsb_id = plane_id
    cdti_plane.position.lat, cdti_plane.position.long, cdti_plane.position.alt = position
    cdti_plane.velocity.north, cdti_plane.velocity.east, cdti_plane.velocity.down = velocity
    cdti_plane.severity = severity
    return cdti_plane


def create_cdti_report(timestamp, advisory_message=None, advisory_level=None):
    cdti_report = _cdti.CDTIReport()
    cdti_report.timestamp = int(timestamp)
    if advisory_message:
        cdti_report.advisoryMessage = advisory_message
    if advisory_level:
        cdti_report.advisoryLevel = advisory_level
    return cdti_report
