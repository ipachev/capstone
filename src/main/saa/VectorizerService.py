
from abc import ABCMeta
from main.saa.c8.Vectorizers import Adsbvectorizer, Tcasvectorizer, Radarvectorizer, Ownvectorizer


# TODO figure out how to link to the service X
class VectorizerService(metaclass=ABCMeta):
    def __init__(self):
        """
        Create the map of vectorizers
        Concrete classes are responsible for mapping protobuf
        packets to concrete Vectorizers.
        """
        self.vectorizers = {}

    def __setitem__(self, key, value):
        """
        Override in order to allow "map notation" access to vectorizers
        Appends a sensor report to a specific vectorizer
        :param key: A protobuf packet
        :param value: A sensor report
        """
        self.vectorizers[key].set_input(value)

    def start(self):
        for vect in self.vectorizers.values():
            vect.start()

    def link(self, out_svc):
        for vect in self.vectorizers.values():
            vect.set_output(out_svc.get_input())

    def exit(self):
        for vect in self.vectorizers.values():
            vect.exit()



class SBVectorizerService(VectorizerService):
    def __init__(self):
        super().__init__()
        self.vectorizers["adsb"] = Adsbvectorizer()
        self.vectorizers["radar"] = Radarvectorizer()
        self.vectorizers["tcas"] = Tcasvectorizer()
        self.vectorizers["ownship"] = Ownvectorizer()
