import itertools, os, glob, numpy as np, sys, json, re

from main.sim.scenario.Simulator import Simulator
from main.sim.builders import SimulationBuilder, TcasConfig, AdsbConfig, RadarConfig, OwnshipConfig
from main.saa.c8.Vectorizers import Tcasvectorizer, Adsbvectorizer, Radarvectorizer, Ownvectorizer
from main.saa.c8.C8 import C8
from tabulate import tabulate
from main.common.MathHelpers import MathHelpers
import itertools
from main.saa.c8.Helpers import model



class SimReport():
    def __init__(self, name='test'):
        self.out = './results/{}_results.txt'.format(name)
        self.sim_info = []
        self.c8_results = []



    def distance(self, A, B):
        return MathHelpers.calculate_range(A, B) * 0.000164579


    def speed(self, intr):
        vel = np.array([intr.north, intr.east, intr.down])

        return np.sqrt(vel.dot(vel))

    def add_sim(self, ownship, intruders, name):
        dist_2_ownship = [self.distance(ownship, intruder) for intruder in intruders]
        inter_distances = [self.distance(a, b) for a, b in itertools.combinations(intruders, 2)]
        intr_speeds = [self.speed(intr) for intr in intruders]

        self.sim_info.append((np.mean(intr_speeds), np.mean(dist_2_ownship), np.mean(inter_distances), len(intruders)))


    def add_results(self, results):
        self.c8_results.append(results)


    def report(self):
        order = sorted(range(len(self.sim_info)), key=lambda x: self.c8_results[x], reverse=True)
        header = ['num intruders', 'avg speeds (fps)', 'avg separation (nmi)', 'avg proximity (nmi)', 'tendency',
                  'accuracy']

        results = []
        accuracies = []
        tendencies = []
        for idx in order:
            info, stats = self.sim_info[idx], self.c8_results[idx]

            avg_speeds, avg_separation, avg_proximity, total_intruders = info
            accuracy, tendency = stats

            accuracies.append(accuracy)
            tendencies.append(tendency)
            results.append(['', round(total_intruders, 4), round(avg_speeds, 4),
                            round(avg_proximity, 4), round(avg_separation, 4), round(tendency, 4),
                            round(accuracy, 4)])

        results.append(['', '', '', '', '', round(np.mean(tendencies), 4), round(np.mean(accuracies), 4)])

        report = tabulate(results, header, floatfmt=".4f", tablefmt="fancy_grid")
        with open(self.out, 'w') as f:
            f.write(report)

        print(report)


def main(args):
    if len(args) == 0:
        exit('you must provide a path for test simulations!')

    path = args[0]
    files = glob.glob('{}/*.json'.format(path))
    sim_report = SimReport(model)


    for file in files:
        tcas_config = TcasConfig()
        adsb_config = AdsbConfig()
        radar_config = RadarConfig()
        ownship_config = OwnshipConfig()
        simulation = Simulator(json.load(open(file)))

        f_name = re.sub('.*/', '', file)
        f_name = re.sub('.json', '', f_name)
        f_name = re.sub('.radar', '', f_name)

        print('evaluating: {}'.format(f_name))

        tcas = tcas_config.factory(tcas_config, simulation, None)
        adsb = adsb_config.factory(adsb_config, simulation, None)
        radar = radar_config.factory(radar_config, simulation, None)
        ownship = ownship_config.factory(ownship_config, simulation, None)

        tcas_vectorizer = Tcasvectorizer()
        adsb_vectorizer = Adsbvectorizer()
        radar_vectorizer = Radarvectorizer()
        ownship_vectorizer = Ownvectorizer()

        intruders = simulation.get_all_intruder_tail_numbers()
        sim_report.add_sim(simulation.get_ownship_at_time(.1), [x for x in simulation.get_intruders_at_time(.1)], f_name)
        c8 = C8(verbose=False)

        for t in np.arange(.1, 11.1, 1):
            vectors = []

            own = simulation.get_ownship_at_time(t)
            own_time = t - np.random.uniform(.1, .9)
            own_report = ownship.create_report(own_time, own, own)
            vectors.append(ownship_vectorizer.work(own_report))

            for tail in intruders:
                time_of_report = t - np.random.uniform(.1, .9)
                intr = simulation.get_intruder_at_time(tail, time_of_report)

                if intr.tcas:
                    t_report = tcas.create_report(time_of_report, intr, own)
                    tcas.scramble_report(intr, t_report, own)
                    vectors.append(tcas_vectorizer.work(t_report))

                if intr.adsb:
                    a_report = adsb.create_report(time_of_report, intr, own)
                    adsb.scramble_report(intr, a_report, own)
                    vectors.append(adsb_vectorizer.work(a_report))

                if intr.radar:
                    r_report = radar.create_report(time_of_report, intr, own)
                    radar.scramble_report(intr, r_report, own)
                    vectors.append(radar_vectorizer.work(r_report))

            np.random.shuffle(vectors)
            for v in vectors:
                c8.work(v)

            c8.worldstate()

        r = c8.evaluate()
        sim_report.add_results(r)

    sim_report.report()





if __name__ == '__main__':
    main(sys.argv[1:])