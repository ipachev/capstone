from copy import copy
from threading import Lock

from main.saa.c8.Vector import Vector


class ReportContainer:
    def __init__(self):
        self.reports = []
        self.lock = Lock()
        #TODO: we should implement time to live

    def add(self, report):
        with self.lock:
            found = False
            for n in range(0, len(self.reports)):
                r = self.reports[n]
                if r.type == report.type and r.id == report.id:
                    temp = self.reports.pop(n)
                    self.reports.append(report)
                    found = True
                    break

            if not found:
                self.reports.append(report)

    def get(self):
        with self.lock:
            return [copy(x) for x in self.reports]

    def clear(self):
        with self.lock:
            self.reports = []

    def empty(self):
        return not self.reports





