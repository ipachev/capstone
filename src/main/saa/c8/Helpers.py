import time
from main.saa.c8.Classifiers import clf_instance
from main.saa.Worldstate import Worldstate, WorldstateContainer
import numpy as np
from main.common.VectorComparators import VectorComparator as vc
from main.saa.c8.Plane import Plane


model = 'adaboost'
classifiers, proba = clf_instance(model)


def build_prediction_table(reports, pool, verbose=False):
    if verbose:
        start = time.time()

    predict_calls = []
    for i in range(len(reports)):
        report_a = reports[i]
        for j in range(i + 1, len(reports)):
            report_b = reports[j]
            if report_a.type != report_b.type:
                predict_calls.append((report_a, report_b))

    predictions = pool.starmap(predict, predict_calls)
    table = {}
    for i in range(len(predict_calls)):
        t1, id1 = predict_calls[i][0].type, predict_calls[i][0].id
        t2, id2 = predict_calls[i][1].type, predict_calls[i][1].id
        table[(t1, id1, t2, id2)] = predictions[i]
        table[(t2, id2, t1, id1)] = predictions[i]

    if verbose:
        print('Calculating predictions, {} reports: {} seconds'.format(len(reports), time.time() - start))

    return table


def find_worldstate(reports, table, pool, queue, verbose=False):
    if verbose:
        start = time.time()
        print('Finding worldstate, {} reports'.format(len(reports)))

    ws_container = WorldstateContainer(queue)
    ws_container.add(Worldstate(intruders=[[reports[0]]], score=0))
    ws_container.start()

    time_waiting = 0
    for n in range(1, len(reports)):
        worldstates = ws_container.get()
        report = reports[n]

        results = pool.starmap(build_ws, [(ws, report, table, queue) for ws in worldstates])
        count = sum(results)

        inner_start = time.time()
        while not count == ws_container.get_count():
            pass

        time_waiting += (time.time() - inner_start)

        ws_container.update()

    ws_container.cleanup()
    best = ws_container.first()

    if verbose:
        print('Finding best worldstate took {} seconds'.format(time.time() - start))
        print(best)

    return best


def make_common_planes(ws):
    planes = []
    for intruder in ws.intruders:
        p = Plane()
        for vec in intruder:
            p.add_vector(vec)
        planes.append(p)

    return [p.update() for p in planes]


def build_ws(ws, report, table, queue):
    def is_valid(intruder, report):
        for prev_report in intruder:
            if prev_report.type == report.type:
                return False
        return True

    count = 0
    for i in range(len(ws.intruders)):
        intruder = ws.intruders[i]
        if is_valid(intruder, report):
            intruders, score = ws.copy()

            intruders[i].append(report)
            score = score_worldstate(intruders, table)

            queue.put(Worldstate(intruders, score))
            count += 1

    new_intruder = [report]
    ws.add(new_intruder)
    ws.score = score_worldstate(ws.intruders, table)
    queue.put(ws)
    count += 1
    return count


def predict(a, b):
    c_type, c_instance = vc.compare(a, b)
    score = classifiers.predict(c_type, [c_instance])[0]

    return score


def score_worldstate(intruders, table):
    score = 0
    for i in range(0, len(intruders)):
        intruder = intruders[i]
        for j in range(0, len(intruder)):
            sensor_vector = intruder[j]
            for k in range(j + 1, len(intruder)):
                sensor_vector_same_ship_other = intruder[k]
                t1, id1 = sensor_vector.type, sensor_vector.id
                t2, id2 = sensor_vector_same_ship_other.type, sensor_vector_same_ship_other.id
                # add a comment here to explain this madness
                if proba:
                    score += table[(t1, id1, t2, id2)][1]
                else:
                    score += table[(t1, id1, t2, id2)]

            for p in range(i + 1, len(intruders)):
                other_intruder = intruders[p]
                for k in range(0, len(other_intruder)):
                    sensor_vector_other_ship = other_intruder[k]
                    if sensor_vector.type != sensor_vector_other_ship.type:
                        t1, id1 = sensor_vector.type, sensor_vector.id
                        t2, id2 = sensor_vector_other_ship.type, sensor_vector_other_ship.id
                        # add a comment here to explain this madness
                        if proba:
                            score += table[(t1, id1, t2, id2)][0]
                        else:
                            score -= table[(t1, id1, t2, id2)]

    return score