from threading import Lock

from main.saa.c8.ReportContainer import ReportContainer
from main.saa.c8.Plane import Ownship
from main.saa.ServiceX import ServiceX
from main.saa.c8.Helpers import make_common_planes, build_prediction_table, find_worldstate
import numpy as np

from multiprocessing import Pool, cpu_count, Queue, Manager
from copy import deepcopy




class C8(ServiceX):
    def __init__(self, verbose=False):
        super().__init__()
        self.data_lock = Lock()
        self.ownship = None
        self.reports = ReportContainer()
        self.manager = Manager()
        self.queue = self.manager.Queue()
        self.num_workers = cpu_count()
        self.pool = Pool(self.num_workers)
        self.stats = {'accuracy': [], 'tendency': []}
        self.verbose=verbose

    def exit(self):
        self.pool.close()
        for p in range(self.num_workers):
            self.queue.put(None)
        self.pool.terminate()


    def evaluate(self):
        return np.mean(self.stats['accuracy']), np.mean(self.stats['tendency'])


    def worldstate(self):
        if self.ownship and not self.reports.empty():
            reports = None
            ownship = None

            with self.data_lock:
                ownship = deepcopy(self.ownship)
                reports = self.reports.get()
                self.reports.clear()

            # cache all predictions we will need
            table = build_prediction_table(reports, self.pool, verbose=self.verbose)

            # find the best worldstate
            ws = find_worldstate(reports, table=table, pool=self.pool, queue=self.queue, verbose=self.verbose)
            for k, v in ws.measure_accuracy().items():
                self.stats[k].append(v)

            if not ws:
                return ownship.to_lla(), []
            # convert worldstate to common planes for CDTI
            planes = make_common_planes(ws)

            return ownship.to_lla(), planes

        else:
            # no ownship
            with self.data_lock:
                return deepcopy(self.ownship.to_lla()), []

    def work(self, report):
        with self.data_lock:
            if report.type is 'own':
                self.ownship_work(report)
            elif self.ownship:
                # send the vector to the spa (fill in missing fields from TCAS and RADAR)
                self.reports.add(report)

    def ownship_work(self, vec):
        """
        Update the ownship
        """
        if self.ownship:
            self.ownship.update(vec)
        else:
            self.ownship = Ownship.from_xyz(vec)
