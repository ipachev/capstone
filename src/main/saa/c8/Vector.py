from collections import namedtuple

Vec = namedtuple('Vec', ['absolute_id', 'id', 'timestamp', 'type', 'x', 'y', 'z', 'dx', 'dy', 'dz'])

def Vector(id, type, absolute_id=None, timestamp=None, x=None, y=None, z=None, dx=None, dy=None, dz=None):
    return Vec(absolute_id, id, timestamp, type, x, y, z, dx, dy, dz)
