import pickle


class Classifier:
    def predict(self, sensor_type, delta_vec):
        raise NotImplementedError


class LinearSVC(Classifier):
    def __init__(self):
        self.clfs = pickle.load(open('./src/main/saa/c8/clfs/lin_svc.p', 'rb'))

    def predict(self, sensor_type, delta_vec):
        return self.clfs[sensor_type].decision_function(delta_vec)


class AdaBoost(Classifier):
    def __init__(self):
        self.clfs = pickle.load(open('./src/main/saa/c8/clfs/adaboost.p', 'rb'))

    def predict(self, sensor_type, delta_vec):
        return self.clfs[sensor_type].predict_proba(delta_vec)


class SVC(Classifier):
    def __init__(self):
        self.clfs = pickle.load(open('./src/main/saa/c8/clfs/SVC.p', 'rb'))

    def predict(self, sensor_type, delta_vec):
        return self.clfs[sensor_type].decision_function(delta_vec)


class SGD(Classifier):
    def __init__(self):
        self.clfs = pickle.load(open('./src/main/saa/c8/clfs/sgd.p', 'rb'))

    def predict(self, sensor_type, delta_vec):
        return self.clfs[sensor_type].decision_function(delta_vec)


class PassiveAggressive(Classifier):
    def __init__(self):
        self.clfs = pickle.load(open('./src/main/saa/c8/clfs/passive_aggressive.p', 'rb'))

    def predict(self, sensor_type, delta_vec):
        return self.clfs[sensor_type].decision_function(delta_vec)



class Ridge(Classifier):
    def __init__(self):
        self.clfs = pickle.load(open('./src/main/saa/c8/clfs/ridge.p', 'rb'))

    def predict(self, sensor_type, delta_vec):
        return self.clfs[sensor_type].decision_function(delta_vec)

models = {'lin_svc': LinearSVC, 'adaboost': AdaBoost, 'SVC': SVC, 'sgd': SGD,
          'passive_aggressive': PassiveAggressive, 'ridge': Ridge}


proba = {'lin_svc': False, 'adaboost': True, 'SVC': False, 'sgd': False,
         'passive_aggressive': False, 'ridge': False}


def clf_instance(model):
    if not model in models:
        raise Exception('{} not a valid model. options are: {}'.format(model, ', '.join(list(models.keys()))))

    return models[model](), proba[model]






