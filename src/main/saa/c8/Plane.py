from main.common.Plane import Plane as _Plane
from main.common.MathHelpers import MathHelpers as mh

class Plane:
    def __init__(self):
        self.ids = {'radar': None, 'adsb': None, 'tcas':None}
        self.absolute_id = None
        self.absolute_ids = {'radar': None, 'tcas': None, 'adsb': None}
        # Correlation tools:
        # Freshies: This is a standard vector (lat, long, alt, north, east, down) and they're FRESH
        # Vectors: These are specific vectors for each sensor (see definitions above)
        # Age: This is an indicator of how long it's been since this plane has been updated with a report
        self.freshies = {'radar': None, 'tcas': None, 'adsb': None}
        self.sensors = {'radar': [], 'tcas': [], 'adsb': []}
        self.id_prefix = {'radar': 'RDR', 'tcas': 'TCAS', 'adsb': ''}


    def add_vector(self, vec):
        self.freshies[vec.type] = vec
        self.sensors[vec.type].append(vec)

        if not self.absolute_id:
            self.absolute_id = vec.absolute_id

        if vec.type in self.ids:
            if self.ids[vec.type]:
                if self.ids[vec.type] != self.add_id_prefix(vec.id, vec.type):
                    raise ValueError('error: ids should match {}, {}'.
                                     format(self.ids[vec.type], self.add_id_prefix(vec.id, vec.type)))

        self.absolute_ids[vec.type] = vec.absolute_id
        prefixed_id = self.add_id_prefix(vec.id, vec.type)
        self.ids[vec.type] = prefixed_id

    def update(self):
        """
        We want to update location with ADSB, if possible.. then radar, lastly tcas.
        If none are available (i.e. we didn't receive a report during this interval, we increment age)
        If age reaches some arbitrary size (10?) we will remove the plane from our list of intruders
        :return:
        """

        if self.freshies['adsb']:
            report = self.freshies['adsb']
            radar = self.ids['radar']
            tcas = self.ids['tcas']

            lat, long, alt = mh.xyz_2_lla(report.x, report.y, report.z)
            n, e, d = mh.vel_2_ned(report.x, report.y, report.z, report.dx, report.dy, report.dz)

            plane = _Plane(self.absolute_id, lat, long, alt, n, e, d, id=self.ids['adsb'], adsb_id=self.ids['adsb'],
                           radar_id=radar, tcas_id=tcas, timestamp=report.timestamp)


        elif self.freshies['radar']:
            report = self.freshies['radar']
            adsb = self.ids['adsb']
            tcas = self.ids['tcas']

            lat, long, alt = mh.xyz_2_lla(report.x, report.y, report.z)
            n, e, d = mh.vel_2_ned(report.x, report.y, report.z, report.dx, report.dy, report.dz)

            plane = _Plane(self.absolute_id, lat, long, alt, n, e, d, id=self.ids['radar'], adsb_id=adsb,
                           radar_id=self.ids['radar'], tcas_id=tcas, timestamp=report.timestamp)


        else:
            report = self.freshies['tcas']
            adsb = self.ids['adsb']
            radar = self.ids['radar']

            lat, long, alt = mh.xyz_2_lla(report.x, report.y, report.z)
            n, e, d = mh.vel_2_ned(report.x, report.y, report.z, report.dx, report.dy, report.dz)

            plane = _Plane(self.absolute_id, lat, long, alt, n, e, d, adsb=adsb, radar=radar, tcas=True,
                           id=self.ids['tcas'], tcas_id=self.ids['tcas'], adsb_id=adsb, radar_id=radar,
                           timestamp=report.timestamp)

        return plane

    def add_id(self, sensor, id):
        self.ids[sensor] = id

    def get_id(self, sensor=None):
        if sensor:
            return self.ids[sensor]
        else:
            return list(self.ids.items())[0]


    def is_match(self, id, sensor=None):
        if sensor:
            prefixed_id = self.add_id_prefix(id, sensor)
            if sensor in self.ids:
                return self.ids[sensor] == prefixed_id
        else:
            for sensor in self.ids:
                prefixed_id = self.add_id_prefix(id, sensor)
                if self.ids[sensor] == prefixed_id:
                    return True
            return False

    def is_not_match(self, id, sensor):
        if sensor in self.ids:
            prefixed_id = self.add_id_prefix(id, sensor)
            return self.ids[sensor] != prefixed_id

    def has_sensor(self, sensor):
        return sensor in self.ids

    def last_vectors(self):
        result = []
        for vectors in self.sensors.values():
            if len(vectors):
                result.append(vectors[-1])
        return result

    def last_adsb(self):
        if self.sensors['adsb']:
            return self.sensors['adsb'][-1]
        return None

    def add_id_prefix(self, id, sensor):
        prefix = self.id_prefix[sensor]
        if sensor == 'adsb' or prefix in str(id):
            return id
        else:
            return '{}{}'.format(self.id_prefix[sensor], id)


class Ownship():
    def __init__(self, x, y, z, dx, dy, dz):
        self.x = x
        self.y = y
        self.z = z
        self.dx = dx
        self.dy = dy
        self.dz = dz


    def to_lla(self):
        lat, long, alt = mh.xyz_2_lla(self.x, self.y, self.z)
        n, e, d = mh.vel_2_ned(self.x, self.y, self.z, self.dx, self.dy, self.dz)

        return _Plane('OWNSHIP', lat, long, alt, n, e, d)

    @classmethod
    def from_xyz(cls, report):
        return cls(report.x, report.y, report.z, report.dx, report.dy, report.dz)

    def update(self, vec):
        self.x = vec.x
        self.y = vec.y
        self.z = vec.z
        self.dx = vec.dx
        self.dy = vec.dy
        self.dz = vec.dz


