from measurement.measures import Distance
from nvector import GeoPoint, FrameB, n_E2R_EN, Pvector, FrameL
from math import sin, radians, cos, sqrt
from main.saa.Vectorizer import Vectorizer
from main.saa.c8.Vector import Vector
from main.common import VectorMath
from main.common.MathHelpers import MathHelpers as mh
from main.common.Plane import Plane
import numpy as np
path = './vectorlogs'

ft_per_nautical_mi = 0.000164579

class Adsbvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an ADSB protobuf packet
        :param packet adsb protobuf packet
        :return: adsb vectors
        """
        x, y, z = mh.lla_2_xyz(latitude=packet.latitude, longitude=packet.longitude, altitude=packet.altitude)

        dx, dy, dz = mh.ned_2_vel(x, y, z, packet.north, packet.east, packet.down)
        adsb_vector = Vector(packet.tail_number, 'adsb', absolute_id=packet.absolute_id,
                             timestamp=packet.timestamp, x=x, y=y, z=z, dx=dx, dy=dy, dz=dz)



        return adsb_vector


class Tcasvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an tcas protobuf packet
        :param packet tcas protobuf packet
        :return: adsb vector
        """
        m_per_nautical_mile = 1852
        range = packet.range * m_per_nautical_mile
        bearing = packet.bearing

        latitude, longitude, altitude = packet.ownship_latitude, packet.ownship_longitude, packet.ownship_altitude
        p_EO_E = GeoPoint(latitude, longitude, mh.feet_2_meters(altitude)[0] * -1, degrees=True)

        frame = FrameB(p_EO_E, packet.yaw + packet.bearing, packet.pitch, packet.roll, degrees=True)
        R_EO = frame.R_EN
        p_ON_O = np.array([range, 0, 0]).T
        p_EN_E = np.array(np.dot(R_EO, p_ON_O)).flatten() + p_EO_E.to_ecef_vector().pvector.T
        p_EN_E = p_EN_E[0]
        lat, long, alt = mh.xyz_2_lla(p_EN_E[0], p_EN_E[1], p_EN_E[2])
        x, y, z = mh.lla_2_xyz(lat, long, packet.altitude + packet.ownship_altitude)

        tcas_vector = Vector('TCAS{}'.format(packet.id), 'tcas', absolute_id=packet.absolute_id, x=x, y=y, z=z,
                             dx=np.nan, dy=np.nan, dz=np.nan, timestamp=np.nan)

        return tcas_vector


class Radarvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an radar protobuf packet
        :param packet radar protobuf packet
        :return: adsb vector
        """

        range, = mh.feet_2_meters(packet.range)
        azimuth = radians(packet.azimuth)
        elevation = radians(packet.elevation)

        latitude, longitude, altitude = packet.latitude, packet.longitude, packet.altitude
        p_EO_E = GeoPoint(latitude, longitude, mh.feet_2_meters(altitude)[0] * -1, degrees=True)

        frame = FrameB(p_EO_E, packet.yaw, packet.pitch, packet.roll, degrees=True)

        d = range * cos(elevation)

        x_OI_O = d * cos(azimuth)
        y_OI_O = d * sin(azimuth)
        z_OI_O = range * sin(-elevation)
        p_OI_O = np.array([x_OI_O, y_OI_O, z_OI_O]).T

        R_EO = frame.R_EN

        p_EI_E = np.array(np.dot(R_EO, p_OI_O)).flatten() + p_EO_E.to_ecef_vector().pvector.T
        x, y, z = mh.meters_2_feet(*p_EI_E.flatten())
        dx, dy, dz = mh.ned_2_vel(x, y, z, packet.north, packet.east, packet.down)
        radar_vec = Vector('RDR{}'.format(packet.id), 'radar', absolute_id=packet.absolute_id,
                           timestamp=packet.timestamp, x=x, y=y, z=z, dx=dx, dy=dy, dz=dz)

        # print("RADARvectorizer work()")

        return radar_vec

class Ownvectorizer(Vectorizer):
    def __init__(self):
        super().__init__()

    def work(self, packet):
        """
        Build a vector from an own protobuf packet
        :param packet own protobuf packet
        :return: adsb vector
        """

        x, y, z = mh.lla_2_xyz(packet.ownship_latitude, packet.ownship_longitude,
                               mh.feet_2_meters(packet.ownship_latitude)[0])

        dx, dy, dz = mh.ned_2_vel(x, y, z, packet.north, packet.east, packet.down)

        own_vec = Vector("OWNSHIP", 'own', absolute_id="OWNSHIP",
                      timestamp=packet.timestamp, x=x, y=y, z=z, dx=dx, dy=dy, dz=dz)



        return own_vec