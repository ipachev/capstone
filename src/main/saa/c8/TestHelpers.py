import time

from main.saa.Worldstate import Worldstate, WorldstateContainer
from main.saa.c8.Classifiers import clf_instance


classifiers = clf_instance('lin_svc')


def find_worldstate(reports, pool, queue, verbose=False):

    if verbose:
        start = time.time()
        print('Finding worldstate, {} reports'.format(len(reports)))

    ws_container = WorldstateContainer(queue)
    ws_container.add(Worldstate(intruders=[[reports[0]]], score=0))
    ws_container.start()

    for n in range(1, len(reports)):
        worldstates = ws_container.get()
        report = reports[n]

        results = pool.starmap(build_ws, [(ws, report, queue) for ws in worldstates])
        count = sum(results)
        while not count == ws_container.get_count():
            pass

        ws_container.update()

    ws_container.cleanup()
    best = ws_container.first()

    if verbose:
        print('Finding best worldstate took {} seconds'.format(time.time() - start))
        print(best)

    return best


def build_ws(ws, report, queue):
    def is_valid(intruder, report):
        for prev_report in intruder:
            if prev_report.type == report.type:
                return False
        return True

    count = 0
    for i in range(len(ws.intruders)):
        intruder = ws.intruders[i]
        if is_valid(intruder, report):
            intruders, score = ws.copy()

            intruders[i].append(report)
            score = score_worldstate(intruders)

            queue.put(Worldstate(intruders, score))
            count += 1

    new_intruder = [report]
    ws.add(new_intruder)
    ws.score = score_worldstate(ws.intruders)
    queue.put(ws)
    count += 1
    return count


def predict(a, b):
    if a.absolute_id == b.absolute_id:
        return 1
    else:
        return 0


def score_worldstate(intruders):
    score = 0
    for i in range(0, len(intruders)):
        intruder = intruders[i]
        for j in range(0, len(intruder)):
            sensor_vector = intruder[j]
            for k in range(j + 1, len(intruder)):
                sensor_vector_same_ship_other = intruder[k]
                score += predict(sensor_vector, sensor_vector_same_ship_other)

            for p in range(i + 1, len(intruders)):
                other_intruder = intruders[p]
                for k in range(0, len(other_intruder)):
                    sensor_vector_other_ship = other_intruder[k]
                    if sensor_vector.type != sensor_vector_other_ship.type:
                        score += (1 - predict(sensor_vector, sensor_vector_other_ship))

    return score
