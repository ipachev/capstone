import random
import sys
import json

import itertools, os, glob, numpy as np

from main.sim.scenario.Simulator import Simulator
from main.sim.builders import SimulationBuilder, TcasConfig, AdsbConfig, RadarConfig, OwnshipConfig
from main.common.VectorComparators import VectorComparator
from main.saa.c8.Vectorizers import Tcasvectorizer, Adsbvectorizer, Radarvectorizer, Ownvectorizer
from main.db.DB import drop, insert_many

tcas_fields = ['x', 'y', 'z', 'delta_pos', 'match']
std_fields = ['x', 'y', 'z', 'dx', 'dy', 'dz', 'timestamp', 'delta_pos', 'delta_speed', 'delta_vel',
               'angle_between_vel', 'match']

fields = {'adsb_radar': std_fields, 'adsb_tcas': tcas_fields, 'radar_tcas': tcas_fields}


def parse_vec(type, vec):
    entry = {k: v for k, v in zip(fields[type], vec)}
    entry['type'] = type

    return entry


def generate_training_instances(path, file, duration):
    tcas_config = TcasConfig()
    adsb_config = AdsbConfig()
    radar_config = RadarConfig()
    simulation = Simulator(json.load(open(os.path.join(path, file))))

    print('simulator loaded: {}'.format(file))

    tcas = tcas_config.factory(tcas_config, simulation, None)
    adsb = adsb_config.factory(adsb_config, simulation, None)
    radar = radar_config.factory(radar_config, simulation, None)

    tcas_vectorizer = Tcasvectorizer()
    adsb_vectorizer = Adsbvectorizer()
    radar_vectorizer = Radarvectorizer()


    intruders = simulation.get_all_intruder_tail_numbers()
    data = []
    for t in np.arange(.1, duration, 1):
        own = simulation.get_ownship_at_time(t)
        for j in range(len(intruders)):
            intr_time = t - random.uniform(.1, .9)
            tail = intruders[j]
            slf = simulation.get_intruder_at_time(tail, intr_time)
            tcasReport = tcas.create_report(intr_time, slf, own)
            tcas.scramble_report(slf, tcasReport, own)
            tcasVector = tcas_vectorizer.work(tcasReport)
            adsbReport = adsb.create_report(intr_time, slf, own)
            adsb.scramble_report(slf, adsbReport, own)
            adsbVector = adsb_vectorizer.work(adsbReport)
            radarReport = radar.create_report(intr_time, slf, own)
            radar.scramble_report(slf, radarReport, own)
            radarVector = radar_vectorizer.work(radarReport)

            matches = itertools.combinations([radarVector, adsbVector, tcasVector], 2)

            for v1, v2 in matches:
                vec_type, vec = VectorComparator.compare(v1, v2)
                entry = parse_vec(vec_type, vec)
                entry['match'] = 1
                data.append(entry)

            intruder_tail = random.choice(list(itertools.chain([intruders[n] for n in range(j)], [intruders[n] for n in range(j + 1, len(intruders))])))
            intr_time = t - random.uniform(.1, .9)
            intruder = simulation.get_intruder_at_time(intruder_tail, intr_time)
            intruder_tcas_r = tcas.create_report(intr_time, intruder, own)
            intruder_tcas_v = tcas_vectorizer.work(tcas.scramble_report(intruder, intruder_tcas_r, own))

            intruder_adsb_r = adsb.create_report(intr_time, intruder, own)
            intruder_adsb_v = adsb_vectorizer.work(adsb.scramble_report(intruder, intruder_adsb_r, own))

            intruder_radar_r = radar.create_report(intr_time, intruder, own)
            intruder_radar_v = radar_vectorizer.work(radar.scramble_report(intruder, intruder_radar_r, own))

            not_matches = [v for v in list(itertools.product([intruder_tcas_v, intruder_adsb_v, intruder_radar_v],
                                                 [tcasVector, radarVector, adsbVector])) if v[0].type != v[1].type]

            for v1, v2 in not_matches:
                vec_type, vec = VectorComparator.compare(v1, v2)
                entry = parse_vec(vec_type, vec)
                entry['match'] = 0
                data.append(entry)

        t += 1
    return data


def main(argv):
    if (len(argv) != 3):
        print("You must specify a path to simulation files!")
        print("You must specify the number of instances!")
        print("You must specify whether or not to delete first")
        print("Usage: %s <simulation file> <number of instances> <delete first>" % sys.argv[0])
        sys.exit(1)

    if argv[2] == 'true':
        delete_first = True
    elif argv[2] == 'false':
        delete_first = False
    else:
        sys.exit('you must specify whether collection should be deleted first')


    path = argv[0]
    duration = int(argv[1])
    files = glob.glob('{}/*.json'.format(path), recursive=True)


    data = []
    for file in files[:5]:
        entries = generate_training_instances(path, file, duration)
        data.extend(entries)

    if delete_first:
        drop('scrambled_temp')
    insert_many(data, 'scrambled_temp')


    print('{} training instances added to scrambled_data'.format(len(entries)))

if __name__ == '__main__':
    main(sys.argv[1:])

