import logging
import pickle
from abc import ABCMeta, abstractmethod
from threading import Thread, Lock
from queue import Queue
from main.common.Plane import Plane


# All the correlation is done in the work method
class ServiceX(metaclass=ABCMeta):
    def __init__(self):
        self.log = logging.getLogger("ServiceX")
        self.worker = Thread(target=self.run, daemon=True)
        self.in_queue = Queue()

    def start(self):
        self.log.info("Starting")
        self.worker.start()

    def run(self):
        for vec in iter(self.in_queue.get, None):
            self.work(vec)

    def get_input(self):
        return self.in_queue

    @abstractmethod
    def work(self, vec):
        """
        Process a vector and update the current world state (list of planes)
        :param vec: the vector to process
        """
        pass

    @abstractmethod
    def worldstate(self):
        """
        Get the current world state
        :return: a COPY of current planes
        """
        pass

    # Exit the thread this service is running on
    def exit(self):
        self.in_queue.put(None)
        self.worker.join()


# Implement concrete Service X's here

class PassThrough(ServiceX):
    def __init__(self, limit=None):
        super().__init__()
        self.data_lock = Lock()
        self.intruders = dict()


        self.limit = limit

    def work(self, vec):
        """
        Process a vector and update the current world state (list of planes)
        :param vec:
        """
        with self.data_lock:
            self.intruders[(vec.type, vec.id)] = vec


        if self.limit:
            if self.limit == len(list(self.intruders.items())):
                with open('test_ws.p', 'wb') as f:
                    pickle.dump(self.intruders, f)


    def worldstate(self):
        """
        Get the current world state
        :return: a COPY of current planes
        """
        intruder_report = []
        strs = []
        with self.data_lock:
            for intruder in [x for x in self.intruders.values() if x.id != 'OWNSHIP']:
                st = "" + intruder.absolute_id + intruder.type + " "+str(intruder.x) + " "+str(intruder.y) + " "+str(intruder.z) + " "+str(intruder.timestamp)
                strs.append(st)
                plane = Plane.from_vec(intruder.absolute_id, intruder.absolute_id, intruder.type, intruder.x, intruder.y,
                                       intruder.z, intruder.dx, intruder.dy, intruder.dz, intruder.timestamp)
                intruder_report.append(plane)

            ov = self.intruders[('own', 'OWNSHIP')]
            own = Plane.from_vec(ov.id, ov.id, ov.type, ov.x, ov.y, ov.z, ov.dx, ov.dy, ov.dz, ov.timestamp)
            self.log.debug("building worldstate")
            self.log.debug('ownship %d %d %d' % (ov.x, ov.y, ov.z))
            strs.sort()
            for s in strs:
                self.log.debug(s)
            self.log.debug("done building worldstate\n")
            self.intruders.clear()
            return own, intruder_report
