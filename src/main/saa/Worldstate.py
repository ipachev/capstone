from heapq import heappush, heappop, nlargest
from threading import Lock, Thread
import numpy as np


class WorldstateContainer:
    """
    Worldstate Container is a fixed size priority queue.
    It is sorted in descending order by worldstate score
    """
    def __init__(self, queue, max_size=100):
        self.worldstates = []
        self.queue = queue
        self.temp_heap = []
        self.max_size = max_size
        self.exiting = False
        self.data_lock = Lock()
        self.worker = Thread(target=self.worker_run)
        self.counter = WorldstateContainer.Counter()

    class Counter:
        def __init__(self, start=0):
            self.val = start
            self.start = start
            self.lock = Lock()

        def increment(self):
            with self.lock:
                self.val += 1

        def reset(self):
            with self.lock:
                self.val = self.start

        def get(self):
            with self.lock:
                return self.val

    def start(self):
        self.worker.start()

        while not self.counter.get() == 1:
            pass

        self.update()

    def cleanup(self):
        self.queue.put(None)
        self.worker.join()

    def add(self, worldstate):
        self.queue.put(worldstate)

    def worker_run(self):
        for ws in iter(self.queue.get, None):
            heappush(self.temp_heap, ws)
            # if len(self.temp_heap) > self.max_size:
            #     heappop(self.temp_heap)
            self.counter.increment()

    def first(self):
        with self.data_lock:
            try:
                return nlargest(1, self.worldstates)[0]
            except IndexError:
                pass

    def empty(self):
        with self.data_lock:
            return not self.worldstates

    def get(self):
        with self.data_lock:
            return self.worldstates

    def update(self):
        self.worldstates = nlargest(20, self.temp_heap)
        self.temp_heap = []
        self.counter.reset()

    def get_count(self):
        return self.counter.get()


class Worldstate:
    def __init__(self, intruders, score=0):
        self.intruders = intruders
        self.score = score

    def __setattr__(self, key, value):
        object.__setattr__(self, key, value)

    def __lt__(self, other):
        return self.score <= other.score

    def add(self, intruder):
        self.intruders.append(intruder)

    def copy(self):
        return self.copy_intruders(), self.score

    def copy_intruders(self):
        result = []
        for intruder in self.intruders:
            result.append(self.copy_intruder(intruder))
        return result

    def copy_intruder(self, intruder):
        result = []
        for report in intruder:
            result.append(report)
        return result


    def measure_accuracy(self):
        seen_intruders = []
        correct_groups = 0
        incorrect_groups = 0
        intruders = [[vec.absolute_id for vec in intruder] for intruder in self.intruders]

        for grouping in intruders:
            abs_ids = set(grouping)
            seen_intruders.extend(grouping)

            if len(abs_ids) == 1:
                correct_groups += 1
            else:
                incorrect_groups += 1

        correlation_tendency = len(set(seen_intruders)) / len(intruders)
        correlation_accuracy = correct_groups / (incorrect_groups + correct_groups)

        return {'accuracy': correlation_accuracy, 'tendency': correlation_tendency}

    def __str__(self):
        intruder_strings = []

        for intruder in self.intruders:
            intruder_strings.append('{}'.format(', '.join(
                    ['({}, {})'.format(report.absolute_id, report.type) for report in intruder])))

        return '\nWorldstate\nscore: {}\n{}\n'.format(self.score, '\n'.join(intruder_strings))





