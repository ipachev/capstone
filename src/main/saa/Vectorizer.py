from collections import namedtuple
from abc import ABCMeta, abstractmethod
from threading import Thread
import queue
import pickle



class Vectorizer(metaclass=ABCMeta):
    def __init__(self):
        """
         Abstract vectorizer. Instatiates a thread but waits for
         concrete class to start the thread

         in_queue: receives packets from sensors
         out_queue: sends vectors to serviceX
        """
        self.worker = Thread(target=self.run)
        self.worker.setDaemon(True)
        self.in_queue = None
        self.exiting = False
        self.out_queue = None
        self.log_file = None
        self.vector_history = []

    def start(self):
        """
        Start the thread
        """
        self.worker.start()

    def run(self):
        """
        Continue running until told to exit
        1. Remove a packet, and call work from concrete class
        2. Send a vector to serviceX
        """
        for packet in iter(self.in_queue.get, None):
            vector = self.work(packet)
            if vector:
                # print("Vectorizer.py: " + vector.type)
                self.send_vector(vector)

    def send_vector(self, vector):
        """
        Send a vector to serviceX
        """
        self.out_queue.put(vector)

    def set_output(self, queue):
        """
        Connect the vectorizer with ServiceX
        """
        self.out_queue = queue

    def set_input(self, queue):
        """
        Set the input to a sensor
        :param queue: the sensor's queue
        """
        self.in_queue = queue

    def exit(self):
        self.in_queue.put(None)

    @abstractmethod
    def work(self, packet):
        """
        Concrete classes implement work
        :param packet: the packet to process
        :return: a vector built from the data in the packet
        """
        pass

    def dump_log(self):
        """
        Pickle the vector history to a file
        The file is created by concrete classes.
        """
        if self.log_file:
            pickle.dump(self.vector_history, self.log_file)
        else:
            print("Error: log file not created")
