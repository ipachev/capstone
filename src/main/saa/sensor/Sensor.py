import logging
from queue import Queue
from socket import socket, SHUT_RDWR

from protobuf import adsb_pb2, ownship_pb2, radar_pb2, tcas_pb2
from threading import Thread

class Sensor:
    """
    Sensor is an abstract type. Subclasses must implement create_report() and get_packet_type().
    """

    def __init__(self, name, host, port, message_header_size=4):
        super().__init__()
        self.name = name
        self.socket = socket()
        self.socket.setblocking(True)
        self.host = host
        self.port = port
        self.exiting = False
        self.worker = Thread(target=self.run)
        self.header_size = message_header_size
        self.queue = Queue()
        self.log = logging.getLogger(self.name)

    def start(self):
        self.connect()
        self.worker.daemon = False
        self.worker.start()

    def run(self):
        while not self.exiting:
            self.work()

    def exit(self):
        self.exiting = True

    def join(self):
        self.worker.join()

    def connect(self):

        self.log.info("connecting to %s %d" % (self.host, self.port))
        self.socket.connect((self.host, self.port))
        self.log.info("connected")

    def on_exit(self):
        self.queue.put(None)
        self.socket.shutdown(SHUT_RDWR)
        self.socket.close()

    def work(self):
        buf = self.socket.recv(self.header_size)
        # if len(buf) == 0:
        #     self.exit()
        #     return
        count = int.from_bytes(buf[:self.header_size], byteorder='big', signed=False)
        # if count != 0:
        #     print("Sensor.py: Got report on " + self.get_packet_type())
        buf = self.socket.recv(count)
        # if len(buf) == 0:
        #     self.exit()
        #     return
        report = self.parse_report(buf)
        self.queue.put(report)

    def parse_report(self, buffer):
        report = self.create_report()
        report.ParseFromString(bytes(buffer))
        return report

    def create_report(self):
        raise NotImplementedError()

    def get_packet_type(self):
        raise NotImplementedError()

    def get_queue(self):
        return self.queue

    def __str__(self):
        return "Sensor(" + self.name + ", " + self.host + ":" + str(self.port) + ")"


class Tcas(Sensor):
    def __init__(self, host="127.0.0.1", port=5001):
        super().__init__("TCAS", host, port)

    def create_report(self):
        return tcas_pb2.TcasReport()

    def get_packet_type(self):
        return "tcas"


class AdsB(Sensor):
    def __init__(self, host="127.0.0.1", port=5002):
        super().__init__("ADS-B", host, port)

    def create_report(self):
        return adsb_pb2.AdsBReport()

    def get_packet_type(self):
        return "adsb"


class Radar(Sensor):
    def __init__(self, host="127.0.0.1", port=5003):
        super().__init__("Radar", host, port)

    def create_report(self):
        return radar_pb2.RadarReport()

    def get_packet_type(self):
        return "radar"


class Ownship(Sensor):
    def __init__(self, host="127.0.0.1", port=5004):
        super().__init__("Ownship", host, port)

    def create_report(self):
        return ownship_pb2.OwnshipReport()

    def get_packet_type(self):
        return "ownship"
