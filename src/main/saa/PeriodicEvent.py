import threading as _threading
import time as _time


class PeriodicEvent(_threading.Thread):
    """Similar to threading.event but this will set() and clear() itself periodically. User code calls wait() to
    block until the next event. Note: the wait() method returns immediately if an event_count is given and the
    last event has already happened."""

    def __init__(self, interval, thread_event=_threading.Event(), trigger_on_start=False, clock=_time.perf_counter,
                 event_count=-1):
        super().__init__()
        if interval <= 0:
            raise ValueError('The interval must greater than 0')
        self.interval = float(interval)
        self.event = thread_event
        self.event.clear()
        self.trigger_on_start = trigger_on_start
        self.event_count = event_count
        self.clock = clock
        self.setDaemon(True)

    def trigger_event(self):
        self.event.set()
        self.event.clear()

    def stop(self):
        self.event_count = 0

    def wait(self):
        self.event.wait()

    def is_running(self):
        number_events = self.event_count
        if self.event_count > 0:
            self.event_count -= 1
        return number_events > 0 or number_events < 0

    def run(self):
        start_time = _time.perf_counter()

        if self.trigger_on_start:
            self.trigger_event()

        while self.is_running():
            time_diff = self.clock() - start_time
            while time_diff < self.interval:
                _time.sleep(self.interval - time_diff)
                time_diff = self.clock() - start_time
            self.trigger_event()
            start_time += self.interval
        self.event.set()
