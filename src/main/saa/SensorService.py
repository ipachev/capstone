from main.saa.sensor.Sensor import AdsB, Radar, Tcas, Ownship


from abc import ABCMeta, abstractmethod


# TODO need to describe what this class does
class SensorService(metaclass=ABCMeta):
    def is_a_sensor_running(self):
        for sensor in self.sensors:
            if not sensor.exiting:
                return True
        return False

    def start(self):
        """
        Start all sensors
        """
        for sensor in self.sensors:
            sensor.start()

    def exit(self):
        for sensor in self.sensors:
            sensor.exit()

        for sensor in self.sensors:
            sensor.join()

    def link(self, VSrvc):
        """
        Link each sensor's queue to a vectorizer in VSrvc
        VSrvc is responsible for establishing mapping: protobuf -> Vectorizer
        :param VSrvc: Vectorizer Service
        """
        for sensor in self.sensors:
            VSrvc[sensor.get_packet_type()] = sensor.get_queue()


class StdSensorService(SensorService):
    def __init__(self):
        """
        Instantiate sensors and queue
        Map each protobuf class to a new queue
        """
        super().__init__()
        self.sensors = [AdsB(), Radar(), Tcas(), Ownship()]





