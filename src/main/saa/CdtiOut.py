import threading
import time
from main.saa.server.CdtiServer import CdtiServer
from main.saa.PeriodicEvent import PeriodicEvent
from main.saa.CdtiReportFactory import create_report
from main.common.VectorMath import find_intruder_locations

class CdtiOut:
    def __init__(self, input_svc, cdti_server=CdtiServer()):
        self.input_svc = input_svc
        self.worker = threading.Thread(target=self.work, daemon=False)
        self.send_event_thread = threading.Thread(target=self.on_send_event, daemon=True)
        self.exiting = False
        self.server = cdti_server
        self.buffer = None
        self.update_event = PeriodicEvent(interval=1.0)
        self.send_event = PeriodicEvent(interval=1.0)

    def start(self):
        self.server.start()
        self.update_event.start()
        self.worker.start()
        self.send_event_thread.start()

    def stop(self):
        self.exiting = True
        self.worker.join()

    def exit(self):
        self.stop()

    def shutdown(self):
        # these wait events could make shutdown take a long time
        # these are here to prevent a race condition that may or may not apply
        # this is here so that on_send_event can terminate gracefully
        # it makes it the case that when shutdown returns, the threads are likely terminated
        # TODO someone should make this code correct and make this comment less verbose.
        # self.send_event.wait()
        # self.send_event.wait()
        print("SERVER STOPPING")
        self.server.stop()
        print("UPDATE EVENT STOPPING")
        self.update_event.stop()
        print("SEND EVENT STOPPING")
        self.send_event.stop()

    def work(self):
        self.update_event.wait()
        while not self.exiting:
            timestamp = time.time()
            ownship, intruders = self.input_svc.worldstate()
            if ownship and intruders:
                self.buffer = (timestamp, ownship, intruders)
                # self.buffer = report = create_report(timestamp, ownship, intruders)
                # self.server.send(report)
            self.update_event.wait()
        print("CDTI WORLDSTATE FETCH THREAD IS EXITING")
        self.shutdown()

    def on_send_event(self):
        self.send_event.wait()
        while not self.exiting:
            self.send_update()
            self.send_event.wait()

    def send_update(self):
        if not self.buffer:
            return
        timestamp, ownship, intruders = self.buffer
        # do some extrapolating
        # updated_intruders = find_intruder_locations(intruders)
        report = create_report(timestamp, ownship, intruders)
        self.server.send(report)





