import logging
from threading import Thread, Lock, Event
from socket import SHUT_RD, SHUT_WR


class CdtiClient(Thread):
    def __init__(self, sock, address):
        super().__init__()
        self.log = logging.getLogger("CDTI Client")
        self.sock = sock
        self.address = str(address)
        self.messages = []
        self.messages_lock = Lock()
        self.running_lock = Lock()
        self.event = Event()
        self.running = True
        self.sock.shutdown(SHUT_RD)

    def is_running(self):
        with self.running_lock:
            return self.running

    def stop(self):
        with self.running_lock:
            self.running = False
            self.event.set()

    def send(self, buffer):
        with self.messages_lock:
            self.messages.append(buffer)
            self.event.set()

    def run(self):
        while self.is_running():
            unsent_messages = []
            with self.messages_lock:
                unsent_messages.extend(self.messages)
                self.messages = []
                with self.running_lock:
                    if self.running:
                        self.event.clear()
            while self.is_running() and unsent_messages:
                try:
                    self.sock.sendall(unsent_messages.pop(0))
                except BrokenPipeError:
                    self.log.info("Client %s disconnected" % self.address)
                    self.stop()
            self.event.wait()

        try:
            self.sock.shutdown(SHUT_WR)
        except OSError as err:
            self.log.debug("Could not shutdown socket to %s (%s)" % (self.address, err.strerror))
        self.sock.close()
        self.log.info("%s stopped\n" % self.address)
