import logging
from socket import socket, SOL_SOCKET, SO_REUSEADDR
from threading import Thread
from main.common.IOUtil import serialize_report
from main.saa.server.CdtiClient import CdtiClient


class CdtiServer:
    def __init__(self, host='', port=5000, sock=socket(),
                 daemon=True, client_func=CdtiClient,
                 serialize_func=serialize_report, name="CdtiServer",
                 log=logging.getLogger("CDTI")):
        sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        sock.bind((host, port))
        self.sock = sock
        self.log = log
        self.clients = []
        self.thread = Thread(name=name, target=self.run)
        self.thread.setDaemon(daemon)
        self.addr = sock.getsockname()
        self.port = port
        self.running = False
        self.sock.listen(0)
        self.create_new_client = client_func
        self.serialize = serialize_func

    def accept(self):
        try:
            connection, address = self.sock.accept()
            if self.running:
                client = self.create_new_client(connection, address)
                self.clients.append(client)
                client.start()
                self.log.info('Accepted new client %s' % str(address))
        except Exception as err:
            raise err

    def send(self, report):
        buffer = self.serialize(report)
        if self.running:
            for client in self.clients:
                client.send(buffer)

    def stop(self):
        self.running = False
        print("stopping all cdti clients")
        for client in self.clients:
            print("stopped cdti client")
            client.stop()
        self.sock.close()
        print("cdti socket closed")

    def start(self):
        self.running = True
        self.thread.start()

    def run(self):
        self.log.info("Starting")
        while self.running:
            self.accept()
        self.sock.close()
        self.log.info("Stopping")
