import logging
from main.saa.c8.C8 import C8
from main.saa.PeriodicEvent import *
from main.saa.CdtiOut import CdtiOut

from main.saa.SensorService import StdSensorService
from main.saa.VectorizerService import SBVectorizerService
from main.saa.simple_cluster.GeneralVectorizerService import GeneralVectorizerService
from main.saa.simple_cluster.BadBubble import BadBubble
from main.saa.ServiceX import PassThrough


def get_log_level():
    log_level = logging.DEBUG
    # log_level = logging.INFO
    # log_level = logging.WARNING
    # log_level = logging.CRITICAL
    # log_level = logging.ERROR
    # log_level = logging.FATAL
    return log_level


def pass_through(limit=15):
    return PassThrough(limit=limit)


def bad_bubble():
    return BadBubble()


def c8():
    return C8(verbose=True)


class SP:
    def __init__(self):
        self.log = logging.getLogger("SurveillanceProcessor")
        self.sensor_svc = StdSensorService()

        self.vector_svc = SBVectorizerService()
        self.svc_x = c8()

        self.vector_svc.link(self.svc_x)
        self.sensor_svc.link(self.vector_svc)
        self.cdti = CdtiOut(self.svc_x)
        self.report_event = PeriodicEvent(1.0)

    def start(self):
        self.svc_x.start()
        self.vector_svc.start()
        self.sensor_svc.start()
        self.cdti.start()

    def shutdown(self):
        self.log.info("Stopping...")
        services = [
            ("CDTI Server", self.cdti),
            ("Service X", self.svc_x),
            ("Vector Services", self.vector_svc),
            ("Sensors", self.sensor_svc)
        ]

        for name, service in services:
            # name, service = service
            self.log.info("Trying to stop %s" % name)
            service.exit()
            self.log.info("Stopped %s" % name)
        self.log.info("done")

if __name__ == "__main__":
    import signal
    import time
    import threading
    import sys

    logging.basicConfig(level=get_log_level(), format='[%(name)s][%(levelname)s] %(message)s')

    def shutdown_handler(signum, frame):
        sp.shutdown()

    def force_exit(*args):
        exit(1)

    sp = SP()
    signal.signal(signal.SIGINT, shutdown_handler)
    signal.signal(signal.SIGTERM, shutdown_handler)
    signal.signal(signal.SIGABRT, shutdown_handler)
    signal.signal(signal.SIGALRM, shutdown_handler)
    sp.start()
