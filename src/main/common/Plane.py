import numpy as np

from main.common.MathHelpers import MathHelpers as mh


class Plane:
    def __init__(self, tail_number, latitude, longitude, altitude, north, east, down, id=None, adsb=True, radar=True,
                 tcas=True, adsb_id=None, radar_id=None, tcas_id=None, timestamp=None):
        self.tail_number = tail_number
        self.adsb_id = None
        self.radar_id = None
        self.tcas_id = None
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.north = north
        self.east = east
        self.down = down
        self.adsb = adsb
        self.radar = radar
        self.tcas = tcas
        self.timestamp = timestamp
        self.id = id

    @classmethod
    def from_vec(cls, plane_id, absolute_id, plane_type, x, y, z, dx=None, dy=None, dz=None, timestamp=None):
        lat, long, alt = mh.xyz_2_lla(x, y, z)

        if timestamp:
            n, e, d = mh.vel_2_ned(x, y, z, dx, dy, dz)
        else:
            n, e, d = None, None, None

        plane = cls(absolute_id, lat, long, alt, n, e, d, id=plane_id, timestamp=timestamp)

        if plane_type == 'adsb':
            plane.adsb_id = plane_id
        elif plane_type == 'radar':
            plane.radar_id = plane_id
        else:
            plane.tcas_id = plane_id

        return plane


    def values(self):
        """
        Get the values of the plane
        :param self:
        :return:
        """
        return [self.id, self.latitude, self.longitude, self.altitude, self.north, self.east, self.down]


    def __str__(self):
        s = ', '.join([str(x) for x in self.values()])
        return s


