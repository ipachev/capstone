import math

import numpy as np
from measurement.measures import Distance
from main.common.EarthReferenceFrame import geo_to_xyz, xyz_to_geo, north_vector, east_vector, down_vector, \
    scale_vector, add_vectors, heading_vector, ReferenceFrame
from pyproj import Geod
from nvector import FrameE
from math import sin, cos
import time
import nvector as nv
import warnings


nautical_conversion_ft = 0.000164579
nautical_conversion_km = 1.852
wgs84 = FrameE(name='WGS84')

warnings.filterwarnings("ignore")

def build_reference_frame(latitude, longitude, altitude, north, east):
    own_pos = geo_to_xyz(latitude, longitude, altitude)
    own_forward_vec = heading_vector(latitude, longitude, north, east)
    own_up_vec = scale_vector(-1, down_vector(latitude, longitude))
    return ReferenceFrame(own_pos, own_forward_vec, own_up_vec)


def find_intruder_ecef(frame, radar_azimuth, radar_elevation, radar_range):
    intruder_z = radar_range * sin(radar_elevation)
    intruder_y = radar_range * cos(radar_elevation) * cos(radar_azimuth)
    intruder_x = radar_range * cos(radar_elevation) * sin(radar_azimuth)
    return frame.to_original_xyz(intruder_x, intruder_y, intruder_z)


def find_intruder_velocity(vector, north_unit, east_unit, down_unit):
    north_vec = scale_vector(vector.north, north_unit)
    east_vec = scale_vector(vector.east, east_unit)
    down_vec = scale_vector(vector.down, down_unit)
    return add_vectors(add_vectors(north_vec, east_vec), down_vec)

def find_unit_vectors(lat, long):
    north_unit = north_vector(lat, long)
    east_unit = east_vector(lat, long)
    down_unit = down_vector(lat, long)
    return north_unit, east_unit, down_unit

def magnitude(vector):
    vector = np.array(vector)
    return np.sqrt(vector.dot(vector))


def delta_magnitude(v1, v2):
    return math.fabs(magnitude(v1) - magnitude(v2))


def distance(latA, longA, altA, latB, longB, altB):
    altA_m = Distance(ft=altA).m
    altB_m = Distance(ft=altB).m

    pointA = wgs84.GeoPoint(latA, longA, altA_m, degrees=True)
    pointB = wgs84.GeoPoint(latB, longB, altB_m, degrees=True)

    try:
        diff_AB = nv.diff_positions(pointA, pointB)
        frame_N = nv.FrameN(pointA)
        p_AB_N = diff_AB.change_frame(frame_N)
        p_AB_N = diff_AB.pvector.ravel()
        p_AB_N = [Distance(m=dist) for dist in p_AB_N]

    except:
        p_AB_N = [Distance(m=0), Distance(m=0), Distance(m=0)]

    dist = Distance(m=np.sqrt(sum([np.power(position.m, 2) for position in p_AB_N])))

    return dist


def diff_position(latA, longA, altA, latB, longB, altB):
    altA_m = Distance(ft=altA).m
    altB_m = Distance(ft=altB).m

    pointA = wgs84.GeoPoint(latA, longA, altA_m, degrees=True)
    pointB = wgs84.GeoPoint(latB, longB, altB_m, degrees=True)

    try:
        diff_AB = nv.diff_positions(pointA, pointB)
        frame_N = nv.FrameN(pointA)
        p_AB_N = diff_AB.change_frame(frame_N)
        p_AB_N = diff_AB.pvector.ravel()
        p_AB_N = [Distance(m=dist) for dist in p_AB_N]

    except:
        p_AB_N = [Distance(m=0), Distance(m=0), Distance(m=0)]

    return p_AB_N

def find_relative_position(latA, longA, altA, heading, latB, longB, altB):
    altA_m = Distance(ft=altA).m
    altB_m = Distance(ft=altB).m

    p_EA_E = wgs84.GeoPoint(latA, longA, -altA_m, degrees=True)
    p_EB_E = wgs84.GeoPoint(latB, longB, -altB_m, degrees=True)

    try:
        diff_AB = nv.diff_positions(p_EA_E, p_EB_E)
        frame_N = nv.FrameB(p_EA_E, yaw=heading, degrees=True)
        p_AB_N = diff_AB.change_frame(frame_N)
        p_AB_N = diff_AB.pvector.ravel()
        p_AB_N = [Distance(m=dist) for dist in p_AB_N]

    except:
        p_AB_N = [Distance(m=0), Distance(m=0), Distance(m=0)]

    return p_AB_N


def geo_from_tcas(range, relative_bearing, relative_altitude, ownship_latitude, ownship_longitude, ownship_altitude,
                  ownship_north, ownship_east):

    altitude = relative_altitude + ownship_altitude

    range_ft = range / nautical_conversion_ft
    distance_ft = math.sqrt(math.fabs(math.pow(range_ft, 2) - math.pow(altitude, 2)))
    distance_m = Distance(ft=distance_ft).m
    ownship_bearing = 90 - math.degrees(math.atan2(ownship_north, ownship_east))
    bearing = relative_bearing + ownship_bearing

    g = Geod(ellps='WGS84')
    longitude, latitude, b = g.fwd(ownship_longitude, ownship_latitude, bearing, distance_m)

    return latitude, longitude, altitude


def geo_from_radar(range, elevation, azimuth, lat, long, alt, north, east):
    frame = build_reference_frame(lat, long, alt, north, east)
    az, el, = math.radians(azimuth), math.radians(elevation)

    x, y, z = find_intruder_ecef(frame, az, el, range)

    return xyz_to_geo(x, y, z)


time_step = 10

def find_intruder_locations(intruders):
    updated_intruders = []
    now = time.time()
    for intruder in intruders:
        if intruder.timestamp:
            lat, long, alt = find_location(intruder.latitude, intruder.longitude, intruder.altitude,
                                           intruder.north, intruder.east, intruder.down, now - intruder.timestamp)
            intruder.latitude = lat
            intruder.longitude = long
            intruder.altitude = alt
            updated_intruders.append(intruder)

    return updated_intruders


def find_location(latitude, longitude, altitude, north, east, down, dt):
    def find_velocity(lat, long):
        n_vec = scale_vector(north, north_vector(lat, long))
        e_vec = scale_vector(east, east_vector(lat, long))
        d_vec = scale_vector(down, down_vector(lat, long))
        return np.array(add_vectors(add_vectors(n_vec, e_vec), d_vec), dtype=np.float64)


    def find_next_pos(lat, long, alt, dt):
        p = np.array(geo_to_xyz(lat, long, alt))
        v = find_velocity(lat, long) * dt
        p = p + v
        return xyz_to_geo(p[0], p[1], p[2])

    current_t = 0
    current_lat = latitude
    current_long = longitude
    current_alt = altitude

    while current_t + time_step < dt:
        current_lat, current_long, current_alt = find_next_pos(current_lat, current_long, current_alt, time_step)
        current_t += time_step
    return find_next_pos(current_lat, current_long, current_alt, dt - current_t)


def geo_from_tcas2(range, bearing, relative_altitude, latitude, longitude, altitude):
    range_ft = range / nautical_conversion_ft
    altitude = relative_altitude + altitude

    distance_ft = math.sqrt(math.fabs(math.pow(range_ft, 2) - math.pow(altitude, 2)))
    distance_m = Distance(ft=distance_ft).m

    g = Geod(ellps='WGS84')
    longitude, latitude, b = g.fwd(longitude, latitude, bearing, distance_m)

    return latitude, longitude, altitude


def geo_from_radar2(range, elevation, azimuth, lat, long, alt):
    """
    Calculate the geo coordinates of a plane from a radar report
    :param range: relative distance to intruder
    :param elevation: up/down angle in degrees
    :param azimuth: left/right angle in degrees
    :param lat: latitude of ownship
    :param long: longitude of ownship
    :param alt: altitude of ownship
    :return: intruders lat, long, altitude
    """
    altitude = alt
    distance_ft = range * np.cos(math.radians(elevation))
    delta_alt = math.sqrt(math.pow(range, 2) - math.pow(distance_ft, 2))

    if elevation > 0:
        altitude += delta_alt
    else:
        altitude -= delta_alt

    distance_m = Distance(ft=distance_ft).m

    g = Geod(ellps='WGS84')
    longitude, latitude, b = g.fwd(long, lat, azimuth, distance_m)

    return latitude, longitude, altitude