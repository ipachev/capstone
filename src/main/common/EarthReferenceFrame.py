import pyproj
import nvector
from measurement.measures import distance
from math import sin, cos, radians, pi, sqrt, pow, atan2, degrees, acos, asin
from main.common.ReferenceFrame import ReferenceFrame
from main.common.GeographicCoordinate import GeographicCoordinate
# The following Earth measurements were found at:
# http://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html
equatorial_radius_feet = 20925524.9
polar_radius_feet = 20855643.0

# default precision for functions defined in this file
default_precision = 25


def magnitude(x, y, z):
    """
    Find the distance a point is from origin given x, y, and z coordinates
    :param x: a number representing the x coordinate
    :param y: a number representing the y coordinate
    :param z: a number representing the z coordinate
    :return: the distance the point at x, y, z is from the origin
    """
    return sqrt(x*x + y*y + z*z)


def add_vectors(a, b):
    """
    Add 2 3D vectors.
    :param a: a tuple with 3 numbers
    :param b: a tuple with 3 numbers
    :return: a tuple with 3 numbers representing the resulting vector
    """
    return a[0] + b[0], a[1] + b[1], a[2] + b[2]


def scale_vector(scale, vector):
    """
    Scale a 3D vector.
    :param scale: a number to scale the vector by
    :param vector: a tuple with 3 numbers
    :return: a tuple with 3 numbers representing the scaled vector
    """
    return scale * vector[0], scale * vector[1], scale * vector[2]


def cross_product(b, c):
    """
    Find the cross product of 3D vectors b, and c
    :param b: a tuple with 3 numbers
    :param c: a tuple with 3 numbers
    :return: the cross product of b and c
    """
    def cross_components(bx, by, bz, cx, cy, cz):
        ax = by*cz - bz*cy
        ay = bz*cx - bx*cz
        az = bx*cy - by*cx
        return ax, ay, az

    return cross_components(b[0], b[1], b[2], c[0], c[1], c[2])


def geocentric_radius(latitude, precision=default_precision):
    """
    Use an ellipsoid to approximate the shape of the earth. This function approximates the radius of
    the earth using latitude

    For background information see
    https://en.wikipedia.org/wiki/Earth_radius#Introduction

    The formula was found at
    https://en.wikipedia.org/wiki/Earth_radius#Geocentric_radius

    :param latitude: Degrees north from the equator.
    :param precision: The floating point precision to round too
    :return: The distance in feet to the center of the earth at latitude degrees.
    """
    latitude = radians(latitude)
    a = equatorial_radius_feet
    b = polar_radius_feet
    cos_φ = cos(latitude)
    sin_φ = sin(latitude)
    numerator = pow(a*a*cos_φ, 2) + pow(b*b*sin_φ, 2)
    denominator = pow(a*cos_φ, 2) + pow(b*sin_φ, 2)
    # return round(sqrt(numerator/denominator), precision)
    return GeographicCoordinate.earth_radius_feet


def geo_to_xyz(latitude, longitude, altitude, precision=default_precision):
    """
    Find the x, y, z coordinate at the given geographic coordinate and altitude.
    :param latitude: a number for the degrees north of the equator
    :param longitude: a number for the degrees east of the prime meridian
    :param altitude: a number for the number of feet above sea level
    :param precision: an integer for the number of decimal places to keep in the final result
    :return: a tuple with three numbers for x, y, and z (in that order). These coordinates represent the number of feet
    from the center of the earth. The +x axis goes through the prime meridian at latitude 0. The y axis is derived using
    right-hand rule.
    """
    lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    alt_meters = distance.Distance(ft=altitude).m
    x_m, y_m, z_m = pyproj.transform(lla, ecef, longitude, latitude, alt_meters, radians=False)
    x, y, z = distance.Distance(m=x_m).ft, distance.Distance(m=y_m).ft, distance.Distance(m=z_m).ft
    return x, y, z


def xyz_to_geo(x, y, z, precision=default_precision):
    """
    Find the latitude, longitude, and altitude using x, y, z coordinates of the earth reference frame
    :param x: a number, the number of feet along the x axis
    :param y: a number, the number of feet along the y axis
    :param z: a number, the number of feet along the z axis
    :param precision: an integer for the number of decimal places to keep in the final result
    :return: a tuple holding latitude, longitude, and altitude (in that order). Latitude and longitude are in degrees.
    Altitude is in feet.
    """
    lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    x_m, y_m, z_m = distance.Distance(ft=x).m, distance.Distance(ft=y).m, distance.Distance(ft=z).m
    long, lat, alt_m = pyproj.transform(ecef, lla, x_m, y_m, z_m, radians=False)
    alt = distance.Distance(m=alt_m).ft
    return lat, long, alt


def north_vector(latitude, longitude, precision=default_precision):
    """
    Find the x, y, z components of the vector that points north at the given latitude and longitude
    :param latitude: a number for the degrees north of the equator
    :param longitude: a number for the degrees east of the prime meridian
    :param precision: an integer for the number of decimal places to keep in the final result
    :return: a tuple with three numbers for x, y, and z (in that order). This is a unit vector that points north.
    """
    nv = nvector.GeoPoint(latitude, longitude, degrees=True)
    vec = nv.to_nvector()
    normal = vec.normal
    b = normal[0][0], normal[1][0], normal[2][0]
    c = east_vector(latitude, longitude)
    x, y, z = cross_product(b, c)
    m = magnitude(x, y, z)
    return x/m, y/m, z/m


def down_vector(latitude, longitude, precision=default_precision):
    """
    Find the x, y, z components of the vector that points down at the given latitude and longitude
    :param latitude: a number for the degrees north of the equator
    :param longitude: a number for the degrees east of the prime meridian
    :param precision: an integer for the number of decimal places to keep in the final result
    :return: a tuple with three numbers for x, y, and z (in that order). This is a unit vector that points down.
    """
    nv = nvector.GeoPoint(latitude, longitude, degrees=True)
    vec = nv.to_nvector()
    normal = vec.normal
    x, y, z = normal[0][0], normal[1][0], normal[2][0]
    return -1.0*x, -1.0*y, -1.0*z


def east_vector(latitude, longitude, precision=default_precision):
    """
    Find the x, y, z components of the vector that points east at the given latitude and longitude
    :param latitude: a number for the degrees north of the equator
    :param longitude: a number for the degrees east of the prime meridian
    :param precision: an integer for the number of decimal places to keep in the final result
    :return: a tuple with three numbers for x, y, and z (in that order). This is a unit vector that points east.
    """
    nv = nvector.GeoPoint(latitude, longitude, degrees=True)
    vec = nv.to_nvector()
    normal = vec.normal
    c = normal[0][0], normal[1][0], normal[2][0]
    b = 0.0, 0.0, 1.0
    x, y, z = cross_product(b, c)
    m = magnitude(x, y, z)
    return x/m, y/m, z/m


def heading_vector(latitude, longitude, north_velocity, east_velocity, precision=default_precision):
    """
    Find the x, y, z components of the vector that points in compass direction at the given latitude and longitude using
    the given north velocity and east velocity.
    :param latitude: a number for the degrees north of the equator
    :param longitude: a number for the degrees east of the prime meridian
    :param north_velocity: a number for the feet per second traveling in the north direction
    :param east_velocity: a number for the feet per second traveling in the east direction
    :param precision: an integer for the number of decimal places to keep in the final result
    :return: a tuple with three numbers for x, y, and z (in that order). This is a unit vector that points the same way
    that a compass would point if it faced the direction of motion.
    """
    north = scale_vector(north_velocity, north_vector(latitude, longitude, precision))
    east = scale_vector(east_velocity, east_vector(latitude, longitude, precision))
    x, y, z = add_vectors(north, east)
    d = magnitude(x, y, z)
    if d > 0:
        x, y, z = round(x/d, precision), round(y/d, precision), round(z/d, precision)
    return x, y, z


