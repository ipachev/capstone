from math import cos, sin
import numpy as np


def vec(x, y, z):
    return np.array([x, y, z, 1.0], dtype=np.float64)


def translate(x=0.0, y=0.0, z=0.0):
    return np.array([
        [1, 0, 0, x],
        [0, 1, 0, y],
        [0, 0, 1, z],
        [0, 0, 0, 1]], dtype=np.float64)


def scale(x=1.0, y=1.0, z=1.0):
    return np.array([
        [x, 0, 0, 0],
        [0, y, 0, 0],
        [0, 0, z, 0],
        [0, 0, 0, 1]], dtype=np.float64)


def rotate_x(θ):
    """
    A rotation matrix about the x-axis. This video goes over the idea:

    https://www.youtube.com/watch?v=fIHgznIttvk

    :param θ: counter-clockwise rotation angle in radians
    :return: a 4x4 matrix that you can multiply a vector by to rotate its coordinates around the x-axis.
    """
    cos_θ = cos(θ)
    sin_θ = sin(θ)
    return np.array([
        [1, 0, 0, 0],
        [0, cos_θ, -sin_θ, 0],
        [0, sin_θ, cos_θ, 0],
        [0, 0, 0, 1]], dtype=np.float64)


def rotate_y(θ):
    """
    A rotation matrix about the y-axis. This video goes over the idea:

    https://www.youtube.com/watch?v=fIHgznIttvk

    :param θ: counter-clockwise rotation angle in radians
    :return: a 4x4 matrix that you can multiply a vector by to rotate its coordinates around the y-axis.
    """
    cos_θ = cos(θ)
    sin_θ = sin(θ)
    return np.array([
        [cos_θ, 0, sin_θ, 0],
        [0, 1, 0, 0],
        [-sin_θ, 0, cos_θ, 0],
        [0, 0, 0, 1]], dtype=np.float64)


def rotate_z(θ):
    """
    A rotation matrix about the z-axis. This video goes over the idea:

    https://www.youtube.com/watch?v=fIHgznIttvk

    :param θ: counter-clockwise rotation angle in radians
    :return: a 4x4 matrix that you can multiply a vector by to rotate its coordinates around the z-axis.
    """
    cos_θ = cos(θ)
    sin_θ = sin(θ)
    return np.array([
        [cos_θ, -sin_θ, 0, 0],
        [sin_θ, cos_θ, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]], dtype=np.float64)
