import numpy as np
import math


class VectorComparator:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    @staticmethod
    def compare(v1, v2):
        dv_type = '{}_{}'.format(*sorted([v1.type, v2.type]))
        dv = VectorComparator.delta_vector(np.array([v1.x, v1.y, v1.z, v1.dx, v1.dy, v1.dz, v1.timestamp]),
                                           np.array([v2.x, v2.y, v2.z, v2.dx, v2.dy, v2.dz, v2.timestamp]))

        A = np.array([v1.dx, v1.dy, v1.dz])
        B = np.array([v2.dx, v2.dy, v2.dz])

        dpos = np.array([dv[0], dv[1], dv[2]])
        mag_dpos = np.sqrt(dpos.dot(dpos))
        mag_dspeed = math.fabs(np.sqrt(A.dot(A)) - np.sqrt(B.dot(B)))

        dvel = A - B
        mag_dvel = math.sqrt(dvel.dot(dvel))
        ratio = np.dot(A, B) / (np.sqrt(A.dot(A)) * np.sqrt(B.dot(B)))
        θ = math.acos(np.clip(ratio, -1.0, 1.0))

        dv = np.append(dv, [mag_dpos, mag_dspeed, mag_dvel, θ])
        dv = dv[~np.isnan(dv)]

        return dv_type, dv


    @staticmethod
    def delta_vector(v1, v2):
        '''
        Get the difference of two vectors.
        Note: vectors must be of the same length


        :param v1:  numpy array
        :param v2:  numpy array
        :return:    numpy array, [delta_vector]
        '''
        return np.fabs(v1 - v2)