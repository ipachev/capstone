from math import sqrt, atan2, degrees
from nvector import GeoPoint, FrameB, ECEFvector
from measurement.measures import Distance
import numpy as np
from numpy.linalg import inv


class MathHelpers:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def vector_1d_meters_to_feet(vec):
        for q in range(len(vec)):
            vec[q] *= 3.28084
        return vec

    @staticmethod
    def vector_1d_feet_to_meters(vec):
        for q in range(len(vec)):
            vec[q] *= 0.3048
        return vec

    @staticmethod
    def geo_frame_to_wgs84_func(lat, long, alt, yaw, pitch, roll):
        """
        Makes a function that takes coordinates in the reference frame (defined by the parameters to this function)
        represented as numpy number arrays of shape "(3,)" (for example np.array([1, 2, 3)) and returns a numpy array
        of numbers with shape "(3,)"  which expresses the same place in WGS84.

        Note: The values in the numpy array argument passed to the returned function must be feet.
        Note: The values in the numpy array returned by the returned function are in feet.

        :param lat: latitude in degrees
        :param long: longitude in degrees
        :param alt: altitude in feet
        :param yaw: in degrees
        :param pitch: in degrees
        :param roll: in degrees
        :return:
        """
        alt = Distance(ft=alt).m
        G = GeoPoint(lat, long, -1 * alt)
        ecef_origin_vector = G.to_ecef_vector()
        R_EB = FrameB(G, yaw, pitch, roll, degrees=True).R_EN

        def tx(p_BA_B):
            vec = MathHelpers.vector_1d_feet_to_meters(p_BA_B)
            result = R_EB.dot(vec) + ecef_origin_vector
            return MathHelpers.vector_1d_meters_to_feet(result)

        return tx

    @staticmethod
    def wgs84_to_geo_frame_func(lat, long, alt, yaw, pitch, roll):
        """
        Makes a function that takes coordinates in WGS84 represented as numpy number arrays of shape "(3,)"
        (for example np.array([1, 2, 3)) and returns a numpy array of numbers with shape "(3,)"  which expresses the
        same place in the reference frame (defined by the parameters to this function).

        Note: This function returned expects and returns coordinate values in meters.

        :param lat: latitude in degrees
        :param long: longitude in degrees
        :param alt: altitude in feet
        :param yaw: in degrees
        :param pitch: in degrees
        :param roll: in degrees
        :return:
        """
        alt = Distance(ft=alt).m
        G = GeoPoint(lat, long, -alt, degrees=True)
        ecef_origin_vector = G.to_ecef_vector().pvector.T[0]
        R_EB = FrameB(G, yaw, pitch, roll, degrees=True).R_EN.T

        def tx(p_EA_E):
            # vec = p_EA_E
            vec = MathHelpers.vector_1d_feet_to_meters(np.array(p_EA_E))
            vec = vec.T - ecef_origin_vector
            result = R_EB.dot(vec)
            result = MathHelpers.vector_1d_meters_to_feet(result)
            return result

        return tx

    @staticmethod
    def make_frame(ownship):
        p_EO_E = GeoPoint(ownship.latitude, ownship.longitude, -1*Distance(ft=ownship.altitude).m, degrees=True)
        return FrameB(p_EO_E, yaw=atan2(ownship.east, ownship.north), degrees=False)

    @staticmethod
    def find_relative_pos(ownship, intruder):
        p_EO_E = GeoPoint(ownship.latitude, ownship.longitude, -1*Distance(ft=ownship.altitude).m, degrees=True)
        p_EO_E = p_EO_E.to_ecef_vector()
        p_EI_E = GeoPoint(intruder.latitude, intruder.longitude, -1*Distance(ft=intruder.altitude).m, degrees=True)
        p_EI_E = p_EI_E.to_ecef_vector()

        own_frame = MathHelpers.make_frame(ownship)
        xyz = np.dot(own_frame.R_EN.T, p_EI_E.pvector - p_EO_E.pvector)

        return Distance(m=xyz[0]).ft, Distance(m=xyz[1]).ft, Distance(m=xyz[2]).ft

    @staticmethod
    def calculate_range(intruder, ownship):
        intruder_x, intruder_y, intruder_z = MathHelpers.find_xyz(intruder)
        ownship_x, ownship_y, ownship_z = MathHelpers.find_xyz(ownship)
        x = intruder_x - ownship_x
        y = intruder_y - ownship_y
        z = intruder_z - ownship_z
        return sqrt(x*x + y*y + z*z)

    @staticmethod
    def feet_2_meters(*lengths):
        return tuple(map(lambda l: Distance(ft=l).m, lengths))

    @staticmethod
    def meters_2_feet(*lengths):
        return tuple(map(lambda l: Distance(m=l).ft, lengths))

    @staticmethod
    def find_xyz(intruder):
        return MathHelpers.lla_2_xyz(intruder.latitude, intruder.longitude, intruder.altitude)

    @staticmethod
    def xyz_2_lla(x, y, z):
        p_EI_E = ECEFvector(np.array([MathHelpers.feet_2_meters(x, y, z)]).reshape(3, 1))
        p_EI_E = p_EI_E.to_geo_point()

        return degrees(p_EI_E.latitude), degrees(p_EI_E.longitude), -MathHelpers.meters_2_feet(p_EI_E.z)[0]


    @staticmethod
    def lla_2_xyz(latitude, longitude, altitude):
        altitude, = MathHelpers.feet_2_meters(altitude)
        p_EI_E = GeoPoint(latitude, longitude, -1 * altitude, degrees=True)
        p_EI_E = p_EI_E.to_ecef_vector()
        xyz = p_EI_E.pvector
        return MathHelpers.meters_2_feet(xyz[0], xyz[1], xyz[2])

    @staticmethod
    def ned_2_vel(x, y, z, n, e, d):
        def mag(arr):
            return sqrt(arr.dot(arr))
        pos = ECEFvector(np.array([[x], [y], [z]]))
        up = np.array(pos.to_nvector().normal).flatten()
        east = np.cross(np.array([0, 0, 1]), up)
        north = np.cross(up, east)

        vel = (n/mag(north) * north) + (e/mag(east) * east) + (-d/mag(up) * up)
        return vel[0], vel[1], vel[2]

    @staticmethod
    def vel_2_ned(x, y, z, dx, dy, dz):
        def mag(arr):
            return sqrt(arr.dot(arr))

        pos = ECEFvector(np.array([x, y, z]).reshape(3, 1))
        up = np.array(pos.to_nvector().normal).flatten()
        east = np.cross(np.array([0, 0, 1]), up)
        north = np.cross(up, east)
        down = -up

        vel = np.array([dx, dy, dz])

        n = np.dot(north, vel) / mag(north)
        e = np.dot(east, vel) / mag(east)
        d = np.dot(down, vel) / mag(down)

        return n, e, d

    @staticmethod
    def closest_point_approach(p0, p1, q0, q1):
        w0 = p0 - q0
        u = p1 - p0
        v = q1 - q0
        a = u.dot(u)
        b = u.dot(v)
        c = v.dot(v)
        d = u.dot(w0)
        e = v.dot(w0)
        s = (b * e - c * d)/(a * c - b * b)
        t = (a * e - b * d)/(a * c - b * b)
        p = p0 + u * s
        q = q0 + v * t

        return sqrt(sum([x * x for x in (w0 + (s * u) - (t * v))]))
        return p, q



    @staticmethod
    def find_bearing_and_range(intruder, ownship):
        g_EO_E = GeoPoint(ownship.latitude, ownship.longitude, -1 * Distance(ft=ownship.altitude).m, degrees=True)
        p_EO_E = g_EO_E.to_ecef_vector()
        frame = FrameB(g_EO_E, yaw=atan2(ownship.east, ownship.north), degrees=False)
        R_EO = frame.R_EN

        p_OP0_O = np.array([0, 0, 0]).T
        p_EP0_E = np.array(np.dot(R_EO, p_OP0_O)).flatten() + p_EO_E.pvector.T
        p_EP0_E = p_EP0_E[0]
        x0, y0, z0 = p_EP0_E[0], p_EP0_E[1], p_EP0_E[2]
        p_OP1_O = np.array([1, 0, 0]).T
        p_EP1_E = np.array(np.dot(R_EO, p_OP1_O)).flatten() + p_EO_E.pvector.T
        p_EP1_E = p_EP1_E[0]
        x1, y1, z1 = p_EP1_E[0], p_EP1_E[1], p_EP1_E[2]
        p_OP2_O = np.array([0, 1, 0]).T
        p_EP2_E = np.array(np.dot(R_EO, p_OP2_O)).flatten() + p_EO_E.pvector.T
        p_EP2_E = p_EP2_E[0]
        x2, y2, z2 = p_EP2_E[0], p_EP2_E[1], p_EP2_E[2]

        xa, ya, za = MathHelpers.lla_2_xyz(intruder.latitude, intruder.longitude, intruder.altitude)
        xb, yb, zb = MathHelpers.lla_2_xyz(intruder.latitude, intruder.longitude, intruder.altitude + 1)

        A = np.array([
            [xa - xb, x1 - x0, x2 - x0],
            [ya - yb, y1 - y0, y2 - y0],
            [za - zb, z1 - z0, z2 - z0]
        ])
        vec = np.array([
            [xa - x0],
            [ya - y0],
            [za - z0]
        ])
        result_vec = np.dot(inv(A), vec).T
        t = result_vec[0][0]
        I_a = np.array([xa, ya, za])
        I_b = np.array([xb, yb, zb])
        p_ER_E = I_a + (I_b - I_a) * t

        p_tmp = MathHelpers.xyz_2_lla(p_ER_E[0], p_ER_E[1], p_ER_E[2])

        tmp = p_ER_E - p_EO_E.pvector.T[0]
        range = sqrt(tmp.dot(tmp)) / 1852.0
        p_OR_O = np.array(np.dot(R_EO.T, tmp)).flatten()

        return degrees(atan2(p_OR_O[1], p_OR_O[0])), range


    # @staticmethod
    # def p_EO_E_2_p_EI_E(p_EO, distance, azimuth):
    #     return n_EO_E.geo_point(t_range_m, bearing, degrees=True)




