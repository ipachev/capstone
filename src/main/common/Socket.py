import logging
import threading
import socket as _socket

log = logging.getLogger(__name__)


class Socket:
    class State:
        new = "NEW"
        connected = "CONNECTED"
        disconnected = "DISCONNECTED"
        error = "ERROR"

    def __init__(self, host, port):
        self.endpoint = (host, port)
        self.sock = None
        self._state = Socket.State.new
        self.lock = threading.Lock()

    def connect(self, sock=None, connection_timeout=3, timeout=None):
        if not sock:
            sock = _socket.create_connection(self.endpoint, connection_timeout)
        self.sock = sock
        self.sock.settimeout(timeout)
        self.state(Socket.State.connected)
        return self

    @staticmethod
    def disconnected():
        raise NotImplemented()

    @staticmethod
    def error(err):
        raise err

    def check_socket_result(self, n):
        n = len(n)
        if n == 0:
            self.state(Socket.State.disconnected)
            self.disconnected()
        return n

    def send(self, buffer, flags=None):
        """
        Send data until all data has been sent or an error occurs. On error, an exception is raised.
        :param buffer: a buffer of bytes to send
        :param flags: optional flags argument same meaning as the arg in pythons standard lib's socket.recv()
        :return: None
        """
        log.debug(self, "sending", len(buffer), "bytes")
        self.sock.sendall(buffer, flags)

    def recv(self, num):
        """
        Read exactly num bytes from the socket.
        :param num: the number of bytes to read.
        """
        try:
            tmp_buffer = self.sock.recv(num)
            num -= self.check_socket_result(tmp_buffer)
            while num > 0:
                tmp_buffer2 = self.sock.recv(num)
                num -= self.check_socket_result(tmp_buffer2)
                tmp_buffer.extend(tmp_buffer2)

            log.debug(self, "recv", len(tmp_buffer), "bytes")
            return tmp_buffer

        except OSError as err:
            self.state(Socket.State.error)
            self.error(err)

    def state(self, state=None):
        with self.lock:
            if state is not None:
                if self._state != state:
                    self._state = state
                    log.debug("Socket(%s:%s, %s)" % (self.endpoint[0], self.endpoint[1], self._state))
            else:
                return self._state

    def __str__(self):
        return "Socket(%s:%s, %s)" % (self.endpoint[0], self.endpoint[1], self.state())

