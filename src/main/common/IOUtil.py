

def serialize_report(report):
    size = report.ByteSize()
    buffer = bytearray(size + 4)
    buffer[:4] = serialize_header(size)
    buffer[4:] = report.SerializeToString()
    return buffer


def serialize_header(message_size):
    return int(message_size).to_bytes(4, byteorder='big', signed=False)


def read_header(buffer):
    return int.from_bytes(buffer[0:4], byteorder='big', signed=False)
