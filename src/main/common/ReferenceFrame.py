from math import pi, atan2
from numpy import array, float64
from main.common.MatrixMath import translate, rotate_x, rotate_y, rotate_z

class ReferenceFrame:
    def __init__(self, xyz_location, y_vector, z_vector):
        """
        Create a new frame of reference centered at xyz_location with its y axis pointing in the same direction as the
        y_vector argument and the z-axis pointing along the z_vector argument
        :param xyz_location: a tuple holding 3 numbers
        :param y_vector: a tuple holding 3 numbers
        :param z_vector: a tuple holding 3 numbers
        :return: a ReferenceFrame that can take points in the original reference frame and calculate positions in this
        new reference frame.
        """
        move_from, move_to = ReferenceFrame._move_matrices(xyz_location)
        rot_z_from, rot_z_to = ReferenceFrame._z_rotation_matrices(y_vector)
        rot_x_from, rot_x_to = ReferenceFrame._x_rotation_matrices(y_vector, rot_z_from)
        rot_y_from, rot_y_to = ReferenceFrame._y_rotation_matrices(z_vector, rot_z_from, rot_x_from)

        self.tx_from_original = rot_y_from.dot(rot_x_from).dot(rot_z_from).dot(move_from)
        self.tx_to_original = move_to.dot(rot_z_to).dot(rot_x_to).dot(rot_y_to)

    def from_original_xyz(self, x, y, z, precision=25):
        """
        Finds new x, y, and z coordinates relative to this reference frame using coordinates from the original frame.
        :param x: a number, the x coordinate
        :param y: a number, the y coordinate
        :param z: a number, the z coordinate
        :param precision: an integer for the number of decimal places to keep in the final result
        :return: a tuple with 3 numbers for x, y, and z (in that order).
        """
        return ReferenceFrame._tx(self.tx_from_original, x, y, z, precision)

    def to_original_xyz(self, x, y, z, precision=25):
        """
        Finds x, y, and z in the original coordinates given coordinates in the current frame.
        :param x: a number, the x coordinate
        :param y: a number, the y coordinate
        :param z: a number, the z coordinate
        :param precision: an integer for the number of decimal places to keep in the final result
        :return: a tuple with 3 numbers for x, y, and z (in that order).
        """
        return ReferenceFrame._tx(self.tx_to_original, x, y, z, precision)

    @staticmethod
    def _tx(matrix, x, y, z, precision):
        tmp = matrix.dot(array([x, y, z, 1]))
        return round(tmp[0], precision), round(tmp[1], precision), round(tmp[2], precision)

    @staticmethod
    def _vec_components(x, y, z):
        return array([x, y, z, 1.0], dtype=float64)

    @staticmethod
    def _vec_from_tuple(v):
        return ReferenceFrame._vec_components(v[0], v[1], v[2])

    @staticmethod
    def _move_matrices(xyz_location):
        x, y, z = xyz_location
        from_origin_frame = translate(-x, -y, -z)
        to_origin_frame = translate(x, y, z)
        return from_origin_frame, to_origin_frame

    @staticmethod
    def _z_rotation_matrices(y_vector):
        yx, yy, yz = y_vector
        theta_z = pi/2 - atan2(yy, yx)
        from_origin_frame = rotate_z(theta_z)
        to_origin_frame = rotate_z(-theta_z)
        return from_origin_frame, to_origin_frame

    @staticmethod
    def _x_rotation_matrices(y_vector, rot_z_from):
        tmp = rot_z_from.dot(ReferenceFrame._vec_from_tuple(y_vector))
        yx, yy, yz = tmp[0], tmp[1], tmp[2]
        theta = -atan2(yz, yy)
        from_origin_frame = rotate_x(theta)
        to_origin_frame = rotate_x(-theta)
        return from_origin_frame, to_origin_frame

    @staticmethod
    def _y_rotation_matrices(z_vector, rot_z_from, rot_x_from):
        rot_matrix = rot_x_from.dot(rot_z_from)
        tmp = rot_matrix .dot(ReferenceFrame._vec_from_tuple(z_vector))
        zx, zy, zz = tmp[0], tmp[1], tmp[2]
        theta = atan2(zx, zz)
        from_origin_frame = rotate_y(theta)
        to_origin_frame = rotate_y(-theta)
        return from_origin_frame, to_origin_frame
