import math


class GeographicCoordinate:
    earth_radius_nautical_miles = 3440.065
    nautical_miles_per_foot = 0.000164579
    earth_radius_feet = earth_radius_nautical_miles/nautical_miles_per_foot

    @staticmethod
    def factory(latitude=0, longitude=0, altitude=0, north=None, east=None):
        """
        latitude and longitude are in degrees
        altitude is in feet
        north and east are in nautical miles
        :param east: nautical miles east of the prime meridian
        :param north: nautical miles north of the equator
        :param altitude: feet above sea level
        :param longitude: in degrees
        :param latitude: in degrees
        """
        radius_pi = GeographicCoordinate.earth_radius_nautical_miles * math.pi
        if east is not None:
            longitude = math.degrees(east/radius_pi)
        if north is not None:
            latitude = math.degrees(north/radius_pi)
        return GeographicCoordinate(latitude, longitude, altitude)

    def __init__(self, latitude_degrees, longitude_degrees, altitude_feet=0):
        self.latitude = latitude_degrees
        self.longitude = longitude_degrees
        self.altitude = altitude_feet

    def __sub__(self, other):
        if isinstance(other, GeographicCoordinate):
            delta_latitude = self.latitude - other.latitude
            delta_longitude = self.longitude - other.longitude
            delta_altitude = self.altitude - other.altitude
            return GeographicCoordinate(delta_latitude, delta_longitude, delta_altitude)
        else:
            raise ValueError

    def __add__(self, other):
        if isinstance(other, GeographicCoordinate):
            north = self.north() + other.north()
            east = self.east() + other.east()
            altitude = self.altitude + other.altitude
            return GeographicCoordinate.factory(north=north, east=east, altitude=altitude)
        else:
            raise ValueError

    def north(self):
        return GeographicCoordinate.earth_radius_nautical_miles*math.radians(self.latitude)*math.pi

    def east(self):
        return GeographicCoordinate.earth_radius_nautical_miles*math.radians(self.longitude)*math.pi

    def distance(self, other):
        """result in nautical miles"""
        if isinstance(other, GeographicCoordinate):
            delta_latitude = self.latitude - other.latitude
            delta_longitude = self.longitude - other.longitude
            delta_altitude = self.altitude - other.altitude
            dif = GeographicCoordinate(delta_latitude, delta_longitude, delta_altitude)
            north = dif.north()
            east = dif.east()
            alt = dif.altitude * GeographicCoordinate.nautical_miles_per_foot
            return math.sqrt(north*north + east*east + alt*alt)
        else:
            raise ValueError

    def find_ned_vector(self, other):
        delta = other - self
        return delta.north(), delta.east(), delta.altitude

    def __str__(self):
        if 0 <= self.longitude % 180:
            longitude_dir = "E"
        else:
            longitude_dir = "W"

        if 0 <= self.latitude:
            latitude_dir = "N"
        else:
            latitude_dir = "S"
        return "GeographicCoordinate({0}\x00\xb0 {1}, {2}\x00\xb0 {3}, {4} feet)".format(self.latitude, latitude_dir,
                                                                                         self.longitude % 180,
                                                                                         longitude_dir, self.altitude)


