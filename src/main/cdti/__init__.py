# __author__ = 'rzink92'
# from math import *
# from main.cdti.Intruder import *
# import sys
#
# try:
#   from OpenGL.GLUT import *
#   from OpenGL.GL import *
#   from OpenGL.GLU import *
# except:
#   print ('''
# ERROR: PyOpenGL not installed properly.
#         ''')
#   sys.exit()
#
# teapotList = None
# keysPressed = []
# g_fViewDistance = 0.
# g_Width = 600
# g_Height = 600
# g_nearPlane = 1.
# g_farPlane = 1000.
# zoom = 10
#
# #camera stuff
# def resetView():
#     global zoom, xRotate, yRotate, zRotate, xTrans, yTrans
#     xRotate = 0.
#     yRotate = 0.
#     zRotate = 0.
#     xTrans = 0.
#     yTrans = 0.
#     glutPostRedisplay()
#
#
#
# # Initialize depth buffer, projection matrix, light source, and lighting
# # model.  Do not specify a material property here.
#
# def init():
#    ambient = [0.0, 0.0, 0.0, 1.0]
#    diffuse = [1.0, 1.0, 1.0, 1.0]
#    specular = [1.0, 1.0, 1.0, 1.0]
#    position = [0.0, 3.0, 3.0, 0.0]
#
#    lmodel_ambient = [0.2, 0.2, 0.2, 1.0]
#    local_view = [0.0]
#
#    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient)
#    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse)
#    glLightfv(GL_LIGHT0, GL_POSITION, position)
#    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient)
#    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, local_view)
#
#    glFrontFace(GL_CW)
#    glEnable(GL_LIGHTING)
#    glEnable(GL_LIGHT0)
#    glEnable(GL_AUTO_NORMAL)
#    glEnable(GL_NORMALIZE)
#    glEnable(GL_DEPTH_TEST)
#
#    #  be efficient--make teapot display list
#    global teapotList
#    teapotList = glGenLists(1)
#    glNewList (teapotList, GL_COMPILE)
#    glutSolidTeapot(1.0)
#
#    glEndList ()
#
# # Move object into position.  Use 3rd through 12th
# # parameters to specify the material property.  Draw a teapot.
# def renderTeapot(x, y, ambr, ambg, ambb, difr, difg,
#                       difb, specr, specg, specb, shine):
#    mat = [0, 0, 0, 0]
#
#    glPushMatrix()
#    glTranslatef(x, y, 0.0)
#    mat[0] = ambr; mat[1] = ambg; mat[2] = ambb; mat[3] = 1.0
#    glMaterialfv(GL_FRONT, GL_AMBIENT, mat)
#    mat[0] = difr; mat[1] = difg; mat[2] = difb
#    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat)
#    mat[0] = specr; mat[1] = specg; mat[2] = specb
#    glMaterialfv(GL_FRONT, GL_SPECULAR, mat)
#    glMaterialf(GL_FRONT, GL_SHININESS, shine * 128.0)
#    # Linux GLUT seems to optimize away the actual generation of the
#    # teapot on modern GLUT when using display-lists, so we have to
#    # use direct calls here...
# #   glCallList(teapotList)
#    #glutSolidTeapot(1.0)
#    glRotatef(45, 1, 0, 0)
#    glutSolidCone(1.0, 2.0, 100, 100)
#    glPopMatrix()
#
# #function to render a "plane" given a position x and pos y
# def renderPlanes(intruder):
#     glPushMatrix()
#     color = [1,1,1,1]
#     glMaterialfv(GL_FRONT,GL_DIFFUSE,color)
#     glTranslatef(intruder.x, intruder.y, 0.0)
#     glRotatef(180, 0, 0, 1)
#     #glutSolidCone(1.0, 1.0, 100, 100)
#     glBegin(GL_TRIANGLES)
#     glVertex3f( 0.0, 1.0, 0.0)
#     glVertex3f(-1.0,-1.0, 0.0)
#     glVertex3f( 1.0,-1.0, 0.0)
#     glEnd()
#     glPopMatrix()
#
# def renderBoundaries(radius, depth, r, g, b):
#     posx, posy = 0,0
#     sides = 32
#     glPushMatrix()
#     color = [r,g,b,1]
#     glMaterialfv(GL_FRONT,GL_DIFFUSE,color)
#     glTranslatef(0, 0, depth)
#     glBegin(GL_POLYGON)
#     for i in range(100):
#       cosine = radius * cos(i*2*pi/sides) + posx
#       sine  = radius * sin(i*2*pi/sides) + posy
#       glVertex2f(cosine,sine)
#
#     glEnd()
#     glPopMatrix()
#
#
# #function to display
# def display():
#     init();
#     glClearColor( 1.0,1.0,1.0, 1.0)
#     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
#     glLoadIdentity()
#     #gluLookAt(0, 0, g_fViewDistance, 0, 0, 0, -.1, 0, 0)
#     #glMatrixMode(GL_PROJECTION)
#     #glLoadIdentity()
#     #gluPerspective(zoom, float(g_Width)/float(g_Height), g_nearPlane, g_farPlane)
#     #glMatrixMode(GL_MODELVIEW)
#     renderPlanes(plane1)
#     renderPlanes(plane2)
#     renderPlanes(plane0)
#
#     #rendering the first circle
#     renderBoundaries(10, -0.1, 0, 0, 0)
#     renderBoundaries(10.4, -0.2, 1, 1, 1)
#     #second circle
#     renderBoundaries(20, -.3, 0, 0, 0)
#     renderBoundaries(20.4, -.4, 1, 1, 1)
#     #thrid circle
#     renderBoundaries(30, -.5, 0, 0, 0)
#     renderBoundaries(30.4, -.6, 1, 1, 1)
#
#     glutSwapBuffers()
#
# def reshape(w, h):
#    glViewport(0, 0, w, h)
#    glMatrixMode(GL_PROJECTION)
#    glLoadIdentity()
#    if (w <= h):
#       glOrtho(0.0, 16.0, 0.0, 16.0*h/w, -10.0, 10.0)
#    else:
#       glOrtho(0.0, 16.0*w/h, 0.0, 16.0, -10.0, 10.0)
#    glMatrixMode(GL_MODELVIEW)
#
#
# def keyboard(key, x, y):
#   keysPressed[ord(key)] = True
#   if keysPressed[ord(key)]:
#       sys.exit(0)
#
# # Main Loop
# if __name__ == "__main__":
#     for i in range(256):
#       keysPressed.append(False)
#
#     glutInit(sys.argv);
#     glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
#     glutInitWindowSize(600, 600);
#     glutInitWindowPosition(50,50);
#     glutCreateWindow(sys.argv[0]);
#     #create some dumby planes here
#     plane1 = Intruder('1', 1, 5)
#     plane2 = Intruder('2', 1, 10)
#     plane0 = Intruder('0', 5, 5)
#     glutReshapeFunc(reshape)
#     glutDisplayFunc(display)
#     glutKeyboardFunc(keyboard)
#     glutMainLoop()