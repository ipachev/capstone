from OpenGL.GLUT import *
from functools import partial
import sys


press_triggers = [None for i in range(256)]
up_triggers = [None for i in range(256)]

up_triggers[27] = partial(sys.exit, 0)


def on_key_press(key, x, y):
    print("pressed key %d" % ord(key))
    global press_triggers
    func = press_triggers[ord(key)]
    if func:
        func()


def on_key_up(key, x, y):
    print("released key %d" % ord(key))
    global up_triggers
    func = up_triggers[ord(key)]
    if func:
        func()


def setup_keyboard():
    glutKeyboardFunc(on_key_press)
    glutKeyboardUpFunc(on_key_up)
