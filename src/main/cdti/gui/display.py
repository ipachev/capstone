from math import *
from main.cdti.Intruder import *
from OpenGL.GLUT import *
from OpenGL.GL import *


plane1 = Intruder('1', 1, 5)
plane2 = Intruder('2', 1, 10)
plane0 = Intruder('0', 5, 5)


def render_boundaries(radius, depth, r, g, b):
    posx, posy = 0,0
    sides = 32
    glPushMatrix()
    color = [r,g,b,1]
    glMaterialfv(GL_FRONT,GL_DIFFUSE,color)
    glTranslatef(0, 0, depth)
    glBegin(GL_POLYGON)
    for i in range(100):
        cosine = radius * cos(i*2*pi/sides) + posx
        sine = radius * sin(i*2*pi/sides) + posy
        glVertex2f(cosine, sine)

    glEnd()
    glPopMatrix()


# function to render a "plane" given a position x and pos y
def render_planes(intruder):
    glPushMatrix()
    color = [1,1,1,1]
    glMaterialfv(GL_FRONT,GL_DIFFUSE,color)
    glTranslatef(intruder.x, intruder.y, 0.0)
    glRotatef(180, 0, 0, 1)
    # glutSolidCone(1.0, 1.0, 100, 100)
    glBegin(GL_TRIANGLES)
    glVertex3f( 0.0, 1.0, 0.0)
    glVertex3f(-1.0,-1.0, 0.0)
    glVertex3f( 1.0,-1.0, 0.0)
    glEnd()
    glPopMatrix()


# Initialize depth buffer, projection matrix, light source, and lighting
# model.  Do not specify a material property here.
def init():
    ambient = [0.0, 0.0, 0.0, 1.0]
    diffuse = [1.0, 1.0, 1.0, 1.0]
    specular = [1.0, 1.0, 1.0, 1.0]
    position = [0.0, 3.0, 3.0, 0.0]

    lmodel_ambient = [0.2, 0.2, 0.2, 1.0]
    local_view = [0.0]

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse)
    glLightfv(GL_LIGHT0, GL_POSITION, position)
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient)
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, local_view)

    glFrontFace(GL_CW)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glEnable(GL_AUTO_NORMAL)
    glEnable(GL_NORMALIZE)
    glEnable(GL_DEPTH_TEST)

    #  be efficient--make teapot display list
    global teapotList
    teapotList = glGenLists(1)
    glNewList (teapotList, GL_COMPILE)
    glutSolidTeapot(1.0)

    glEndList ()


# function to display
def display():
    init()
    glClearColor( 1.0,1.0,1.0, 1.0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    # gluLookAt(0, 0, g_fViewDistance, 0, 0, 0, -.1, 0, 0)
    # glMatrixMode(GL_PROJECTION)
    # glLoadIdentity()
    # gluPerspective(zoom, float(g_Width)/float(g_Height), g_nearPlane, g_farPlane)
    # glMatrixMode(GL_MODELVIEW)
    render_planes(plane1)
    render_planes(plane2)
    render_planes(plane0)

    # rendering the first circle
    render_boundaries(10, -0.1, 0, 0, 0)
    render_boundaries(10.4, -0.2, 1, 1, 1)
    # second circle
    render_boundaries(20, -.3, 0, 0, 0)
    render_boundaries(20.4, -.4, 1, 1, 1)
    # thrid circle
    render_boundaries(30, -.5, 0, 0, 0)
    render_boundaries(30.4, -.6, 1, 1, 1)

    glutSwapBuffers()


def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    if w <= h:
        glOrtho(0.0, 16.0, 0.0, 16.0*h/w, -10.0, 10.0)
    else:
        glOrtho(0.0, 16.0*w/h, 0.0, 16.0, -10.0, 10.0)
    glMatrixMode(GL_MODELVIEW)




