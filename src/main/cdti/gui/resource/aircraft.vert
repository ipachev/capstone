#version 330

attribute vec3 vertex;
attribute vec2 texture_coordinate;

out vec2 fragment_texture_coordinate;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

void main() {
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertex, 1.0);
    fragment_texture_coordinate = texture_coordinate;
}