#version 330

out vec4 framebuffer_color;
in vec2 fragment_texture_coordinate;
uniform sampler2D sprite_sheet;

void main() {
    framebuffer_color = texture(sprite_sheet, fragment_texture_coordinate);
}