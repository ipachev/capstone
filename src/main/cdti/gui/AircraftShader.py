from collections import defaultdict
import vispy
from vispy import gloo, util, app
from vispy.gloo import Texture2D
from vispy.util.transforms import perspective, translate, rotate, ortho, scale
from main.cdti.gui.loader import *


icon_names = ['air traffic directional', 'air traffic non-directional', 'proximate traffic directional',
              'proximate traffic non-directional', 'traffic advisory directional', 'traffic advisory non-directional',
              'resolution advisory directional', 'resolution advisory non-directional', 'ownship normal',
              'ownship crash']

zoom_settings = ['out', 'default', 'in']


def normalize_numpy_array(arr):
    mag = np.linalg.norm(arr)
    return np.array([arr[i]/mag for i in range(len(arr))])


class PngSpriteTexture2D(Texture2D):
    def __init__(self, data, tex_format='rgba', internalformat='rgba', wrapping='clamp_to_edge'):
        super().__init__(data=data, format=tex_format, internalformat=internalformat, wrapping=wrapping,
                         interpolation='linear')


class ZoomSetting:
    def __init__(self, aircraft_scale, east_to_west_units, altitude, base_resolution=400):
        self.aircraft_scale = aircraft_scale
        self.east_to_west_units = east_to_west_units
        self.altitude = altitude
        self.base_resolution = base_resolution

    def units_to_pixels(self, units):
        return units*self.base_resolution/self.east_to_west_units

    def pixels_to_units(self, pixels):
        return pixels*self.east_to_west_units/self.base_resolution


class AircraftShader(vispy.gloo.Program):
    def __init__(self, vert_file="aircraft.vert", frag_file="aircraft.frag", sprite_sheet="cdti_icons.png"):
        super().__init__(load_text(vert_file), load_text(frag_file), count=4)
        self._raw_data = np.zeros(4, dtype=[('vertex', np.float32, 3), ('texture_coordinate', np.float32, 2)])
        self.sprite_sheet = PngSpriteTexture2D(load_texture_data(sprite_sheet))
        self.vertex_lookup = AircraftShader._load_vertex_data()
        self.texture_coordinates_lookup = AircraftShader._load_texture_coordinates()
        self.zoom_settings_lookup = AircraftShader._load_zoom_settings()

        self.camera_transform = None
        self.camera_position = None
        self.camera_target = None
        self.camera_up_direction = None
        self.camera_zoom = None
        self.is_orthopedic_projection = True
        self.perspective_transform = None
        self.window_size = None

        self.zoom_camera()
        self.move_camera()

    @staticmethod
    def _load_vertex_data():
        vertex_data = [(1, 1, 0), (-1, 1, 0), (-1, -1, 0), (1, -1, 0)]
        native_data = np.zeros((4, 3), np.float32)
        for i in range(4):
            x, y, z = vertex_data[i]
            native_data[i] = x, y, z
        result = defaultdict(lambda: native_data)
        return result

    @staticmethod
    def _load_texture_coordinates():
        icon_map = {}
        all_icon_coordinates = [((0, i*80/800), (1, i*80/800), (1, (79 + i*80)/800), (0, (79 + i*80)/800))
                                for i in range(10)]
        for i in range(10):
            texture_coordinates = list(all_icon_coordinates[i])
            native_data = np.zeros((4, 2), np.float32)
            for j in range(4):
                u, v = texture_coordinates[j]
                native_data[j] = v, u
            icon_map[icon_names[i]] = native_data
        return icon_map

    @staticmethod
    def _load_zoom_settings():
        default_zoom_setting = ZoomSetting(.75, 2.5, 100000)
        result = defaultdict(lambda: default_zoom_setting)
        result['in'] = ZoomSetting(1, 0.5, 100000)
        result['out'] = ZoomSetting(0.5, 40, 100000)
        return result

    def zoom_camera(self, zoom_config_name='default'):
        self.camera_zoom = self.zoom_settings_lookup[zoom_config_name]
        self.move_camera()

    def zoom_camera_special(self, zoom_config):
        self.camera_zoom = zoom_config

    def move_camera(self, position=None, look_at=None, up_direction=None):
        position_default = [0, 0, 1000]
        look_at_default = [0, 0, -1]
        up_direction_default = [0, 1, 0]

        if not position:
            if self.camera_position is not None:
                position = self.camera_position
            else:
                position = position_default

        if not look_at:
            if self.camera_target is not None:
                look_at = self.camera_target
            else:
                look_at = look_at_default

        if not up_direction:
            if self.camera_up_direction is not None:
                up_direction = self.camera_up_direction
            else:
                up_direction = up_direction_default

        self.update_camera_transform(position, look_at, up_direction)

    def draw_aircraft(self, aircraft_type, aircraft_location, aircraft_heading, aircraft_rotation_axis=[0, 0, -1]):
        self.prepare_shader(aircraft_type)

        width, height = self.window_size
        aircraft_size_in_units = self.camera_zoom.pixels_to_units(80)
        win_width_units = self.camera_zoom.pixels_to_units(width)
        win_height_units = self.camera_zoom.pixels_to_units(height)
        aspect_ratio = max(win_height_units, win_width_units)/min(win_height_units, win_width_units)
        ui_scale = aircraft_size_in_units*self.camera_zoom.aircraft_scale/2

        scale_model = scale([ui_scale*aspect_ratio, ui_scale, 1])
        rot = rotate(aircraft_heading, np.array(aircraft_rotation_axis, 'float32'))
        trans = translate(np.array([aircraft_location[i] for i in range(3)]), 'float32')

        self['model_matrix'] = np.mat(rot) * np.mat(scale_model) * np.mat(trans)

        self.draw('triangle_fan')

    def prepare_shader(self, aircraft_type):
        self['view_matrix'] = self.camera_transform
        self['projection_matrix'] = self.perspective_transform
        self['sprite_sheet'] = self.sprite_sheet

        self._raw_data['vertex'] = self.vertex_lookup[aircraft_type]
        self._raw_data['texture_coordinate'] = self.texture_coordinates_lookup[aircraft_type]
        self.bind(gloo.VertexBuffer(self._raw_data))

    def update_camera_transform(self, camera_position, look_at_position, up_vector):
        camera_position = np.array(camera_position)
        look_at_position = np.array(look_at_position)
        up_vector = np.array(up_vector)
        forward = normalize_numpy_array(np.subtract(look_at_position, camera_position))
        side = normalize_numpy_array(np.cross(forward, up_vector))
        up_vector = normalize_numpy_array(np.cross(side, forward))

        col0 = [side[0], up_vector[0], forward[0], 0]
        col1 = [side[1], up_vector[1], forward[1], 0]
        col2 = [side[2], up_vector[2], forward[2], 0]
        col3 = [0, 0, 0, 1]

        self.camera_transform = np.matrix([col0, col1, col2, col3], 'float32') * np.mat(translate(-1 * camera_position))
        self.camera_position = camera_position
        self.camera_target = look_at_position
        self.camera_up_direction = up_vector

    def update_perspective_transform(self, window_size, using_orthographic_projection=True):
        width, height = window_size
        view_width = self.camera_zoom.pixels_to_units(width)
        view_height = self.camera_zoom.pixels_to_units(height)
        aspect_ratio = max(width, height)/min(width, height)
        depth = self.camera_zoom.altitude
        if not using_orthographic_projection:
            frustum = util.transforms.frustum(-view_width/2, view_width/2,
                                              -view_height/2, view_height/2,
                                              depth, 0)
            result = np.mat(frustum) * np.mat(perspective(90, aspect_ratio, depth, 0))
        else:
            result = ortho(-view_width*aspect_ratio/2, view_width*aspect_ratio/2,
                           -view_height/2, view_height/2, depth, 0)
        self.perspective_transform = result
        self.window_size = (width, height)

