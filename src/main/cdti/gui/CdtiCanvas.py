from vispy import gloo, app
from main.cdti.gui.AircraftShader import AircraftShader, icon_names
import OpenGL.GL as gl


class CdtiCanvas(app.Canvas):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.context.set_sample_coverage(16)
        self.aircraft_shader = AircraftShader()
        self.aircraft_shader.zoom_camera()
        self.aircraft_list = [(icon_names[0], [0, 0, 500], 70),
                              (icon_names[2], [0, 12, 400], 40),
                              (icon_names[3], [-12, 0, 30], -40)]
        gl.glDisable(gl.GL_DITHER)

    def on_draw(self, event):
        gloo.set_state(clear_color=(0.0, 0.0, 0.0, 1.0), blend_func=('src_alpha', 'one_minus_src_alpha'), blend=True)

        gloo.clear()
        self.aircraft_shader.move_camera([0, 0, 0], [0, 0, -1])
        self.aircraft_shader.update_perspective_transform(self.size)

        for aircraft in self.aircraft_list:
            self.aircraft_shader.draw_aircraft(*aircraft)

    def on_resize(self, event):
        width, height = event.physical_size
        gloo.set_viewport(0, 0, width, height)
        self.aircraft_shader.update_perspective_transform(event.physical_size)
        self.update()
