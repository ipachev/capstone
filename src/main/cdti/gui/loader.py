import numpy as np
import pkg_resources
import os
from PIL import Image


def get_path(file_name):
    file_path = os.path.join('resource', file_name)
    return pkg_resources.resource_filename(__name__, file_path)


def load_bin(file_name):
    file_path = os.path.join('resource', file_name)
    return pkg_resources.resource_string(__name__, file_path)


def load_text(file_name):
    return load_bin(file_name).decode("utf-8")


def load_texture_data(file_name):
    pillow_image = Image.open(get_path(file_name))
    px = pillow_image.load()
    size = pillow_image.size
    pic_data = np.zeros((size[0], size[1], 4), np.float32)
    for y in range(size[1] - 1, -1, -1):
        for x in range(size[0] - 1, -1, -1):
            r, g, b, a = px[x, y]
            pic_data[x, y] = r/255, g/255, b/255, a/255

    return pic_data
