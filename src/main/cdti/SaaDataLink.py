from functools import partial
from socket import socket, AF_INET, SOCK_STREAM
import selectors
import protobuf.cdti_pb2 as _cdti


def no_op(*args, **kwargs):
    return args, kwargs


class SaaDataLink:
    def __init__(self, host='localhost', port=5000, buffer_size=8096, verbose=False):
        self.buffer_size = buffer_size
        self.buffer = bytearray(buffer_size)
        self.buffer_pos = 0
        self.selector = selectors.DefaultSelector()
        self.sock = socket(AF_INET, SOCK_STREAM)
        self.sock.connect((host, port))
        self.endpoint = self.sock.getsockname()
        self.selector.register(self.sock, selectors.EVENT_READ)
        self.is_connected = True

        self.verbose = verbose
        if self.verbose:
            self.log = partial(print, "[SAA Data Link {0}:{1}]".format(self.endpoint[0], self.endpoint[1]))
        else:
            self.log = no_op

    def saa_disconnected(self):
        self.log("Connection closed by SAA")
        self.selector.unregister(self.sock)
        self.sock.close()
        self.is_connected = False

    def recv(self):
        report_len = self.sock.recv(4)
        if len(report_len) == 0:
            self.saa_disconnected()
            return None
        size = int.from_bytes(report_len, byteorder='big', signed=False)
        report = _cdti.CDTIReport()
        raw_bytes = self.sock.recv(size)
        if len(raw_bytes) == 0:
            self.saa_disconnected()
            return None
        report.ParseFromString(raw_bytes)
        if self.verbose:
            print('############### START FROM SP #######################')
            print(report)
            print('############### END FROM SP #######################')
        return report
        # self.buffer_pos = len(self.buffer)
        # buffer = bytearray(self.buffer_size)
        # num_recv = self.sock.recv_into(buffer)
        # if num_recv > 0:
        #     self.buffer[self.buffer_pos:self.buffer_pos + num_recv] = buffer[:num_recv]
        #     self.buffer_pos += num_recv
        # elif num_recv == 0:
        #     self.saa_disconnected()

    def parse_buffer(self):
        if self.buffer_pos < 4:
            return None
        size = int.from_bytes(self.buffer[0:4], byteorder='big', signed=False)
        if self.buffer_pos < size + 4:
            return None
        report = _cdti.CDTIReport()
        report.ParseFromString(bytes(self.buffer[4:4+size]))
        self.log("New report from SAA", size, "bytes")

        unread_byte_count = self.buffer_pos - (size + 4)
        if unread_byte_count > 0:
            tmp = bytearray(self.buffer[size + 4:self.buffer_pos])
        self.buffer = bytearray(self.buffer_size)
        self.buffer_pos = 0
        if unread_byte_count > 0:
            self.buffer[0:unread_byte_count] = tmp[:]
            self.buffer_pos = unread_byte_count
            return self.parse_buffer() or report
        return report

    def read(self, timeout=0):
        if self.selector.select(timeout):
            return self.recv()
            # return self.parse_buffer()
        return None
