import json
import math
import os
import threading
import signal
import random
import time

import cherrypy
from main.common.MathHelpers import MathHelpers
from ws4py.messaging import TextMessage

import protobuf.cdti_pb2 as _cdti
from main.cdti.SaaDataLink import SaaDataLink
from main.cdti.ws.Socket import WebSocketHandler
from main.cdti.ws.server import Server, browser
from main.common.GeographicCoordinate import GeographicCoordinate
from main.common.Plane import Plane
from main.common.VectorMath import nautical_conversion_ft

'''
try:
    data_link = SaaDataLink()
except ConnectionRefusedError:
    data_link = False
    print('#### WARNING - Fake CDTI is Running ####')
'''


def send_message(message):
    cherrypy.engine.publish('websocket-broadcast', TextMessage(message))

def get_icon(plane):
    if plane.adsb_id == "0":
        return 'ownship normal'
    if get_speed(plane) > 0:
        directional = 'directional'
    else:
        directional = 'non-directional'
    if plane.severity == _cdti.CDTIPlane.RESOLUTION:
        return 'resolution advisory {0}'.format(directional)
    if plane.severity == _cdti.CDTIPlane.TRAFFIC:
        return 'traffic advisory {0}'.format(directional)
    if plane.severity == _cdti.CDTIPlane.PROXIMATE:
        return 'proximate traffic {0}'.format(directional)
    return 'air traffic {0}'.format(directional)


def recv_data():

    while True:
            print('...')
            create_fake_data()

#Creates a single fake plane to observe for testing purposes only
def create_fake_data():
    aircraft_list = [{'icon': 'ownship normal', 'position': (0, 0, 0),
                          'velocity': 1}]
    aircraft_list.append({'icon': 'air traffic directional', 'position': (random.random()*50, 0, 0),
                                  'velocity': 1,
                                  'relative_alt': -999,
                                  'id': 'fake plane 0'})
    aircraft_list.append({'icon': 'air traffic directional', 'position': (random.random()*10, 0, 0),
                                  'velocity': 1,
                                  'relative_alt': -999,
                                  'id': 'fake plane 1'})
    update_message = json.dumps(aircraft_list)
    send_message(update_message)
    time.sleep(1)


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.staticdir.root': os.path.join(os.path.abspath(os.path.dirname(__file__)), 'ws'),
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static',
            'tools.staticdir.index': './index.html'
        },
        '/ws': {
            'tools.websocket.on': True,
            'tools.websocket.handler_cls': WebSocketHandler
        }
    }
    recv = threading.Thread(target=recv_data, daemon=True)
    recv.start()
    browser.start()
    cherrypy.quickstart(Server(), '/', config=conf)


