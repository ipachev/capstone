function Camera() {
    var _pixelsPerMile = app.pixelsPerMile;
    var _centerCanvas = null;
    var _fieldOfView = null;
    var _x = 0;
    var _y = 0;
    var _angle = 0;

    this.minZoom = _pixelsPerMile*2;//_mToPx/0.75;
    this.maxZoom = _pixelsPerMile/2;//_mToPx/2;
    this.defaultZoom = _pixelsPerMile;

    var _cam = this;

    var _worldPointFromPixelPoint = function(pixelPoint) {
        var dyScreen = (app.canvas.height/2 - pixelPoint.y)/_pixelsPerMile;
        var dxScreen = (pixelPoint.x - app.canvas.width/2)/_pixelsPerMile;
        var d = Math.sqrt(dyScreen*dyScreen + dxScreen*dxScreen);
        var phi = Math.atan2(dyScreen, dxScreen) - _angle;
        return {
            x: _x + d*Math.cos(phi),
            y: _y + d*Math.sin(phi)
        };
    };

    Object.defineProperty(this, "fov", {
        get: function() {
            return {
                h:_fieldOfView.h, v: _fieldOfView.v
            };
        },
        enumerable: true,
        configurable: false
    });


    function _move(x, y) {
        _x = x;
        _y = y;
    }

    this.update = function(){

    };

    var _resizeCanvas = (function () {
        resetCanvasCenterPoint();
        resetFOV();
    }).bind(this);
    _resizeCanvas();



    Object.defineProperty(this, "move", {
        value: _move,
        writable: false,
        configurable: false,
        enumerable: true
    });

    Object.defineProperty(this, "center", {
        get: function() {
            return { x: _centerCanvas.x, y: _centerCanvas.y };
        },
        configurable: false,
        enumerable: true
    });

    Object.defineProperty(this, "x", {
        get: function() {
            return _x;
        },
        set: function(newX) {
            _move(newX, _y);
        },
        enumerable: true,
        configurable: true
    });

    Object.defineProperty(this, "y", {
        get: function() {
            return _y;
        },
        set: function(newY) {
            _move(_x, newY);
        },
        enumerable: true,
        configurable: true
    });

    Object.defineProperty(this, "milesPerPixel", {
        get: function() {
            return 1/_pixelsPerMile;
        },
        set: function(scale) {
            var camPos = _worldPointFromPixelPoint(_centerCanvas);
            _pixelsPerMile = 1/scale;
            _resizeCanvas();
            _move(camPos.x, camPos.y);
        },
        enumerable: true,
        configurable: false
    });

    Object.defineProperty(this, "pixelsPerMile", {
        get: function() {
            return _pixelsPerMile;
        },
        set: function(scale) {
            var camPos = _worldPointFromPixelPoint(_centerCanvas);
            _pixelsPerMile = scale;
            _resizeCanvas();
            _move(camPos.x, camPos.y);
        },
        enumerable: true,
        configurable: false
    });

    Object.defineProperty(this, "resizeCanvas", {
        value: _resizeCanvas,
        writable: false,
        configurable: false,
        enumerable: true
    });

    Object.defineProperty(this, "screenToWorld", {
        value: _worldPointFromPixelPoint,
        writable: false,
        configurable: false,
        enumerable: true
    });

    function resetFOV() {
        _fieldOfView = {
            h: app.canvas.width/_pixelsPerMile,
            v: app.canvas.height/_pixelsPerMile
        };
    }

    function resetCanvasCenterPoint() {
        _centerCanvas = {
            x: app.canvas.width/2,
            y: app.canvas.height/2
        };
    }
}