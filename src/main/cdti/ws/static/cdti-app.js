//this function will be called by index.html
function main()
{
    render();
}

//render all the components for the CDTI
function render() {
    drawBackground('black');
    var canvasCenter = app.camera.center;
    drawCompass();

    app.ctx.save() ;
        app.ctx.translate(canvasCenter.x, canvasCenter.y);
        console.log('canvas center: ' + canvasCenter.x + ' ' + canvasCenter.y)
        app.ctx.scale(1, -1);
        app.ctx.scale(app.camera.pixelsPerMile, app.camera.pixelsPerMile);
        console.log('camera angle: ' + app.camera.angle)
        //draw all the aircrafts
        drawAircrafts(app.aircrafts);
    app.ctx.restore();
    drawWarning();

}

//this function will output the message to the pilot
function drawWarning()
{
    textOutput = 'NOT A PLANE IN SIGHT';
    color = 'cyan'
    level = 0;
    minAlt = 100000;

    if(app.aircrafts.length > 1)
    for(var i = 1; i < app.aircrafts.length; i++)
    {
        console.log('The level is at ' + level);
        plane = app.aircrafts[i];
        altitude = app.aircrafts[i].relative_alt;

        advisoryLvl = String(plane.iconName);
        
        if(advisoryLvl.indexOf('air') > -1 && level == 0)
        {
            textOutput = 'TRAFFIC';
            color = 'cyan';
        }
        else if(advisoryLvl.indexOf('proximate') > -1 && level <= 1)
        {
            textOutput = 'TRAFFIC';
            color = 'cyan';
            level = 1;
        }
        else if(advisoryLvl.indexOf('traffic advisory') > -1 && level <= 2)
        {
            if(altitude > 0)
                textOutput = 'DO NOT CLIMB';
            else
                textOutput = 'DO NOT DESCEND';

            color = 'yellow';
            level = 2;
        }
        else if(advisoryLvl.indexOf('resolution') > -1 && level <= 3)
        {
            if(altitude < minAlt)
                minAlt = altitude;
            //got a resolution advisory
            if(minAlt > 0)
                textOutput = 'DESCEND 1500 fpm';
            else
                textOutput = 'CLIMB 1500 fpm';

            color = 'red';
            level = 3;
            i = app.aircrafts.length;
        }

        console.log('After plane - ' + String(plane.iconName) + ' the level is at ' + level);
    }//end of for

    app.ctx.save();
        app.ctx.fillStyle = color;
        boxHeight = app.canvas.height/12;
        boxWidth = app.canvas.width/4;
        xShift = 0//app.canvas.width/2 - app.canvas.width/8;
        yShift = 0;
        app.ctx.fillRect(xShift, yShift, boxWidth, boxHeight);
    app.ctx.restore();

    app.ctx.save();
        app.ctx.font = "bold 20px Arial";
        app.ctx.fillStyle = "black";
        app.ctx.fillText(textOutput, 50, boxHeight/2);
    app.ctx.restore();
}

function drawMessage(message, color)
{
    //do some shit
}

//set up the background for the CDTI
function drawBackground(color)
{
    app.ctx.save();
        app.ctx.fillStyle = color;
        app.ctx.fillRect(0, 0, app.canvas.width, app.canvas.height);
        console.log('canvas dimensions: ' + app.canvas.width + ' ' + app.canvas.height)
    app.ctx.restore();

}

//go through the list of aircrafts and draw
//each aircraft with the correct symbol
//
//----the first aircraft is ownship----
function drawAircrafts(aircrafts)
{
    var own_rot = app.aircrafts[0].angle;
    //go through all the aircrafts and draw them
    for (var i = 0; i < app.aircrafts.length; ++i)
    {

      var aircraft = app.aircrafts[i];
      var rotation = aircraft.angle - own_rot;
      app.ctx.save();
        app.ctx.translate(aircraft.y, aircraft.x);
        app.ctx.rotate(rotation);
        app.ctx.scale(app.pixelsPerAircraft/app.camera.pixelsPerMile, app.pixelsPerAircraft/app.camera.pixelsPerMile);
        //added this code to draw the alt and id
        if(i != 0)
        {
            //this is where we will draw the labels for the planes
            drawAircraftLabel(aircraft, own_rot);
        }
        //end of added code
        app.cdtiIcon.draw(aircraft.iconName);
      app.ctx.restore();
    }

}

/**
This function will take in a plane
and draw it's id and relative altitude
to the ownship

the aircraft to display
**/
function drawAircraftLabel(aircraft, ownship_rotation)
{
    var rotation = aircraft.angle - ownship_rotation;
    app.ctx.save();
        app.ctx.rotate(-rotation);
        app.ctx.rotate(3.14159);
        app.ctx.scale(-1, 1);
        app.ctx.font = "0.2px Arial";
        app.ctx.fillStyle = "white";
        //app.ctx.textAlign = "center";
        var textOutput = aircraft.id;
        //converts the alt to decimal
        var alt = (aircraft.relative_alt | 0);
        app.ctx.fillText(textOutput, 0.6, 0);
        app.ctx.fillText(alt, 0.6, .2);
    app.ctx.restore();
}

/**
This function will draw the compass for our CDTI
**/
function drawCompass()
{
    var canvasCenter = app.camera.center;
    app.ctx.save();
        app.ctx.scale(1,1);
        var compass = -Math.min(app.canvas.height, app.canvas.width)/2*0.95;
        app.ctx.textAlign = 'center';
        app.ctx.fillStyle = 'white';
        app.ctx.font = '50px Arial';
        app.ctx.textBaseline = 'middle';
        app.ctx.translate(canvasCenter.x, canvasCenter.y);

        for (var i = 0; i < 36; ++i)
        {
            drawCompassTick(i*Math.PI/16, compass);
        }

        
        //draw the NEWS symbols for the compass
        drawCompassDirection('N', 0, compass);
        drawCompassDirection('S', Math.PI, compass);
        drawCompassDirection('E', Math.PI/2, compass);
        drawCompassDirection('W', Math.PI*3/2, compass);

        drawRing(-compass);
        drawRing(-compass/3);
        drawRing(-compass*2/3);
    app.ctx.restore();
}

/**
The following functions are helpers for the  Compass function that are called within
the compass function to draw the rings, symbols and the mile read outs
**/
function drawRing(size) {
    app.ctx.save();
        //app.ctx.rotate(-app.camera.angle - Math.PI/2);
        app.ctx.beginPath();
        app.ctx.arc(0, 0, size, 0, 2 * Math.PI, false);
        app.ctx.strokeStyle = 'lightblue';
        app.ctx.lineWidth = 5;
        app.ctx.stroke();
    app.ctx.restore();
    drawRingLabel(size);
    
}

function drawRingLabel(size) {
    app.ctx.save();
        app.ctx.font = '20px Arial';
        app.ctx.fillStyle = 'lightgreen';
        //app.ctx.rotate(-app.camera.angle - Math.PI/2);
        app.ctx.translate(size, 0);
        var text = Math.round(size/app.camera.pixelsPerMile*10)/10 + " nmi";
        app.ctx.save();
            var textSize = app.ctx.measureText(text);
            var sidePadding = 2;
            app.ctx.beginPath();
            app.ctx.rect(-(textSize.width/2 + sidePadding), -10, textSize.width + 2*sidePadding, 20);
            app.ctx.fillStyle = '#333333';
            app.ctx.fill();
        app.ctx.restore();
        //app.ctx.rotate(Math.PI);
        app.ctx.fillText(text, 0, 0);
    app.ctx.restore();
}

function drawCompassDirection(direction, angle, compass) {
    app.ctx.save();
        app.ctx.rotate(angle - app.camera.angle - Math.PI/2);
        app.ctx.translate(0, compass);
        app.ctx.save();
            var textSize = app.ctx.measureText(direction);
            var sidePadding = 5;
            app.ctx.beginPath();
            app.ctx.rect(-(textSize.width/2 + sidePadding), -25, textSize.width + 2*sidePadding, 50);
            app.ctx.fillStyle = 'lightblue';
            app.ctx.fill();
        app.ctx.restore();
        app.ctx.fillText(direction, 0, 0);
    app.ctx.restore();
}

function drawCompassTick(angle, compass)
{
    app.ctx.save();
        app.ctx.rotate(angle - app.camera.angle - Math.PI/2);
        app.ctx.translate(0, compass);
        app.ctx.beginPath();
        app.ctx.moveTo(0, -10);
        app.ctx.lineTo(0, 10);
        app.ctx.strokeStyle = 'lightblue';
        app.ctx.lineWidth = 3;
        app.ctx.stroke();
    app.ctx.restore();
}


//this function creates a fake object
function createMockPlane()
{
    this.relative_alt = 0;
    this.id = 'FAKE';
    this.angle = Math.PI/2;
    this.x = 1;
    this.y = 1;
}

//create a list of fake planes to display
function createFakePlanes(numberOfPlanes)
{
    var aircrafts = [];

    for (var i = 0; i < numberOfPlanes; i++)
    {
        var mockPlane = new createMockPlane();
        mockPlane.x = Math.floor((Math.random() * 5));
        mockPlane.y = Math.floor((Math.random() * 5));
        aircrafts.push(mockPlane);
    }

    return aircrafts;
}


//#########################################################

/*
function main() {
    //app.camera.update();
    //app.camera.update();
    render();
    //window.requestAnimationFunc(main);
}

function render() {
    app.ctx.save();
        app.ctx.fillStyle = 'black';
        app.ctx.fillRect(0, 0, app.canvas.width, app.canvas.height);
        console.log('canvas dimensions: ' + app.canvas.width + ' ' + app.canvas.height)
    app.ctx.restore();

    var canvasCenter = app.camera.center;
    app.ctx.save();
        app.ctx.translate(canvasCenter.x, canvasCenter.y);
        console.log('canvas center: ' + canvasCenter.x + ' ' + canvasCenter.y)
        app.ctx.scale(1, -1);
        app.ctx.scale(app.camera.pixelsPerMile, app.camera.pixelsPerMile);
        //changed the - to +
//        app.ctx.rotate(app.camera.angle);
        console.log('camera angle: ' + app.camera.angle)
        //app.ctx.translate(app.camera.x, app.camera.y);
        //console.log('camera coords: ' + app.camera.x + ' ' + app.camera.y)


    var own_rot = app.aircrafts[0].angle;

    for (var i = 0; i < app.aircrafts.length; ++i)
    {

        var aircraft = app.aircrafts[i];
        var rotation = aircraft.angle - own_rot;
        app.ctx.save();
        app.ctx.translate(aircraft.y, aircraft.x);
        app.ctx.rotate(rotation);
        app.ctx.scale(app.pixelsPerAircraft/app.camera.pixelsPerMile, app.pixelsPerAircraft/app.camera.pixelsPerMile);
        //added this code to draw the alt and id
        if(i != 0)
        {
        //this is where we will draw the labels for the planes
        app.ctx.save();
            app.ctx.rotate(-rotation);
            app.ctx.rotate(3.14159);
            app.ctx.scale(-1, 1);
            app.ctx.font = "0.2px Arial";
            app.ctx.fillStyle = "red";
            //app.ctx.textAlign = "center";
            var textOutput = aircraft.id;
            //converts the alt to decimal
            var alt = (aircraft.relative_alt | 0);
            app.ctx.fillText(textOutput, 0.2, 0);
            app.ctx.fillText(alt, 0.2, .2);
        app.ctx.restore();
        }
        //end of added code
        app.cdtiIcon.draw(aircraft.iconName);


        app.ctx.restore();
    }

    app.ctx.restore();

    //this section draws all of the compass
    app.ctx.save();
        app.ctx.scale(1,1);
        var compass = -Math.min(app.canvas.height, app.canvas.width)/2*0.95;
        app.ctx.textAlign = 'center';
        app.ctx.fillStyle = 'white';
        app.ctx.font = '50px Arial';
        app.ctx.textBaseline = 'middle';
        app.ctx.translate(canvasCenter.x, canvasCenter.y);

        for (var i = 0; i < 36; ++i) {
            drawCompassTick(i*Math.PI/16);
        }

        drawRing(-compass);
        drawRing(-compass/3);
        drawRing(-compass*2/3);


        drawCompassDirection('N', 0);
        drawCompassDirection('S', Math.PI);
        drawCompassDirection('E', Math.PI/2);
        drawCompassDirection('W', Math.PI*3/2);

        function drawRing(size) {
            app.ctx.save();
                //app.ctx.rotate(-app.camera.angle - Math.PI/2);
                app.ctx.beginPath();
                app.ctx.arc(0, 0, size, 0, 2 * Math.PI, false);
                app.ctx.strokeStyle = 'lightblue';
                app.ctx.lineWidth = 5;
                app.ctx.stroke();
            app.ctx.restore();
            drawRingLabel(size);
        }

        function drawRingLabel(size) {
            app.ctx.save();
                app.ctx.font = '20px Arial';
                app.ctx.fillStyle = 'lightgreen';
                //app.ctx.rotate(-app.camera.angle - Math.PI/2);
                app.ctx.translate(size, 0);
                var text = Math.round(size/app.camera.pixelsPerMile*10)/10 + " nmi";
                app.ctx.save();
                    var textSize = app.ctx.measureText(text);
                    var sidePadding = 2;
                    app.ctx.beginPath();
                    app.ctx.rect(-(textSize.width/2 + sidePadding), -10, textSize.width + 2*sidePadding, 20);
                    app.ctx.fillStyle = '#333333';
                    app.ctx.fill();
                app.ctx.restore();
                //app.ctx.rotate(Math.PI);
                app.ctx.fillText(text, 0, 0);
            app.ctx.restore();
        }

        function drawCompassDirection(direction, angle) {
            app.ctx.save();
                app.ctx.rotate(angle - app.camera.angle - Math.PI/2);
                app.ctx.translate(0, compass);
                app.ctx.save();
                    var textSize = app.ctx.measureText(direction);
                    var sidePadding = 5;
                    app.ctx.beginPath();
                    app.ctx.rect(-(textSize.width/2 + sidePadding), -25, textSize.width + 2*sidePadding, 50);
                    app.ctx.fillStyle = 'lightblue';
                    app.ctx.fill();
                app.ctx.restore();
                app.ctx.fillText(direction, 0, 0);
            app.ctx.restore();
        }

        function drawCompassTick(angle) {
            app.ctx.save();
                app.ctx.rotate(angle - app.camera.angle - Math.PI/2);
                app.ctx.translate(0, compass);
                app.ctx.beginPath();
                app.ctx.moveTo(0, -10);
                app.ctx.lineTo(0, 10);
                app.ctx.strokeStyle = 'lightblue';
                app.ctx.lineWidth = 3;
                app.ctx.stroke();
            app.ctx.restore();
        }

    app.ctx.restore();
}*/