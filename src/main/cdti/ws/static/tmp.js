function Camera() {
    var _pixelsPerMile = app.pixelsPerMile;
    var _centerCanvas = null;
    var _fieldOfView = null;
    var _x = 0;
    var _y = 0;
    var _angle = 0;

    this.minZoom = _pixelsPerMile*2;//_mToPx/0.75;
    this.maxZoom = _pixelsPerMile/2;//_mToPx/2;
    this.defaultZoom = _pixelsPerMile;

    var _cam = this;

    var _worldPointFromPixelPoint = function(pixelPoint) {
        var dyScreen = (app.canvas.height/2 - pixelPoint.y)/_pixelsPerMile;
        var dxScreen = (pixelPoint.x - app.canvas.width/2)/_pixelsPerMile;
        var d = Math.sqrt(dyScreen*dyScreen + dxScreen*dxScreen);
        var phi = Math.atan2(dyScreen, dxScreen) - _angle;
        return {
            x: _x + d*Math.cos(phi),
            y: _y + d*Math.sin(phi)
        };
    };

    Object.defineProperty(this, "fov", {
        get: function() {
            return {
                h:_fieldOfView.h, v: _fieldOfView.v
            };
        },
        enumerable: true,
        configurable: false
    });


    function _move(x, y) {
        _x = x;
        _y = y;
    }

    this.update = function(){

    };

    var _resizeCanvas = (function () {
        resetCanvasCenterPoint();
        resetFOV();
    }).bind(this);
    _resizeCanvas();



    Object.defineProperty(this, "move", {
        value: _move,
        writable: false,
        configurable: false,
        enumerable: true
    });

    Object.defineProperty(this, "center", {
        get: function() {
            return { x: _centerCanvas.x, y: _centerCanvas.y };
        },
        configurable: false,
        enumerable: true
    });

    Object.defineProperty(this, "x", {
        get: function() {
            return _x;
        },
        set: function(newX) {
            _move(newX, _y);
        },
        enumerable: true,
        configurable: true
    });

    Object.defineProperty(this, "y", {
        get: function() {
            return _y;
        },
        set: function(newY) {
            _move(_x, newY);
        },
        enumerable: true,
        configurable: true
    });

    Object.defineProperty(this, "milesPerPixel", {
        get: function() {
            return 1/_pixelsPerMile;
        },
        set: function(scale) {
            var camPos = _worldPointFromPixelPoint(_centerCanvas);
            _pixelsPerMile = 1/scale;
            _resizeCanvas();
            _move(camPos.x, camPos.y);
        },
        enumerable: true,
        configurable: false
    });

    Object.defineProperty(this, "pixelsPerMile", {
        get: function() {
            return _pixelsPerMile;
        },
        set: function(scale) {
            var camPos = _worldPointFromPixelPoint(_centerCanvas);
            _pixelsPerMile = scale;
            _resizeCanvas();
            _move(camPos.x, camPos.y);
        },
        enumerable: true,
        configurable: false
    });

    Object.defineProperty(this, "resizeCanvas", {
        value: _resizeCanvas,
        writable: false,
        configurable: false,
        enumerable: true
    });

    Object.defineProperty(this, "screenToWorld", {
        value: _worldPointFromPixelPoint,
        writable: false,
        configurable: false,
        enumerable: true
    });

    function resetFOV() {
        _fieldOfView = {
            h: app.canvas.width/_pixelsPerMile,
            v: app.canvas.height/_pixelsPerMile
        };
    }

    function resetCanvasCenterPoint() {
        _centerCanvas = {
            x: app.canvas.width/2,
            y: app.canvas.height/2
        };
    }
}function main() {
    //app.camera.update();
    //app.camera.update();
    render();
    //window.requestAnimationFunc(main);
}

function render() {
    app.ctx.save();
        app.ctx.fillStyle = 'black';
        app.ctx.fillRect(0, 0, app.canvas.width, app.canvas.height);
    app.ctx.restore();


    var canvasCenter = app.camera.center;
    app.ctx.save();

        app.ctx.translate(canvasCenter.x, canvasCenter.y);
        app.ctx.scale(1, -1);
        app.ctx.scale(app.camera.pixelsPerMile, app.camera.pixelsPerMile);
        app.ctx.rotate(-app.camera.angle);
        app.ctx.translate(-app.camera.x, -app.camera.y);

        for (var i = 0; i < app.aircrafts.length; ++i) {
            var aircraft = app.aircrafts[i];
            app.ctx.save();
                app.ctx.translate(aircraft.x, aircraft.y);
                drawPlaneLabel("hallo")
                if (aircraft.angle) {

                    app.ctx.rotate(-aircraft.angle);
                }
                app.ctx.scale(app.pixelsPerAircraft/app.camera.pixelsPerMile, app.pixelsPerAircraft/app.camera.pixelsPerMile);
                app.cdtiIcon.draw(aircraft.iconName);
            app.ctx.restore();
        }
    app.ctx.restore();

    app.ctx.save();
        app.ctx.scale(1,1);
        var compass = -Math.min(app.canvas.height, app.canvas.width)/2*0.95;
        app.ctx.textAlign = 'center';
        app.ctx.fillStyle = 'white';
        app.ctx.font = '50px Arial';
        app.ctx.textBaseline = 'middle';
        app.ctx.translate(canvasCenter.x, canvasCenter.y);

        for (var i = 0; i < 36; ++i) {
            drawCompassTick(i*Math.PI/16);
        }

        drawRing(-compass);
        drawRing(-compass/3);
        drawRing(-compass*2/3);


        drawCompassDirection('N', 0);
        drawCompassDirection('S', Math.PI);
        drawCompassDirection('E', Math.PI/2);
        drawCompassDirection('W', Math.PI*3/2);


        function drawRing(size) {
            app.ctx.save();
                //app.ctx.rotate(-app.camera.angle - Math.PI/2);
                app.ctx.beginPath();
                app.ctx.arc(0, 0, size, 0, 2 * Math.PI, false);
                app.ctx.strokeStyle = 'lightblue';
                app.ctx.lineWidth = 5;
                app.ctx.stroke();
            app.ctx.restore();
            drawRingLabel(size);
        }

        function drawRingLabel(size) {
            app.ctx.save();
                app.ctx.font = '20px Arial';
                app.ctx.fillStyle = 'lightgreen';
                //app.ctx.rotate(-app.camera.angle - Math.PI/2);
                app.ctx.translate(size, 0);
                var text = Math.round(size/app.camera.pixelsPerMile*10)/10 + " nmi";
                app.ctx.save();
                    var textSize = app.ctx.measureText(text);
                    var sidePadding = 2;
                    app.ctx.beginPath();
                    app.ctx.rect(-(textSize.width/2 + sidePadding), -10, textSize.width + 2*sidePadding, 20);
                    app.ctx.fillStyle = '#333333';
                    app.ctx.fill();
                app.ctx.restore();
                //app.ctx.rotate(Math.PI);
                app.ctx.fillText(text, 0, 0);
            app.ctx.restore();
        }

        function drawPlaneLabel(size, text) {
            app.ctx.save();
                app.ctx.font = '20px Arial';
                app.ctx.fillStyle = 'lightgreen';
                //app.ctx.rotate(-app.camera.angle - Math.PI/2);
                app.ctx.translate(80, 0);
                app.ctx.save();
                    var textSize = app.ctx.measureText(text);
                    var sidePadding = 2;
                    app.ctx.beginPath();
                    app.ctx.rect(-(textSize.width/2 + sidePadding), -10, textSize.width + 2*sidePadding, 20);
                    app.ctx.fillStyle = '#333333';
                    app.ctx.fill();
                app.ctx.restore();
                //app.ctx.rotate(Math.PI);
                app.ctx.fillText(text, 0, 0);
            app.ctx.restore();
        }

        function drawCompassDirection(direction, angle) {
            app.ctx.save();
                app.ctx.rotate(angle - app.camera.angle - Math.PI/2);
                app.ctx.translate(0, compass);
                app.ctx.save();
                    var textSize = app.ctx.measureText(direction);
                    var sidePadding = 5;
                    app.ctx.beginPath();
                    app.ctx.rect(-(textSize.width/2 + sidePadding), -25, textSize.width + 2*sidePadding, 50);
                    app.ctx.fillStyle = 'lightblue';
                    app.ctx.fill();
                app.ctx.restore();
                app.ctx.fillText(direction, 0, 0);
            app.ctx.restore();
        }

        function drawCompassTick(angle) {
            app.ctx.save();
                app.ctx.rotate(angle - app.camera.angle - Math.PI/2);
                app.ctx.translate(0, compass);
                app.ctx.beginPath();
                app.ctx.moveTo(0, -10);
                app.ctx.lineTo(0, 10);
                app.ctx.strokeStyle = 'lightblue';
                app.ctx.lineWidth = 3;
                app.ctx.stroke();
            app.ctx.restore();
        }

        //app.ctx.save();
        //    app.ctx.rotate(Math.PI + -app.camera.angle - Math.PI/2);
        //    app.ctx.translate(0, compass);
        //    app.ctx.fillText('S', 0, 0);
        //app.ctx.restore();
        //app.ctx.save();
        //    app.ctx.rotate(Math.PI/2 + -app.camera.angle - Math.PI/2);
        //    app.ctx.translate(0, compass);
        //    app.ctx.fillText('E', 0, 0);
        //app.ctx.restore();
        //    app.ctx.save();
        //    app.ctx.rotate(3*Math.PI/2 + -app.camera.angle - Math.PI/2);
        //    app.ctx.translate(0, compass);
        //    app.ctx.fillText('W', 0, 0);
        //app.ctx.restore();
    app.ctx.restore();
}function CdtiSocket() {
    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser!");

        var ws = new WebSocket("ws://localhost:9152/ws");

        ws.onopen = function () {
            ws.send("start");
        };

        ws.onmessage = function (evt) {
            var received_msg = evt.data;
            //console.log(received_msg);
            app.updateModel(JSON.parse(received_msg));
        };

        ws.onclose = function () {
            console.log("Connection is closed...");
        };
        return ws;
    }

    else {
        alert("WebSocket NOT supported by your Browser!");
    }
}

function InitCdti() {
    var _ctx;
    var _numUnloaded = 0;
    
    function App() {
        this.aircrafts = [];
        Object.defineProperty(this, "CANVAS_ID", {
            value: "cdti",
            writable: false,
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(this, "canvas", {
            get: function() { return document.getElementById(app.CANVAS_ID); },
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(this, "ctx", {
            get: function() { return _ctx; },
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(this, "resetGraphics", {
            value: resetGraphicsContexts,
            enumerable: true,
            writable: false,
            configurable: false
        });

        Object.defineProperty(this, "pixelsPerAircraft", {
            value: 80,
            enumerable: true,
            writable: true,
            configurable: false
        });

        Object.defineProperty(this, "pixelsPerMile", {
            value: 80,
            enumerable: true,
            writable: true,
            configurable: false
        });

        Object.defineProperty(this, "resetGraphics", {
            value: resetGraphicsContexts,
            enumerable: true,
            writable: false,
            configurable: false
        });
        
        Object.defineProperty(this, "now", {
            value: timeNow,
            enumerable: true,
            writable: false,
            configurable: false
        });
        
        Object.defineProperty(this, "cache", {
            value: {},
            enumerable: true,
            writable: false,
            configurable: false
        });
        
        Object.defineProperty(this, "loadImage", {
            value: loadImageAsset,
            enumerable: true,
            writable: false,
            configurable: false
        });
        this.updateModel = updateModel;
        this.ws = CdtiSocket();
    }

    function updateModel(aircrafts) {
        function Aircraft(id, x, y, angle, alt, icon) {
            this.x = x;
            this.y = y;
            this.angle = angle;
            this.iconName = icon;
            this.relative_alt = alt;
            this.id = id;
        }
        var results = [];
        for (var i = 0; i < aircrafts.length; ++i) {
            results[i] = new Aircraft(aircrafts[i].id, aircrafts[i].position[0], aircrafts[i].position[1],
                                      aircrafts[i].relative_alt, Math.PI/180*aircrafts[i].velocity, aircrafts[i].icon)
        }
        app.aircrafts = results;
        app.camera.angle = -results[0].angle;
        //results[0].angle = 0;
        window.main();
    }
    
    function timeNow() {
        return (new Date()).getTime();
    }

    function getWindowDimension(dim) {
        dim = dim.charAt(0).toUpperCase() + dim.slice(1).toLowerCase();
        var tmp = document.documentElement["client" + dim];
        return Math.max(tmp , window["inner"+dim] || 0);
    }

    function resetCanvas() {
        var canvas = document.getElementById(app.CANVAS_ID);
        if (canvas) {
            canvas.parentNode.removeChild(canvas);
        }
        
        canvas = document.createElement("canvas");
        canvas.id = app.CANVAS_ID;
        canvas.width = getWindowDimension("width");
        canvas.height = getWindowDimension("height");
        canvas.style.position = "absolute";
        canvas.style.top = "0";
        canvas.style.left = "0";
        document.body.appendChild(canvas);
        _ctx = canvas.getContext("2d");
        resetGraphicsContexts();
        
        if (app.camera) {
            app.camera.resizeCanvas();
        }
    }
    
    function resetGraphicsContexts() {
        _ctx.fillStyle = "#000000";
        _ctx.font = "20px sans-serif";
        _ctx.globalAlpha = 1;
        _ctx.globalCompositeOperation = "source-over";
        _ctx.imageSmoothingEnabled = true;
        _ctx.lineCap = "butt";
        _ctx.lineDashOffset = 0;
        _ctx.lineJoin = "miter";
        _ctx.lineWidth = 1;
        _ctx.miterLimit = 10;
        _ctx.shadowBlur = 0;
        _ctx.shadowColor = "rgba(0, 0, 0, 0)";
        _ctx.shadowOffsetX = 0;
        _ctx.shadowOffsetY = 0;
        _ctx.strokeStyle = "#000000";
        _ctx.textAlign = "start";
        _ctx.textBaseline = "alphabetic";
        _ctx.setTransform(1, 0, 0, 1, 0, 0);
        Object.defineProperty(app, "camera", {
            value: new Camera(),
            enumerable: true,
            writable: true,
            configurable: false
        });
    }
    
    function loadImageAsset(path) {
        ++_numUnloaded;
        var i = new Image();
        i.addEventListener("load", function(e){
            app.cache[path] = i;
            if (--_numUnloaded === 0) {    
                afterAssetsLoad();
            }
        });
        i.src = path;
    }
    
    window.app = new App();
    
    window.addEventListener("resize", resetCanvas);
    
    window.addEventListener('orientationchange', resetCanvas);
    resetCanvas();
};

function loadAssets() {
    app.loadImage("/static/cdti_icons.png");
}

function CdtiIcons(image, imageCount) {
    if (imageCount <= 0) throw "imageCount can not be <= 0";
    var _iconNames = ['air traffic directional', 'air traffic non-directional', 'proximate traffic directional',
              'proximate traffic non-directional', 'traffic advisory directional', 'traffic advisory non-directional',
              'resolution advisory directional', 'resolution advisory non-directional', 'ownship normal',
              'ownship crash']
    var _iconIndex = {};
    for (var i = 0; i < _iconNames.length; ++i) {
        _iconIndex[_iconNames[i]] = i;
    }

    var _image = image;
    var _frameCount = imageCount;
    var _iconWidth = _image.width;
    var _iconHeight = _image.height / imageCount;
    Object.defineProperty(this, "imageCount", {
        get: function() { return _frameCount; },
        configurable: true,
        enumerable: true
    });
    Object.defineProperty(this, "width", {
        value: 1,//_iconWidth,
        configurable: true,
        enumerable: true
    });
    Object.defineProperty(this, "height", {
        value: 1,//_iconHeight,
        configurable: true,
        enumerable: true
    });

    function _drawImage(iconName) {
        var srcY = _iconHeight * _iconIndex[iconName];
        var posX = -this.width/2;
        var posY = -this.height/2;
        app.ctx.scale(1, -1);
        app.ctx.drawImage(_image, 0, srcY, _iconWidth, _iconHeight, posX, posY, this.width, this.height);
    }
    this.draw = _drawImage.bind(this);
}

function afterAssetsLoad() {
    app.cdtiIcon = new CdtiIcons(app.cache["/static/cdti_icons.png"], 10);
    setupInput();
    main();
}function setupInput() {
    var _lastEvent = getTimeStamp();
    var _lastUpdate = 0;
    var _keys = new Array(200); fillFalse(_keys);
    var _mouseButtons = new Array(20); fillFalse(_mouseButtons);
    var _mousePos = { x: app.canvas ? app.canvas.width/2 : 0, y: 0 };

    document.addEventListener("mousewheel", zoomCamera, false);
    document.addEventListener("DOMMouseScroll", zoomCamera, false);

    window.addEventListener("keydown", registerKey, true);
    window.addEventListener("keyup", deregisterKey, true);
    window.addEventListener("mousemove", registerMousePos, true);
    window.addEventListener("mousedown", registerMouseDown, true);
    window.addEventListener("mouseup", deregisterMouseDown, true);
    window.addEventListener("contextmenu", noAction, false);

    window.addEventListener("touchstart", noAction);
    window.addEventListener("touchmove", noAction);
    window.addEventListener("touchend", noAction);
    window.addEventListener("touchcancel", noAction);

    function noAction(e) {
        e.preventDefault();
        return false;
    }

    function makeCharCodeArray(str) {
        str = str.toUpperCase();
        var len = str.length + arguments.length - 1;
        var arr = new Array(len);
        var i = 0;

        for (; i < str.length; ++i) {
            arr[i] = str.charCodeAt(i);
        }

        var k = 1;
        while (i < len) {
            arr[i++] = arguments[k++];
        }

        return arr;
    }

    function isKeyDown(arrCodes) {
        var len = arrCodes.length;
        var i = 0;
        while (i < len) {
            if (_keys[arrCodes[i++]]) {
                return true;
            }
        }
        return false;
    }

    function zoomCamera(e) {
        console.log("zomming!");
        var e = window.event || e; // old IE support
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        if (delta < 0) {
            app.camera.pixelsPerMile = (app.camera.pixelsPerMile/1.05);
        }
        else {
            app.camera.pixelsPerMile = (app.camera.pixelsPerMile*1.05);
        }
        main();
    }

    function registerMousePos(e) {
        _mousePos.x = e.clientX;
        _mousePos.y = e.clientY;
        _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
    }

    function registerMouseDown(e) {
        e.preventDefault();
        _mouseButtons[e.which] = true;
        _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
    }

    function deregisterMouseDown(e) {
        e.preventDefault();
        _mouseButtons[e.which] = false;
        _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
    }

    function registerKey(e) {
        _keys[e.which] = true;
        if (e.repeat) {
            e.preventDefault();
        }
        else {
            _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
        }
    }

    function deregisterKey(e) {
        _keys[e.which] = false;
        if (e.repeat) {
            e.preventDefault();
        }
        else {
            _lastEvent = getTimeStamp();
        }
    }

    function fillFalse(arr) {
        var len = arr.length;
        var i = 0;
        while (i < len) {
            _keys[i++] = false;
        }
    }

    function getTimeStamp() {
        return (new Date()).getTime();
    }
}