function setupInput() {
    var _lastEvent = getTimeStamp();
    var _lastUpdate = 0;
    var _keys = new Array(200); fillFalse(_keys);
    var _mouseButtons = new Array(20); fillFalse(_mouseButtons);
    var _mousePos = { x: app.canvas ? app.canvas.width/2 : 0, y: 0 };

    document.addEventListener("mousewheel", zoomCamera, false);
    document.addEventListener("DOMMouseScroll", zoomCamera, false);
    window.addEventListener("keyup", zoomKeys, true);

    window.addEventListener("keydown", registerKey, true);
    window.addEventListener("keyup", deregisterKey, true);
    window.addEventListener("mousemove", registerMousePos, true);
    window.addEventListener("mousedown", registerMouseDown, true);
    window.addEventListener("mouseup", deregisterMouseDown, true);
    window.addEventListener("contextmenu", noAction, false);

    window.addEventListener("touchstart", noAction);
    window.addEventListener("touchmove", noAction);
    window.addEventListener("touchend", noAction);
    window.addEventListener("touchcancel", noAction);

    window.addEventListener("keypress", registerKey, true);

    function noAction(e) {
        e.preventDefault();
        return false;
    }

    function zoomKeys(e) {
        console.log("zoomKeys!")
        if (e.keyCode == 38) {
            app.camera.pixelsPerMile = (app.camera.pixelsPerMile*1.05);
        }
        if (e.keyCode == 40) {
            app.camera.pixelsPerMile = (app.camera.pixelsPerMile/1.05);
        }
        main();
     }

    function makeCharCodeArray(str) {
        str = str.toUpperCase();
        var len = str.length + arguments.length - 1;
        var arr = new Array(len);
        var i = 0;

        for (; i < str.length; ++i) {
            arr[i] = str.charCodeAt(i);
        }

        var k = 1;
        while (i < len) {
            arr[i++] = arguments[k++];
        }

        return arr;
    }

    function isKeyDown(arrCodes) {
        var len = arrCodes.length;
        var i = 0;
        while (i < len) {
            if (_keys[arrCodes[i++]]) {
                return true;
            }
        }
        return false;
    }

    function zoomCamera(e) {
        console.log("zomming!");
        var e = window.event || e; // old IE support
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        if (delta < 0) {
            app.camera.pixelsPerMile = (app.camera.pixelsPerMile/1.05);
        }
        else {
            app.camera.pixelsPerMile = (app.camera.pixelsPerMile*1.05);
        }
        main();
    }

    function registerMousePos(e) {
        _mousePos.x = e.clientX;
        _mousePos.y = e.clientY;
        _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
    }

    function registerMouseDown(e) {
        e.preventDefault();
        _mouseButtons[e.which] = true;
        _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
    }

    function deregisterMouseDown(e) {
        e.preventDefault();
        _mouseButtons[e.which] = false;
        _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
    }

    function registerKey(e) {
        _keys[e.which] = true;
        if (e.repeat) {
            e.preventDefault();
        }
        else {
            _lastEvent = e.timeStamp ? e.timeStamp : getTimeStamp();
        }
    }

    function deregisterKey(e) {
        _keys[e.which] = false;
        if (e.repeat) {
            e.preventDefault();
        }
        else {
            _lastEvent = getTimeStamp();
        }
    }

    function fillFalse(arr) {
        var len = arr.length;
        var i = 0;
        while (i < len) {
            _keys[i++] = false;
        }
    }

    function getTimeStamp() {
        return (new Date()).getTime();
    }
}