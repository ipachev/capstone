function CdtiSocket() {
    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser!");

        var ws = new WebSocket("ws://localhost:9152/ws");

        ws.onopen = function () {
            ws.send("start");
        };

        ws.onmessage = function (evt) {
            var received_msg = evt.data;
            console.log(received_msg);
            app.updateModel(JSON.parse(received_msg));
        };

        ws.onclose = function () {
            console.log("Connection is closed...");
        };
        return ws;
    }

    else {
        alert("WebSocket NOT supported by your Browser!");
    }
}

