function InitCdti() {
    var _ctx;
    var _numUnloaded = 0;
    
    function App() {
        this.aircrafts = [];
        Object.defineProperty(this, "CANVAS_ID", {
            value: "cdti",
            writable: false,
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(this, "canvas", {
            get: function() { return document.getElementById(app.CANVAS_ID); },
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(this, "ctx", {
            get: function() { return _ctx; },
            enumerable: true,
            configurable: false
        });

        Object.defineProperty(this, "resetGraphics", {
            value: resetGraphicsContexts,
            enumerable: true,
            writable: false,
            configurable: false
        });

        Object.defineProperty(this, "pixelsPerAircraft", {
            value: 80,
            enumerable: true,
            writable: true,
            configurable: false
        });

        Object.defineProperty(this, "pixelsPerMile", {
            value: 80,
            enumerable: true,
            writable: true,
            configurable: false
        });

        Object.defineProperty(this, "resetGraphics", {
            value: resetGraphicsContexts,
            enumerable: true,
            writable: false,
            configurable: false
        });
        
        Object.defineProperty(this, "now", {
            value: timeNow,
            enumerable: true,
            writable: false,
            configurable: false
        });
        
        Object.defineProperty(this, "cache", {
            value: {},
            enumerable: true,
            writable: false,
            configurable: false
        });
        
        Object.defineProperty(this, "loadImage", {
            value: loadImageAsset,
            enumerable: true,
            writable: false,
            configurable: false
        });
        this.updateModel = updateModel;
        this.aircrafts = [new Aircraft(0, 0, Math.PI/180*0, 'ownship normal', 'OWNSHIP', 0)]
        this.ws = CdtiSocket();
    }

    function Aircraft(x, y, angle, icon, id, relative_alt) {
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.iconName = icon;
        this.id = id;
        this.relative_alt = relative_alt
        console.log("made aircarft with ", this.id, this.relative_alt, this.iconName)
    }

    function updateModel(aircrafts) {

        var results = [];
        for (var i = 0; i < aircrafts.length; ++i) {
            results[i] = new Aircraft(aircrafts[i].position[0], aircrafts[i].position[1], Math.PI/180*aircrafts[i].velocity, aircrafts[i].icon, aircrafts[i].id, aircrafts[i].relative_alt)
        }
        app.aircrafts = results;
        app.camera.angle = -results[0].angle;
        //results[0].angle = 0;
        window.main();
    }
    
    function timeNow() {
        return (new Date()).getTime();
    }

    function getWindowDimension(dim) {
        dim = dim.charAt(0).toUpperCase() + dim.slice(1).toLowerCase();
        var tmp = document.documentElement["client" + dim];
        return Math.max(tmp , window["inner"+dim] || 0);
    }

    function resetCanvas() {
        var canvas = document.getElementById(app.CANVAS_ID);
        if (canvas) {
            canvas.parentNode.removeChild(canvas);
        }
        
        canvas = document.createElement("canvas");
        canvas.id = app.CANVAS_ID;
        canvas.width = getWindowDimension("width");
        canvas.height = getWindowDimension("height");
        canvas.style.position = "absolute";
        canvas.style.top = "0";
        canvas.style.left = "0";
        document.body.appendChild(canvas);
        _ctx = canvas.getContext("2d");
        resetGraphicsContexts();
        
        if (app.camera) {
            app.camera.resizeCanvas();
        }
    }
    
    function resetGraphicsContexts() {
        _ctx.fillStyle = "#000000";
        _ctx.font = "20px sans-serif";
        _ctx.globalAlpha = 1;
        _ctx.globalCompositeOperation = "source-over";
        _ctx.imageSmoothingEnabled = true;
        _ctx.lineCap = "butt";
        _ctx.lineDashOffset = 0;
        _ctx.lineJoin = "miter";
        _ctx.lineWidth = 1;
        _ctx.miterLimit = 10;
        _ctx.shadowBlur = 0;
        _ctx.shadowColor = "rgba(0, 0, 0, 0)";
        _ctx.shadowOffsetX = 0;
        _ctx.shadowOffsetY = 0;
        _ctx.strokeStyle = "#000000";
        _ctx.textAlign = "start";
        _ctx.textBaseline = "alphabetic";
        _ctx.setTransform(1, 0, 0, 1, 0, 0);
        Object.defineProperty(app, "camera", {
            value: new Camera(),
            enumerable: true,
            writable: true,
            configurable: false
        });
    }
    
    function loadImageAsset(path) {
        ++_numUnloaded;
        var i = new Image();
        i.addEventListener("load", function(e){
            app.cache[path] = i;
            if (--_numUnloaded === 0) {    
                afterAssetsLoad();
            }
        });
        i.src = path;
    }
    
    window.app = new App();
    
    window.addEventListener("resize", resetCanvas);
    
    window.addEventListener('orientationchange', resetCanvas);
    resetCanvas();
};

function loadAssets() {
    app.loadImage("/static/cdti_icons.png");
}

function CdtiIcons(image, imageCount) {
    if (imageCount <= 0) throw "imageCount can not be <= 0";
    var _iconNames = ['air traffic directional', 'air traffic non-directional', 'proximate traffic directional',
              'proximate traffic non-directional', 'traffic advisory directional', 'traffic advisory non-directional',
              'resolution advisory directional', 'resolution advisory non-directional', 'ownship normal',
              'ownship crash']
    var _iconIndex = {};
    for (var i = 0; i < _iconNames.length; ++i) {
        _iconIndex[_iconNames[i]] = i;
    }

    var _image = image;
    var _frameCount = imageCount;
    var _iconWidth = _image.width;
    var _iconHeight = _image.height / imageCount;
    Object.defineProperty(this, "imageCount", {
        get: function() { return _frameCount; },
        configurable: true,
        enumerable: true
    });
    Object.defineProperty(this, "width", {
        value: 1,//_iconWidth,
        configurable: true,
        enumerable: true
    });
    Object.defineProperty(this, "height", {
        value: 1,//_iconHeight,
        configurable: true,
        enumerable: true
    });

    function _drawImage(iconName) {
        var srcY = _iconHeight * _iconIndex[iconName];
        var posX = -this.width/2;
        var posY = -this.height/2;
        app.ctx.scale(1, -1);
        app.ctx.drawImage(_image, 0, srcY, _iconWidth, _iconHeight, posX, posY, this.width, this.height);
    }
    this.draw = _drawImage.bind(this);
}

function afterAssetsLoad() {
    app.cdtiIcon = new CdtiIcons(app.cache["/static/cdti_icons.png"], 10);
    setupInput();
    main();
}