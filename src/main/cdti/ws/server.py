import os
import time
from threading import Thread
import webbrowser
import cherrypy
from ws4py.server.cherrypyserver import WebSocketPlugin, WebSocketTool
from ws4py.websocket import WebSocket
from ws4py.manager import WebSocketManager
from ws4py.messaging import TextMessage

cherrypy.config.update({'server.socket_port': 9152})
WebSocketPlugin(cherrypy.engine).subscribe()
cherrypy.tools.websocket = WebSocketTool()
connections = WebSocketManager()


class Server(object):
    @cherrypy.expose
    def index(self):
        return open('./static/index.html')

    @cherrypy.expose
    def ws(self):
        pass


def start_browser():
    time.sleep(0.1)
    url = "http://localhost:9152/static"
    webbrowser.open(url, new=2)

browser = Thread(target=start_browser)


