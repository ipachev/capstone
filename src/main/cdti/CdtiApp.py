import json
import math
import os
import threading
import signal
import random
import time

import cherrypy
from main.common.MathHelpers import MathHelpers
from ws4py.messaging import TextMessage

import protobuf.cdti_pb2 as _cdti
from main.cdti.SaaDataLink import SaaDataLink
from main.cdti.ws.Socket import WebSocketHandler
from main.cdti.ws.server import Server, browser
from main.common.GeographicCoordinate import GeographicCoordinate
from main.common.Plane import Plane
from main.common.VectorMath import nautical_conversion_ft

try:
    data_link = SaaDataLink()
except ConnectionRefusedError:
    data_link = False
    print('#### WARNING - Fake CDTI is Running ####')



def send_message(message):
    cherrypy.engine.publish('websocket-broadcast', TextMessage(message))


def get_heading(plane):
    north, east = plane.velocity.north, plane.velocity.east
    result = math.degrees(math.atan2(north, east))
    if math.isnan(result):
        result = 0.0
    return result


def get_speed(plane):
    north, east, down = plane.velocity.north, plane.velocity.east, plane.velocity.down
    origin = GeographicCoordinate.factory()
    return origin.distance(GeographicCoordinate.factory(north=north, east=east, altitude=-down))


def get_relative_position(ownship, plane=None):
    '''return a tuple containing distance north from ownship,
                                 distance east from ownship
                                 distance down from ownship'''
    if plane is None:
        x, y, z = 0, 0, 0
        # print("ownship %f %f %f" % (x, y, z))
    else:
        plane_id = plane.adsb_id or plane.tcas_id or plane.radar_id
        #print(plane)
        own_plane = Plane(ownship.adsb_id, ownship.position.lat, ownship.position.long, ownship.position.alt, ownship.velocity.north, ownship.velocity.east, ownship.velocity.down)
        intruder = Plane(plane_id, plane.position.lat, plane.position.long, plane.position.alt, plane.velocity.north, plane.velocity.east, plane.velocity.down)
        #print(intruder)
        x, y, z = MathHelpers.find_relative_pos(own_plane, intruder)


        x = nautical_conversion_ft * x
        y = nautical_conversion_ft * y
        z = nautical_conversion_ft * z

        # print("intruder %s :  %f %f %f" % (plane.adsb_id, x, y, z))

    return x, y, z


def get_icon(plane):
    if plane.adsb_id == "0":
        return 'ownship normal'
    if get_speed(plane) > 0:
        directional = 'directional'
    else:
        directional = 'non-directional'
    if plane.severity == _cdti.CDTIPlane.RESOLUTION:
        return 'resolution advisory {0}'.format(directional)
    if plane.severity == _cdti.CDTIPlane.TRAFFIC:
        return 'traffic advisory {0}'.format(directional)
    if plane.severity == _cdti.CDTIPlane.PROXIMATE:
        return 'proximate traffic {0}'.format(directional)
    return 'air traffic {0}'.format(directional)


def recv_data():

    while True:
        if data_link == False:
            print('...')
            create_fake_data()
        else:
            report = data_link.recv()
            # if report is None:
            #     cherrypy.engine.exit()
            #     return
            ownship_position = get_relative_position(report.ownship)
            aircraft_list = [{'icon': get_icon(report.ownship), 'position': ownship_position,
                              'velocity': get_heading(report.ownship)}]
            for plane in report.planes:
                aircraft_list.append({'icon': get_icon(plane), 'position': get_relative_position(report.ownship, plane),
                                      'velocity': get_heading(plane),
                                      'relative_alt': plane.position.alt - report.ownship.position.alt,
                                      'id': plane.adsb_id or plane.tcas_id or plane.radar_id})
                print(get_heading(plane))
            update_message = json.dumps(aircraft_list)
            send_message(update_message)

#Creates a single fake plane to observe for testing purposes only
def create_fake_data():
    aircraft_list = [{'icon': 'ownship normal', 'position': (0, 0, 0),'velocity': 1}]
    aircraft_list.append({'icon': 'resolution advisory directional', 'position': (random.random()*5, 0, 0),
                                  'velocity': 1,
                                  'relative_alt': 80,
                                  'id': 'fake plane'})
    update_message = json.dumps(aircraft_list)
    send_message(update_message)
    time.sleep(1)


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.staticdir.root': os.path.join(os.path.abspath(os.path.dirname(__file__)), 'ws'),
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static',
            'tools.staticdir.index': './index.html'
        },
        '/ws': {
            'tools.websocket.on': True,
            'tools.websocket.handler_cls': WebSocketHandler
        }
    }
    recv = threading.Thread(target=recv_data, daemon=True)
    recv.start()
    browser.start()
    cherrypy.quickstart(Server(), '/', config=conf)


# Catch the data_link error so we know when we are not using the simulator
# Need to replace the report with a fake list of reports or just a flag that
#   will inform the CDTI that no connection is being used and therefor we
#   will want the CDTI to use a fake list of airplanes instead

