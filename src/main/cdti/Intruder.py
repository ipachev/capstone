from OpenGL.GLUT import *
from OpenGL.GL import *
from OpenGL.GLU import *

class Intruder:

    x = 0
    y = 0
    name = 'no name'

    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def change_pos(self, newX, newY):
        x = newX
        y = newY

    def renderIntruder(self):
        glPushMatrix()
        glTranslatef(self.x, self.y, 0.0)
        glRotatef(45, 1, 0, 0)
        color = [1.0,0.,0.,1.]
        glMaterialfv(GL_FRONT,GL_DIFFUSE,color)
        glutSolidCone(2.0, 1.0, 100, 100)
        glPopMatrix()