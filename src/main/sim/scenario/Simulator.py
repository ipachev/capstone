from main.sim.scenario.Plane import Node, SimPlane, Position, Sensors


class Simulator:
    def __init__(self, json):
        """
        This is a system for determining the location and velocity of simPlanes and sending that data to the SAA for
        analysis.
        :param json: A .json file containing the location checkpoints for all of the planes in the simulation.
        """
        self.ownship = None
        self.intruders = {}
        for d in json['initial-conditions']:
            tail = d['tail']
            sens = None
            if not tail == 'OWNSHIP':
                sens = Sensors(d['sensors']['tcas'], d['sensors']['adsb'], d['sensors']['radar'])
            pos = Position(d['latitude'], d['longitude'], d['altitude'])
            start_node = Node(0.0, d['velocity']['north'], d['velocity']['east'], d['velocity']['down'], pos)
            plane = SimPlane(tail, sens, start_node)

            if tail == 'OWNSHIP':
                if self.ownship is not None:
                    raise Exception("too many ownships")
                self.ownship = plane
            else:
                self.intruders[tail] = plane

        checkpoints = json['checkpoints']
        self.ownship.build_nodes(checkpoints)
        for i in self.intruders.values():
            i.build_nodes(checkpoints)

    def get_ownship_at_time(self, time):
        return self.ownship.get_plane_at_time(time)

    def get_intruder_at_time(self, tail, time):
        return self.intruders[tail].get_plane_at_time(time)

    def get_all_intruder_tail_numbers(self):
        tmp = list(self.intruders.keys())
        tmp.sort()
        return tmp

    def get_intruders_at_time(self, time):
        tmp = self.get_all_intruder_tail_numbers()
        return map(lambda tail: self.get_intruder_at_time(tail, time), tmp)
