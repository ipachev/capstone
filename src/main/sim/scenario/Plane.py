from math import sqrt
import numpy as np
from main.common.Plane import Plane
from main.common.EarthReferenceFrame import *


class Position:
    def __init__(self, latitude, longitude, altitude):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude


class Node:
    """
    Nodes are used for updating planes in the simulation.
    They take in a time, position and velocity
    The simulator will then force a plane to be at the specified position at the specified time with the specified
    velocity
    """
    def __init__(self, time, north_velocity, east_velocity, down_velocity, position=None):
        self.time = time
        self.position = None
        self.x = None
        self.y = None
        self.z = None
        self.point = None

        self.dx = None
        self.dy = None
        self.dz = None
        self.velocity = None

        self.north = north_velocity
        self.east = east_velocity
        self.down = down_velocity

        if position is not None:
            self.set_position(position)

    def set_position(self, position):
        lat, long, alt = position.latitude, position.longitude, position.altitude
        self.position = position
        self.x, self.y, self.z = geo_to_xyz(lat, long, alt)
        self.point = np.array([self.x, self.y, self.z], dtype=np.float64)
        self.velocity = self.find_velocity(lat, long)
        self.dx, self.dy, self.dz = self.velocity[0], self.velocity[1], self.velocity[2]

    def find_velocity(self, lat, long):
        north_unit = np.array(north_vector(lat, long), dtype=np.float64)
        east_unit = np.array(east_vector(lat, long), dtype=np.float64)
        down_unit = np.array(down_vector(lat, long), dtype=np.float64)
        return self.north * north_unit + self.east * east_unit + self.down * down_unit


class Sensors:
    def __init__(self, tcas, adsb, radar):
        self.tcas = tcas
        self.adsb = adsb
        self.radar = radar


class SimPlane:
    @staticmethod
    def find_position(node, time):
        """
        Finds the location of a plane at the given time
        :param node: The starting location of the plane
        :param time: The time that you want to find the plane's projected location
        :return: The projected location of the plane at the given time
        """
        pos = node.point
        vel = node.velocity
        dt = time - node.time

        delta_pos = dt * vel
        result = pos + delta_pos
        x, y, z = result[0], result[1], result[2]
        lat, long, alt = xyz_to_geo(x, y, z)
        return Position(lat, long, alt)

    def __init__(self, tail_number, sensors, start_node):
        """
        TODO What is a plane?
        :param tail_number: The unique identifier for this plane.
        :param sensors: The active sensors for this plane ???
        :param start_node: The starting location for the plane
        :return:
        """
        self.nodes = [start_node]
        self.times = None
        self.tail = tail_number
        self.sensors = sensors
        if self.sensors is not None:
            self.radar = sensors.radar
            self.tcas = sensors.tcas
            self.adsb = sensors.adsb

    def build_nodes(self, checkpoints):
        """
        Finds all of the checkpoints for a plane in the set :checkpoints: then creates a list of nodes to specify when
        the velocity of the plane changes.
        :param checkpoints:The set of all checkpoints that update velocities of planes.
        """

        for c in checkpoints:
            if c['tail'] == self.tail:
                time = c['time']
                vel = c['velocity']
                self.nodes.append(Node(time, vel['north'], vel['east'], vel['down'], None))
        self.generate_interpolated_node()
        self.nodes.sort(key=lambda n: n.time)
        self.calc_positions()
        self.times = np.empty(len(self.nodes), dtype=np.float64)
        for node, i in zip(self.nodes, range(0, len(self.nodes))):
            self.times[i] = node.time

    def generate_interpolated_node(self):
        for i in range(1, len(self.nodes)):
            node = self.nodes[i]
            last = self.nodes[i-1]
            for t in np.arange(last.time, node.time, 0.1):
                generated = Node(t, last.north, last.east, last.down, None)
                self.nodes.append(generated)

    def calc_positions(self):
        """
        //does this have mean that the nodes need to be input in chronological order?
        Calculates the starting positions of all of the nodes of a plane.
        """
        for i in range(1, len(self.nodes)):
            node = self.nodes[i]
            last = self.nodes[i-1]
            position = SimPlane.find_position(last, node.time)
            node.set_position(position)

    def get_at_time(self, time):
        """
        Finds the position and velocity of a plane at a given time
        :param time: The time into the simulation
        :return: The position and velocity of the plane at a given time
        """
        n = len(self.nodes)
        if n < 2:
            last_node = self.nodes[0]
        else:
            group_size = int(round(sqrt(n)))
            i = 0
            last_i = 0
            while i < n and self.times[i] < time:
                last_i = i
                i += group_size
            if last_i - i == 0:
                last_node = self.nodes[i]
            elif i >= n:
                last_node = self.nodes[n - 1]
            else:
                last_k = last_i
                for k in range(last_i, n, 1):
                    if self.times[i] > time:
                        break
                    last_k = k
                last_node = self.nodes[last_k]

        pos = SimPlane.find_position(last_node, time)
        vel = last_node.north, last_node.east, last_node.down
        return pos, vel

    def get_plane_at_time(self, time):
        """
        Gets the plane's details (tail number, latitude, longitude, altitude, north, east, down, active sensors) at a
        given time
        :param time: The time into the simulation
        :return: A :Plane: with the the simPlane's details at the given time
        """
        pos, vel = self.get_at_time(time)
        lat, long, alt = pos.latitude, pos.longitude, pos.altitude
        north, east, down = vel[0], vel[1], vel[2]
        plane = None
        if self.tail != 'OWNSHIP':
            adsb, tcas, radar = self.adsb, self.tcas, self.radar
            plane = Plane(self.tail, lat, long, alt, north, east, down, adsb=adsb, tcas=tcas, radar=radar)
        else:
            plane = Plane(self.tail, lat, long, alt, north, east, down)

        return plane
