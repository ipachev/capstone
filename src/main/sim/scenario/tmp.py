import numpy as np
from main.common.EarthReferenceFrame import *
lat = 89.999
long = 180.0000
alt = 10000.0
north_velocity = -300
east_velocity = 0
down_velocity = 0

n = north_vector(lat, long)
e = east_vector(lat, long)
d = down_vector(lat, long)
n = np.array(n, dtype=np.float64)
e = np.array(e, dtype=np.float64)
d = np.array(d, dtype=np.float64)
velocity = north_velocity*n + east_velocity*e + down_velocity*d
print(velocity)
