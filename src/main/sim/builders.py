import time

from main.sim.sensor.Tcas import Tcas
from main.sim.sensor.Adsb import Adsb
from main.sim.sensor.Radar import Radar
from main.sim.sensor.Ownship import Ownship


class Config:
    def __init__(self, factory, host, port, is_logging_reports, log_file, is_scrambled, reports_per_packet,
                 packets_per_second):
        self.factory = factory
        self.host = host
        self.port = port
        self.is_logging_reports = is_logging_reports
        self.log_file = log_file
        self.is_scrambled = is_scrambled
        self.reports_per_packet = reports_per_packet
        self.packets_per_second = packets_per_second
        self.time = time.time

    def __str__(self):
        name = str(self.name).capitalize()
        fact = str(self.factory)
        host = str(self.host)
        port = str(self.port)
        return "%sConfig(factory=%s, host='%s', port=%s)" % (name, fact, host, port)


class TcasConfig(Config):
    def __init__(self, factory=Tcas, host='', port=5001, is_logging_reports=False, log_file='tcas.log',
                 is_scrambled=True, reports_per_packet=25, packets_per_second=1):
        super().__init__(factory, host, port, is_logging_reports, log_file, is_scrambled, reports_per_packet,
                         packets_per_second)

    @property
    def name(self):
        return 'tcas'

    @property
    def colloquial_name(self):
        return 'TCAS'


class AdsbConfig(Config):
    def __init__(self, factory=Adsb, host='', port=5002, is_logging_reports=False, log_file='adsb.log',
                 is_scrambled=True, reports_per_packet=50, packets_per_second=1):
        super().__init__(factory, host, port, is_logging_reports, log_file, is_scrambled, reports_per_packet,
                         packets_per_second)

    @property
    def name(self):
        return 'adsb'

    @property
    def colloquial_name(self):
        return 'ADS-B'


class RadarConfig(Config):
    def __init__(self, factory=Radar, host='', port=5003, is_logging_reports=False, log_file='radar.log',
                 is_scrambled=True, reports_per_packet=20, packets_per_second=1):
        super().__init__(factory, host, port, is_logging_reports, log_file, is_scrambled, reports_per_packet,
                         packets_per_second)

    @property
    def name(self):
        return 'radar'

    @property
    def colloquial_name(self):
        return 'Radar'


class OwnshipConfig(Config):
    def __init__(self, factory=Ownship, host='', port=5004, is_logging_reports=False, log_file='ownship.log',
                 is_scrambled=True, reports_per_packet=1, packets_per_second=10):
        super().__init__(factory, host, port, is_logging_reports, log_file, is_scrambled, reports_per_packet,
                         packets_per_second)

    @property
    def name(self):
        return 'ownship'

    @property
    def colloquial_name(self):
        return 'Ownship'


class SimulationBuilder:
    def __init__(self, simulation=None, tcas=None, adsb=None, radar=None, ownship=None, validate=True,
                 validator_out_file=None):
        self.simulation = simulation
        self.tcas = tcas
        self.adsb = adsb
        self.radar = radar
        self.ownship = ownship
        self.validate = validate
        self.validator_out_file = validator_out_file

    def is_ready(self):
        return self.simulation and self.tcas and self.adsb and self.radar and self.ownship

    def __iter__(self):
        yield self.simulation
        yield self.tcas
        yield self.adsb
        yield self.radar
        yield self.ownship

    def __str__(self):
        name = "SimulationBuilder"
        sim = str(self.simulation)
        tcas = str(self.tcas)
        adsb = str(self.adsb)
        radar = str(self.radar)
        ownship = str(self.ownship)
        return "%s(simulation=%s, tcas=%s, adsb='%s', radar=%s, ownship=%s)" % (name, sim, tcas, adsb, radar, ownship)

    def __getitem__(self, key):
        if key == "tcas":
            return self.tcas
        if key == "adsb":
            return self.adsb
        if key == "radar":
            return self.radar
        if key == "ownship":
            return self.ownship

    def __setitem__(self, key, val):
        if key == "tcas":
            self.tcas = val
        if key == "adsb":
            self.adsb = val
        if key == "radar":
            self.radar = val
        if key == "ownship":
            self.ownship = val
