class ReportBuffer:
    def __init__(self):
        self.unsent_reports = {}
        self.report_sequence = []

    def add(self, tail, report):
        self.unsent_reports[tail] = report
        try:
            self.report_sequence.remove(tail)
        except ValueError:
            pass
        self.report_sequence.append(tail)

    def read(self, max_reports):
        reports = self._most_recent(max_reports)
        return ReportBuffer._combine_reports(reports), reports

    def _most_recent(self, max_reports):
        result = []
        i = 0
        while i < max_reports and len(self.report_sequence):
            report_name = self.report_sequence.pop(0)
            report = self.unsent_reports[report_name]
            result.append(report)
            i += 1
        return result

    @staticmethod
    def _combine_reports(reports):
        size = sum(map(lambda r: 4 + r.ByteSize(), reports))
        buf = bytearray(size)
        pos = 0
        for report in reports:
            size = report.ByteSize()
            buf[pos:pos + 4] = size.to_bytes(4, byteorder='big', signed=False)
            pos += 4
            buf[pos:pos + size] = report.SerializeToString()
            pos += size
        return buf

    def __len__(self):
        return len(self.report_sequence)

    def __bool__(self):
        return len(self.report_sequence) > 0

