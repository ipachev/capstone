import random
from math import atan2, atan, degrees, sqrt

from main.common.MathHelpers import MathHelpers

import protobuf.radar_pb2
from main.sim.sensor.AbstractSensor import AbstractSensor
from main.sim.sensor.IdMap import IdMap
import numpy as np
import nvector as nv

class Radar(IdMap, MathHelpers, AbstractSensor):
    max_range = 2893095  # about 550 miles

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def create_report(self, report_time, plane, ownship):
        """
        Creates the radar report for an individual plane:
            id, timestamp, latitude, longitude, altitude,
            north, east, down, range, azimuth, elevation
        :param plane: the plane to have a report created for
        :return:
        """
        report = protobuf.radar_pb2.RadarReport()
        report.id = self.get_id(plane)
        report.absolute_id = plane.tail_number
        report.timestamp = report_time
        report.latitude = ownship.latitude
        report.longitude = ownship.longitude
        report.altitude = int(round(ownship.altitude))
        report.north = plane.north
        report.east = plane.east
        report.down = plane.down
        # calculates range (in feet) with equation specified in SensorEquations
        # report.range = self.calculate_range(ownship, plane)
        # calculates azimuth with equation specified in SensorEquations
        # report.azimuth = self.calculate_azimuth(ownship, plane)

        # calculates elevation with equation specified in SensorEquations
        report.elevation = self.calculate_elevation(plane, ownship)
        report.roll = 0.0
        report.pitch = 0.0
        report.yaw = degrees(atan2(ownship.east, ownship.north))
        own_lat, own_long, own_alt = ownship.latitude, ownship.longitude, ownship.altitude
        own_yaw, own_pitch, own_roll = report.yaw, report.pitch, report.roll
        frame_tx = self.wgs84_to_geo_frame_func(own_lat, own_long, own_alt, own_yaw, own_pitch, own_roll)
        vec = self.lla_2_xyz(plane.latitude, plane.longitude, plane.altitude)
        vec = frame_tx(vec)
        report.azimuth = degrees(atan2(vec[1], vec[0]))
        d = sqrt(vec[1]*vec[1] + vec[0]*vec[0])
        report.elevation = degrees(atan(-vec[2]/d))
        report.range = sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2])

        return report

    @staticmethod
    def scramble_report(intruder, report, ownship):
        report.latitude += random.uniform(-.0001, .0001)
        report.longitude += random.uniform(-.0001, .0001)
        report.altitude += random.randint(-50, 50)
        report.north += random.uniform(-20, 20)
        report.east += random.uniform(-20, 20)
        report.down += random.uniform(-20, 20)
        report.range += random.uniform(-50, 50)
        report.azimuth += random.uniform(2.0, 2.0)
        report.elevation += random.uniform(-2.5, 2.5)
        return report

    def filter_intruder(self, intruder, ownship):
        is_in_range = self.calculate_range(intruder, ownship) < Radar.max_range
        # 90% chance of reporting
        return (not intruder.radar) or random.uniform(0.0, 1.0) > 0.9 or (not is_in_range)

    @staticmethod
    def calculate_range(intruder, ownship):
        return MathHelpers.calculate_range(intruder, ownship)

    @staticmethod
    def calculate_azimuth(intruder, ownship):
        x, y, z = MathHelpers.find_relative_pos(ownship, intruder)
        return degrees(atan2(y, x))

    @staticmethod
    def calculate_elevation(intruder, ownship):
        x, y, z = MathHelpers.find_relative_pos(ownship, intruder)
        d = sqrt(x*x + y*y)
        return degrees(atan2(-z, d))
