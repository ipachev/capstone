import random

from main.common.MathHelpers import MathHelpers

import protobuf.ownship_pb2
from main.sim.sensor.AbstractSensor import AbstractSensor


class Ownship(AbstractSensor, MathHelpers):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def create_report(self, report_time, ownship, ownship_again):
        report = protobuf.ownship_pb2.OwnshipReport()
        report.timestamp = report_time
        report.absolute_id = ownship.tail_number
        report.ownship_latitude = ownship.latitude
        report.ownship_longitude = ownship.longitude
        report.ownship_altitude = self.calculate_altitude(ownship)
        report.north = ownship.north
        report.east = ownship.east
        report.down = ownship.down

        return report

    @staticmethod
    def scramble_report(intruder, report, ownship):
        report.ownship_latitude += random.uniform(-.0001, .0001)
        report.ownship_longitude += random.uniform(-.0001, .0001)
        report.ownship_altitude += random.randint(-50, 50)
        report.north += random.uniform(-5, 5)
        report.east += random.uniform(-5, 5)
        report.down += random.uniform(-5, 5)
        return report

    @staticmethod
    def filter_intruder(intruder, ownship):
        return True

    @staticmethod
    def calculate_altitude(ownship):
        return int(round(ownship.altitude))

    def get_intruder(self, at_time, ownship):
        return ownship
