class IdMap:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tail_to_id = {}

    def get_id(self, intruder):
        # TODO : add ID removal for old planes
        if not self.tail_to_id:
            self.tail_to_id[intruder.tail_number] = 1
        elif intruder.tail_number not in self.tail_to_id:
            self.tail_to_id[intruder.tail_number] = max(self.tail_to_id.values()) + 1
        return self.tail_to_id[intruder.tail_number]