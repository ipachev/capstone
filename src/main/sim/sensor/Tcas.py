import random
from math import atan2, degrees

from main.common.MathHelpers import MathHelpers

import protobuf.tcas_pb2
from main.sim.sensor.AbstractSensor import AbstractSensor
from main.sim.sensor.IdMap import IdMap


class Tcas(IdMap, MathHelpers, AbstractSensor):
    range_threshold = 607612
    vertical_distance_threshold = 10000
    feet_per_nautical_mile = 6076.12

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def create_report(self, report_time, intruder, ownship):
        report = protobuf.tcas_pb2.TcasReport()
        report.id = self.get_id(intruder)
        report.absolute_id = intruder.tail_number
        report.altitude = self.calculate_altitude(intruder, ownship)
        report.bearing, report.range = MathHelpers.find_bearing_and_range(intruder, ownship)
        report.ownship_latitude = ownship.latitude
        report.ownship_longitude = ownship.longitude
        report.ownship_altitude = int(round(ownship.altitude))
        report.roll = 0
        report.pitch = 0
        report.yaw = degrees(atan2(ownship.east, ownship.north))

        return report

    def scramble_report(self, intruder, report, ownship):
        # altitude +- 125 feet
        report.altitude += random.randint(-125, 125)
        # range +- 100 feet
        range_scramble_ft = 100 / 6076.12
        report.range += random.uniform(-range_scramble_ft, range_scramble_ft)
        # bearing +- 10 degrees
        report.bearing += random.randint(-10, 10)
        return report

    @staticmethod
    def filter_intruder(intruder, ownship):
        # 85% chance of reporting
        is_range_near = Tcas.calculate_range(intruder, ownship) <= Tcas.range_threshold
        is_alt_near = abs(Tcas.calculate_altitude(intruder, ownship)) <= Tcas.vertical_distance_threshold
        return (not intruder.tcas) or not is_alt_near or not is_range_near or random.uniform(0, 1.0) > 0.85

    @staticmethod
    def calculate_range(intruder, ownship):
        range_feet = MathHelpers.calculate_range(intruder, ownship)
        range_nautical_miles = range_feet / Tcas.feet_per_nautical_mile
        return range_nautical_miles

    @staticmethod
    def calculate_bearing(intruder, ownship):
        return

    @staticmethod
    def calculate_altitude(intruder, ownship):
        intruder_altitude = intruder.altitude
        ownship_altitude = ownship.altitude
        delta_altitude = intruder_altitude - ownship_altitude
        return int(round(delta_altitude))

