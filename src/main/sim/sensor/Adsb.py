import random

from main.common.MathHelpers import MathHelpers

import protobuf.adsb_pb2
from main.sim.sensor.AbstractSensor import AbstractSensor


class Adsb(AbstractSensor, MathHelpers):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def create_report(self, report_time, intruder, ownship):
        report = protobuf.adsb_pb2.AdsBReport()
        report.tail_number = intruder.tail_number
        report.absolute_id = intruder.tail_number
        report.timestamp = report_time
        report.longitude = intruder.longitude
        report.latitude = intruder.latitude
        report.altitude = int(round(intruder.altitude))
        report.north = intruder.north
        report.east = intruder.east
        report.down = intruder.down
        return report

    def scramble_report(self, intruder, report, ownship):
        report.latitude += random.uniform(-.0001, .0001)
        report.longitude += random.uniform(-.0001, .0001)
        report.altitude += random.randint(-50, 50)
        report.north += random.uniform(-15, 15)
        report.east += random.uniform(-15, 15)
        report.down += random.uniform(-10, 10)

        return report

    def filter_intruder(self, intruder, ownship):
        # 99% chance of reporting
        return (not intruder.adsb) or random.uniform(0.0, 1.0) > 0.99
