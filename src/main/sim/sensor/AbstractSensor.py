import logging
import random
from socket import SHUT_RDWR
from threading import Thread

from main.saa.PeriodicEvent import PeriodicEvent
from main.sim.sensor.ReportBuffer import ReportBuffer


class AbstractSensor:
    def __init__(self, options, simulation, socket):
        super().__init__()

        self._is_running = False
        self.start_time = None

        self.time = options.time
        self.reports_per_packet = options.reports_per_packet
        self.packets_per_second = options.packets_per_second
        self.packet_wait_period = 1.0/self.packets_per_second
        self.report_event = PeriodicEvent(self.packet_wait_period / self.reports_per_packet)

        self.socket = socket
        self.next_send_time = None
        self.unsent_reports = ReportBuffer()

        self.simulation = simulation
        if self.simulation is not None:
            self.intruders = self.simulation.get_all_intruder_tail_numbers()
            self.total_intruders = len(self.intruders)
            self.intruder_index = 0
        self.is_scrambled = options.is_scrambled

        self.log = logging.getLogger(__name__).getChild(type(self).__name__)

        self.report_logger = logging.getLogger(__name__).getChild(type(self).__name__).getChild('data')
        self.is_logging_reports = options.is_logging_reports
        self.log_file = options.log_file
        self._setup_log_file()

        self.worker = Thread(target=self.update_loop)

    def _setup_log_file(self):
        self.report_logger.propagate = False
        file_handler = logging.FileHandler(self.log_file)
        file_handler.setFormatter(logging.Formatter("%(message)s"))
        self.report_logger.addHandler(file_handler)
        if self.is_logging_reports:
            self.report_logger.setLevel(logging.INFO)

    def start(self, start_time):
        self.log.info('starting')
        self._is_running = True
        self.start_time = start_time
        self.next_send_time = random.uniform(0, self.packet_wait_period)
        self.report_event.start()
        self.worker.start()

    def stop(self):
        self._is_running = False

    def _stop(self):
        self.log.info('stopping')
        self._is_running = False

        try:
            self.report_event.stop()
            self.socket.shutdown(SHUT_RDWR)
            self.socket.close()
        except OSError as err:
            raise err
        self.log.info('done')

    def get_ownship(self, at_time):
        return self.simulation.get_ownship_at_time(at_time)

    def get_intruder(self, at_time, ownship):
        for _ in range(self.total_intruders):
            tmp = self.simulation.get_intruder_at_time(self.intruders[self.intruder_index], at_time)
            self.intruder_index += 1
            self.intruder_index %= self.total_intruders
            if not self.filter_intruder(tmp, ownship):
                return tmp
        return None

    def update_loop(self):
        while self.is_running():
            report_time = self.time() - self.start_time
            self.log.debug("report time: %s" % str(report_time))
            ownship = self.get_ownship(report_time)
            intruder = self.get_intruder(report_time, ownship)
            if intruder is not None:
                self.log.debug('creating report')
                report = self.create_report(report_time + self.start_time, intruder, ownship)
                if self.is_scrambled:
                    self.log.debug('scrambling the report')
                    self.scramble_report(intruder, report, ownship)
                self.unsent_reports.add(intruder.tail_number, report)

            if bool(self.unsent_reports) and self.is_send_past_due(report_time):
                self.send_packet(report_time)
            self.report_event.wait()

    def send_packet(self, report_time):
        self.log.debug('sending reports')
        assert self.unsent_reports
        buf, reports = self.unsent_reports.read(self.reports_per_packet)
        self.log_reports(reports)
        self.next_send_time = report_time + self.packet_wait_period
        try:
            self.socket.sendall(buf)
            self.log.debug('sent report')
        except OSError as err:
            self.log.warning(err)
            self.stop()

    def is_running(self):
        return self._is_running

    def is_send_past_due(self, report_time):
        return report_time - self.next_send_time > 0

    def log_reports(self, reports):
        for r in reports:
            self.report_logger.info(str(list(map(lambda f: str(f[1]), r.ListFields()))))

    def create_report(self, report_time, intruder, ownship):
        raise NotImplemented

    def filter_intruder(self, intruder, ownship):
        raise NotImplemented

    def scramble_report(self, intruder, report, ownship):
        raise NotImplemented


