from socket import socket, SHUT_RD, SHUT_WR
import threading
from json import JSONEncoder
import protobuf.cdti_pb2 as _cdti
from main.common.EarthReferenceFrame import xyz_to_geo
from geopy.distance import vincenty
from math import sqrt
from measurement.measures import Distance


class Validator:
    def __init__(self, simulator, start_time, radar_map, tcas_map, output_file=None, sock=socket()):
        self.simulator = simulator
        self.radar_map = radar_map
        self.tcas_map = tcas_map
        self.socket = sock
        self.socket.setblocking(True)
        self.start_time = None
        self.output_file = output_file
        self.worker = threading.Thread(target=self.work)
        self.exiting = False
        self.start_time = start_time

    def start(self):
        self.worker.start()

    def exit(self):
        self.exiting = True

    def shutdown(self):
        try:
            self.socket.shutdown(SHUT_RD)
        except OSError:
            pass

        try:
            self.socket.shutdown(SHUT_WR)
        except OSError:
            pass

        try:
            self.socket.close()
        except OSError:
            pass

    def work(self):
        print("validator: trying to connect to cdti output of SP...")
        self.socket.connect(('localhost', 5000))
        print("validator connected!")
        while not self.exiting:
            report = self.recv_report()
            if not report:
                break
            timestamp = report.timestamp
            relative_time = timestamp - self.start_time
            ownship = report.ownship
            planes = report.planes
            self.match_planes(planes, relative_time)
        self.shutdown()

    def match_planes(self, planes, time):
        seen_plane_tails = []
        event = {
            'time': time,
            'matches': [],
            'duplicates': []
        }

        sim_planes = self.simulator.get_intruders_at_time(time)
        for plane in planes:
            if plane.id.startswith("RDR"):
                rdr_id = int(plane.id[2:])
                for tail, id in self.radar_map.items():
                    if rdr_id == id:
                        matched_tail = tail
            elif plane.id.startswith("TCA"):
                tcas_id = int(plane.id[2:])
                for tail, id in self.tcas_map.items():
                    if tcas_id == id:
                        matched_tail = tail
            else:
                matched_tail = plane.id

            matched_sim_plane = [p for p in sim_planes if p.tail_number == matched_tail][0]
            if matched_tail in seen_plane_tails:
                duplicate = self.create_duplicate_struct(plane)
                event["duplicates"].append(duplicate)
            else:
                seen_plane_tails.append(matched_tail)
                match = self.create_match_struct(matched_sim_plane, plane)
                event["matches"].append(match)

        missing_planes = [plane for plane in sim_planes if plane.tail_number not in seen_plane_tails]
        event["missing"] = self.create_missing_list(missing_planes)

        if self.output_file is not None:
            self.output_file.write("%s,\n" % JSONEncoder().encode(event))
        else:
            print("%s\n\n" % JSONEncoder().encode(event))

    def recv_report(self):
        try:
            buf = self.socket.recv(4)
            count = int.from_bytes(buf[:4], byteorder='big', signed=False)
            buf = self.socket.recv(count)
            report = _cdti.CDTIReport()
            report.ParseFromString(bytes(buf))
            return report
        except Exception as err:
            print(err)
            self.exit()

    def create_duplicate_struct(self, plane):
        duplicate = {}
        duplicate["id"] = plane.id
        # this location probably needs revision
        lat, long, alt = xyz_to_geo(plane.position.N, plane.position.E, plane.position.D)
        duplicate["latitude"] = lat
        duplicate["longitude"] = long
        duplicate["alt"] = alt
        duplicate["velocity"] = {}
        duplicate["velocity"]["north"] = plane.velocity.N
        duplicate["velocity"]["east"] = plane.velocity.E
        duplicate["velocity"]["down"] = plane.velocity.D
        return duplicate

    def create_missing_list(self, missing_planes):
        missing = []
        for plane in missing_planes:
            miss = {}
            missed_tail = plane.tail_number
            miss["tail"] = missed_tail
            if missed_tail in self.radar_map:
                miss["radar"] = self.radar_map[missed_tail]
            if missed_tail in self.tcas_map:
                miss["tcas"] = self.tcas_map[missed_tail]

            miss["latitude"] = plane.latitude
            miss["longitude"] = plane.longitude
            miss["altitude"] = plane.altitude
            miss["velocity"] = {}
            miss["velocity"]["north"] = plane.north
            miss["velocity"]["east"] = plane.east
            miss["velocity"]["down"] = plane.down
            missing.append(miss)

        return missing

    def create_match_struct(self, matched_sim_plane, plane):
        match = {}

        matched_tail = matched_sim_plane.tail_number
        match["real"] = {}
        match["real"]["tail"] = matched_tail
        if matched_tail in self.radar_map:
            match["real"]["radar"] = self.radar_map[matched_tail]
        if matched_tail in self.tcas_map:
            match["real"]["tcas"] = self.tcas_map[matched_tail]

        match["real"]["latitude"] = matched_sim_plane.latitude
        match["real"]["longitude"] = matched_sim_plane.longitude
        match["real"]["altitude"] = matched_sim_plane.altitude
        match["real"]["velocity"] = {}
        match["real"]["velocity"]["north"] = matched_sim_plane.north
        match["real"]["velocity"]["east"] = matched_sim_plane.east
        match["real"]["velocity"]["down"] = matched_sim_plane.down

        match["calculated"] = {}
        match["calculated"]["id"] = plane.id

        lat, long, alt = xyz_to_geo(plane.position.N, plane.position.E, plane.position.D)
        match["calculated"]["latitude"] = lat
        match["calculated"]["longitude"] = long
        match["calculated"]["altitude"] = alt

        match["calculated"]["velocity"] = {}
        match["calculated"]["velocity"]["north"] = plane.velocity.N
        match["calculated"]["velocity"]["east"] = plane.velocity.E
        match["calculated"]["velocity"]["down"] = plane.velocity.D
        flat_distance_ft = vincenty((lat, long),
                                    (matched_sim_plane.latitude,
                                     matched_sim_plane.longitude)).ft
        vertical_distance_ft = abs(matched_sim_plane.altitude - alt)
        total_distance_nmi = Distance(ft=sqrt(flat_distance_ft * flat_distance_ft +
                                              vertical_distance_ft * vertical_distance_ft)).nm

        match["distance"] = total_distance_nmi
        return match
