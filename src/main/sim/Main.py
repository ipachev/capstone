import sys
import logging
import json
import atexit
import signal
from main.sim.Server import Server
from main.sim.scenario.Simulator import Simulator


logging.basicConfig(level=logging.INFO, format='[%(name)s][%(levelname)s] %(message)s')

WRONG_NUM_ARGS = -1

def main(args):
    if (len(args) != 2):
        print("You must specify a simulation file!")
        print("Usage: %s <simulation file>" % args[0])
        sys.exit(WRONG_NUM_ARGS)

    server = Server()
    server.config.simulation = Simulator(json.load(open(args[1])))

    # This code shows how to change the configuration of the server.
    # All sensors have these configuration options
    server.config.tcas.is_logging_reports_to_file = True
    server.config.tcas.log_file = 'tcas.log'
    server.config.tcas.host = '127.0.0.1'
    server.config.tcas.port = 5001
    server.config.tcas.is_scrambled = True
    server.config.tcas.reports_per_packet = 25
    server.config.tcas.packets_per_second = 1

    server.config.adsb.is_logging_reports_to_file = True
    server.config.adsb.is_scrambled = True
    server.config.radar.is_logging_reports_to_file = True
    server.config.radar.is_scrambled = True
    server.config.ownship.is_logging_reports_to_file = True
    server.config.ownship.is_scrambled = False#True
    atexit.register(server.signal_handler)
    server.start()


if __name__ == '__main__':
    import signal


    def shutdown_handler(signum, frame):
        exit(0)

    signal.signal(signal.SIGINT, shutdown_handler)
    main(sys.argv)

