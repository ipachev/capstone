import sys
import logging
import time

from selectors import DefaultSelector, EVENT_READ
from socket import socket, SOL_SOCKET, SO_REUSEADDR, SHUT_RDWR, AF_INET, SOCK_STREAM

from main.sim.builders import SimulationBuilder, TcasConfig, AdsbConfig, RadarConfig, OwnshipConfig
from main.sim.Validator import Validator


class Server:
    def __init__(self, builder=None, logger=None):
        # self.log = logger or logging.getLogger(__name__).getChild(type(self).__name__)
        self.log = logger or logging.getLogger("Simulation Server")

        builder = builder or Server.default_builder()
        simulation, tcas, adsb, radar, ownship = builder

        self.config = SimulationBuilder(simulation, tcas, adsb, radar, ownship)

        self.selector = None
        self.server_sockets = []

        self.factories = {}
        self.sensors = []
        self.validators = []

        self.is_running = False
        self.sim_builder = None

    @staticmethod
    def default_builder():
        tcas = TcasConfig()
        adsb = AdsbConfig()
        radar = RadarConfig()
        ownship = OwnshipConfig()
        return SimulationBuilder(None, tcas, adsb, radar, ownship)

    def start(self):
        assert not self.selector
        assert not self.server_sockets
        assert not self.sensors
        self.is_running = True
        self.log.info("starting")

        self.sim_builder = SimulationBuilder(self.config.simulation, None, None, None, None)
        self.selector = DefaultSelector()
        simulation, tcas, adsb, radar, ownship = self.config
        for config in [tcas, adsb, radar, ownship]:
            sock = socket(AF_INET, SOCK_STREAM)
            sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
            sock.bind((config.host, config.port))
            sock.listen(1)
            sock.setblocking(False)
            self.server_sockets.append(sock)
            self.selector.register(sock, EVENT_READ, config.name)
            self.factories[config.name] = config.factory
        self.run()

    def run(self):
        self.log.info("running")
        while self.is_running:
            try:
                events = self.selector.select(0.01)
                for key, mask in events:
                    self.accept(key.fileobj, key.data)
            except OSError as err:
                self.log.error("unexpected error", err)
                self.is_running = False
                raise err
        self._stop()

    def stop(self):
        self.is_running = False

    def _stop(self):
        self.log.info("stopping")
        try:
            while self.server_sockets:
                try:
                    sock = self.server_sockets.pop()
                    sock.shutdown(SHUT_RDWR)
                    sock.close()
                except OSError:
                    continue

            self.selector.close()
            self.selector = None

            while self.sensors:
                sensor = self.sensors.pop()
                sensor.shutdown()

        except OSError as err:
            self.log.error(err)
            raise err
        except:
            self.log.error("Unexpected error:", sys.exc_info()[0])
            raise
        self.log.info("done")

    def accept(self, server_sock, sensor_name):
        sock, addr = server_sock.accept()
        self.log.info('accepted %s connection from %s', self.config[sensor_name].colloquial_name, str(addr))
        assert not self.sim_builder[sensor_name]
        # print("Socket: " + str(addr) + " Sensor name: " + str(sensor_name))
        sensor = self.factories[sensor_name](self.config[sensor_name], self.config.simulation, sock)
        self.sensors.append(sensor)
        self.sim_builder[sensor_name] = sensor
        if self.sim_builder.is_ready():
            self.start_simulation()

    def start_simulation(self):
        self.log.info("starting simulation")
        start_time = time.time()
        if self.config.validate:
            radar_map = self.sim_builder.radar.tail_to_id
            tcas_map = self.sim_builder.tcas.tail_to_id
            out_file = self.config.validator_out_file
            validator = Validator(self.config.simulation, start_time, radar_map, tcas_map, output_file=out_file)
            self.validators.append(validator)

        for sensor in [s for s in self.sim_builder if s is not self.sim_builder.simulation]:
            sensor.start(start_time)
        self.sim_builder = SimulationBuilder(self.config.simulation, None, None, None, None)

    def signal_handler(self):
        self.log.info("stopping")
        self.stop()
        self.log.info("done")
