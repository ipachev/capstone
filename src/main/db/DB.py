
import random, numpy as np

from pymongo import MongoClient
from src.main.db.helpers import helpers
from sklearn.utils import shuffle
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, Imputer, MinMaxScaler, MaxAbsScaler, Normalizer

from sklearn.svm import LinearSVC, SVC, NuSVC
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import BayesianRidge
from sklearn.linear_model import PassiveAggressiveClassifier, RidgeClassifier
from sklearn.linear_model import SGDClassifier
from sklearn import cross_validation
from sklearn.ensemble import AdaBoostClassifier
import pickle


def insert_many(entries, collection):
    helpers.connect_and_write(collection=collection, documents=entries, delete_first=False)


def drop(collection):
    helpers.drop_collection(collection)


def build_training_data():
    sensor_types = ['adsb_radar', 'adsb_tcas', 'radar_tcas']
    std_fields = ['x', 'y', 'z', 'dx', 'dy', 'dz', 'timestamp', 'delta_pos', 'delta_speed', 'delta_vel',
                  'angle_between_vel']
    tcas_fields = ['x', 'y', 'z', 'delta_pos']
    fields = {'adsb_radar': std_fields, 'adsb_tcas': tcas_fields, 'radar_tcas': tcas_fields}

    results = {k: [[],[]] for k in sensor_types}

    client = MongoClient('mongodb://sean:dopepwd@ivawn.xyz:7777/capstoneDB')
    db = client.capstoneDB

    pipes = []
    pipes.extend([[{'$match': {'match': 0, 'type': t}}] for t in sensor_types])
    pipes.extend([[{'$match': {'match': 1, 'type': t}}] for t in sensor_types])

    documents = []
    for pipe in pipes:
        docs = [doc for doc in db.scrambled.aggregate(pipeline=pipe)]
        # docs = random.sample(docs, 5000)
        documents.extend(docs)

    for doc in documents:
        datum = [x for x in [doc[field] for field in fields[doc['type']]]]
        target = doc['match']
        s_t = doc['type']
        results[s_t][0].append(datum)
        results[s_t][1].append(target)

    pickle.dump(results, open('./src/main/db/training_data.p', 'wb'))


def get_training_data():
    try:
        return pickle.load(open('./src/main/db/training_data.p', 'rb'))
    except:
        raise Exception('training_data.p not found. check your work directory and/or rerun build training data.')



class Evaluate:
    def __init__(self):
        self.scalers = {'std': StandardScaler}
        self.learners = {'lin_svc': LinearSVC, 'sgd': SGDClassifier, 'ada': AdaBoostClassifier,
                         'passive_agressive': PassiveAggressiveClassifier, 'ridge': RidgeClassifier}
        self.steps = {'scale': self.get_scaler, 'clf': self.get_model}

    def get_scaler(self, type='std', params=None):
        if params:
            return self.scalers[type](*params)
        return self.scalers[type]()

    def get_model(self, name='lin_svc', params=None):
        if params:
            return self.learners[name](*params)
        return self.learners[name]()

    def make_pipeline(self, stages):
        pipe = []
        for stage, params in stages:
            pipe.append((stage, self.steps[stage](*params)))

        return Pipeline(pipe)

    def test_clf(self, models):
        r = get_training_data()

        results = []
        for data_tuple, model in zip(r.items(), models):
            k, v = data_tuple
            X, y = v

            score = cross_validation.cross_val_score(model, X, y, scoring='f1')
            results.append((k, np.mean(score)))

        print(', '.join(['{}: {:.4f}'.format(t, s) for t,s in results]), '\n')

    def build_clf(self, models, name):
        r = get_training_data()
        clfs = {}
        for data_tuple, model in zip(r.items(), models):
            k, v = data_tuple
            X, y = v
            clfs[k] = model.fit(X, y)

        pickle.dump(clfs, open('./src/main/saa/c8/clfs/{}.p'.format(name), 'wb'))




def make():
    eval = Evaluate()
    for name, model in eval.learners.items():
        models = [eval.make_pipeline([('scale', []), ('clf', [name])]) for _ in range(3)]
        print('{} results'.format(name))
        eval.test_clf(models)
        eval.build_clf(models, name)


def main():
    make()

if __name__ == '__main__':
    main()




