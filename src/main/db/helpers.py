from os import walk, stat, mkdir
from os.path import join
from pymongo import MongoClient
import pickle
import logging


data_fields = ['type', 'x', 'y', 'z', 'dx', 'dy', 'dz', 'timestamp', 'delta_pos', 'delta_speed', 'delta_vel',
               'angle_between_vel', 'match']

class helpers:
    @staticmethod
    def connect_and_write(collection, documents, delete_first=True):
        if documents:
            client = MongoClient('mongodb://sean:dopepwd@ivawn.xyz:7777/capstoneDB')
            db = client.capstoneDB

            if delete_first:
                db[collection].drop()

            db[collection].insert_many(documents)

        else:
            logging.info('No records to insert')

    @staticmethod
    def drop_collection(collection):
        client = MongoClient('mongodb://sean:dopepwd@ivawn.xyz:7777/capstoneDB')
        db = client.capstoneDB
        db[collection].drop()


