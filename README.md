# S3nse
**Version 0.1.0b1**
____
### OS Support
The program has been extensively tested on Mac OSX and Arch Linux. There are also reports that it works on Ubuntu. It will likely also run on other Linux distributions that have python3 support.

### Installation
1. Download the install script 
[here](https://bitbucket.org/ipachev/capstone/raw/6ba5f92d2b3b83b10dacc9e835780424b2a054d8/install/install.sh)
2. Download dependencies for the install script
    * clang
    * python3
    * virtualenv
    * git
3. Select a directory to install into and run the installer script:  
`./install.sh <install path>`
    * If the install path does not exist, it will be created

### Prepackaged Distribution
If you have a USB copy of the application, it is already installed. You just need to navigate to `<usb path>/senseandavoid` and follow the instructions below. You will still need to make sure you have:
* python3
* virtualenv

### Running all the applications
1. Navigate to the installed directory `cd <install path>`
2. Start the **simulation server**, **surveillance processor**, and **CDTI**: 
`./run_all.sh <simulation file>`
    * There is a nice set of simulations that can be found in `./capstone/simulations`
3. Press ctrl-C to exit the programs and display stats.

### Running some of the applications
1. Navigate to the installed directory `cd <install path>`
2. Load the python virtualenv by running `source ./bin/activate`
3. Run the programs:
    * Start the simulation server: `python3 ./run_server.py <simulation file>`
        - Simulations can be found in `./capstone/simulations`
    * Start the surveillance processor: `python3 ./run_sp.py`
    * Start the CDTI: `python3 ./run_cdti.py`
4. Press ctrl-C on each application to exit.
    * Killing the surveillance processor will display stats.

### Simluation Generator
The simulation generator can be found in `capstone/tools/simulation-generator`.  
Instructions for running the simulation generator can be found in `capstone/tools/simulation-generator/README.md`.
