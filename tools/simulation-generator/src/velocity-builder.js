import _ from "underscore"
import bb from "backbone"

const [PI, sin, cos, sqrt, atan, atan2, random, floor] = [Math.PI, Math.sin, Math.cos, Math.sqrt, Math.atan, Math.atan2, Math.random, Math.floor]
const RADIANS_PER_DEGREE = PI / 180
const radians = (d) => d * RADIANS_PER_DEGREE
const degrees = (r) => r / RADIANS_PER_DEGREE
const randomIntInclusive = (min, max) => floor(random() * (max - min + 1)) + min

var staticMethods = {
    toNED: function toNED(bearing, pitch, speed) {
        bearing = radians(bearing)
        pitch = radians(pitch)
        let n = cos(bearing)*cos(pitch)*speed
        let e = sin(bearing)*cos(pitch)*speed
        let d = -sin(pitch)*speed
        return {"north":n, "east":e, "down":d}
    },

    fromNED: function fromNED(n, e, d) {
        let speed = sqrt(n*n + e*e + d*d)
        let longitudinalSpeed = sqrt(n*n + e*e)
        let pitch = -degrees(atan(d/longitudinalSpeed))
        let bearing = degrees(atan2(e, n))
        if (bearing < 0) {
            bearing = 360 + bearing
        }
        return {'bearing': bearing, 'pitch': pitch, 'speed': speed}
    },

    parseNEDString: function(raw) {
        return _.map(raw.replace(/,/g, ' ').trim().split(/\s+/), (x) => Number(x))
    },

    findRandomSpeed: () => randomIntInclusive(120/2.5, 870/2.5) * 2.5,
    findRandomBearing: () => randomIntInclusive(0/0.25, 360/0.25) * 0.25,
    findRandomPitch: () => randomIntInclusive(-90/0.1, 90/0.1) * 0.1
}

export default bb.Model.extend({
    defaults: {
        'bearing': 0,
        'pitch': 0,
        'speed': 0
    },

    initialize: function() {
        if (this.has('north') && this.has('east') && this.has('down')) {
            var data = staticMethods.fromNED(this.get('north'), this.get('east'), this.get('down'))
            this.set(data)
        }
    },

    validate: function(attributes, options) {
        let {bearing, pitch, speed} = attributes
        let errors = {
            'bearing': _.isFinite(bearing) && bearing < 360 && 0 <= bearing,
            'pitch': _.isFinite(pitch) && pitch <= 90 && -90 <= bearing,
            'speed': _.isFinite(speed) && 0 <= speed
        }
        if (_.some(_.values(errors), (x) => x)) {
            return errors
        }
    },

    build: function() {
        var [n, e, d] = this.toNED()
        return new Velocity(n, e, d)
    },

    setRandomSpeed: function () {
        this.set('speed', staticMethods.findRandomSpeed())
    },

    setRandomBearing: function () {
        this.set('bearing', staticMethods.findRandomBearing())
    },

    setRandomPitch: function () {
        this.set('pitch', staticMethods.findRandomPitch())
    }
}, staticMethods)