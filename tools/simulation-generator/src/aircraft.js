import "babel-polyfill"
import _ from "underscore"
import bb from "backbone"

import Position from './position.js'
import Velocity from './velocity.js'
import Orientation from './orientation.js'
import ManeuverCollection from './maneuver-collection.js'
const [PI, sin, cos, sqrt, atan, atan2, random, floor] = [Math.PI, Math.sin, Math.cos, Math.sqrt, Math.atan, Math.atan2, Math.random, Math.floor]
const randomIntInclusive = (min, max) => floor(random() * (max - min + 1)) + min


function sequenceGenerator() {
    var startLetters = ["C", "G", "L", "R", "S", "X"]
    var twoLetterCombos = _.shuffle(['AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ','DA','DB','DC','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ','EA','EB','EC','ED','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ','FA','FB','FC','FD','FE','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ','GA','GB','GC','GD','GE','GF','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ','HA','HB','HC','HD','HE','HF','HG','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ','IA','IB','IC','ID','IE','IF','IG','IH','IJ','IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ','JA','JB','JC','JD','JE','JF','JG','JH','JI','JK','JL','JM','JN','JO','JP','JQ','JR','JS','JT','JU','JV','JW','JX','JY','JZ','KA','KB','KC','KD','KE','KF','KG','KH','KI','KJ','KL','KM','KN','KO','KP','KQ','KR','KS','KT','KU','KV','KW','KX','KY','KZ','LA','LB','LC','LD','LE','LF','LG','LH','LI','LJ','LK','LM','LN','LO','LP','LQ','LR','LS','LT','LU','LV','LW','LX','LY','LZ','MA','MB','MC','MD','ME','MF','MG','MH','MI','MJ','MK','ML','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NB','NC','ND','NE','NF','NG','NH','NI','NJ','NK','NL','NM','NO','NP','NQ','NR','NS','NT','NU','NV','NW','NX','NY','NZ','OA','OB','OC','OD','OE','OF','OG','OH','OI','OJ','OK','OL','OM','ON','OP','OQ','OR','OS','OT','OU','OV','OW','OX','OY','OZ','PA','PB','PC','PD','PE','PF','PG','PH','PI','PJ','PK','PL','PM','PN','PO','PQ','PR','PS','PT','PU','PV','PW','PX','PY','PZ','QA','QB','QC','QD','QE','QF','QG','QH','QI','QJ','QK','QL','QM','QN','QO','QP','QR','QS','QT','QU','QV','QW','QX','QY','QZ','RA','RB','RC','RD','RE','RF','RG','RH','RI','RJ','RK','RL','RM','RN','RO','RP','RQ','RS','RT','RU','RV','RW','RX','RY','RZ','SA','SB','SC','SD','SE','SF','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SP','SQ','SR','ST','SU','SV','SW','SX','SY','SZ','TA','TB','TC','TD','TE','TF','TG','TH','TI','TJ','TK','TL','TM','TN','TO','TP','TQ','TR','TS','TU','TV','TW','TX','TY','TZ','UA','UB','UC','UD','UE','UF','UG','UH','UI','UJ','UK','UL','UM','UN','UO','UP','UQ','UR','US','UT','UV','UW','UX','UY','UZ','VA','VB','VC','VD','VE','VF','VG','VH','VI','VJ','VK','VL','VM','VN','VO','VP','VQ','VR','VS','VT','VU','VW','VX','VY','VZ','WA','WB','WC','WD','WE','WF','WG','WH','WI','WJ','WK','WL','WM','WN','WO','WP','WQ','WR','WS','WT','WU','WV','WX','WY','WZ','XA','XB','XC','XD','XE','XF','XG','XH','XI','XJ','XK','XL','XM','XN','XO','XP','XQ','XR','XS','XT','XU','XV','XW','XY','XZ','YA','YB','YC','YD','YE','YF','YG','YH','YI','YJ','YK','YL','YM','YN','YO','YP','YQ','YR','YS','YT','YU','YV','YW','YX','YZ','ZA','ZB','ZC','ZD','ZE','ZF','ZG','ZH','ZI','ZJ','ZK','ZL','ZM','ZN','ZO','ZP','ZQ','ZR','ZS','ZT','ZU','ZV','ZW','ZX','ZY'])
    var n = {}
    var next = 0;
    return function find() {
        var s = startLetters[randomIntInclusive(0, startLetters.length-1)]
        var m = String(randomIntInclusive(10, 99))
        var t = twoLetterCombos[randomIntInclusive(0, twoLetterCombos.length-1)]
        var result = s + m + t
        return _.has(n, result) ? find() : result
    }
}

var Aircraft = bb.Model.extend({
    idAttribute: "tail",

    defaults: {
        initialPosition: new Position(0, 0, 0),
        initialVelocity: new Velocity(1, 0, 0),
        initialOrientation: new Orientation(0, 0, 0),
        adsb: true,
        radar: true,
        tcas: true
    },

    initialize: function(attributes) {
        if (!this.has('tail')) {
            this.set('tail',Aircraft.tailSequence())
        }
        this.set('created', Date())
        this.maneuvers = new ManeuverCollection()
    },

    initialCheckpoint: function() {
        var tail = this.get('tail')
        var position = this.get('initialPosition').toJSON()
        var velocity = this.get('initialVelocity').toJSON()
        var orientation = this.get('initialOrientation').toJSON()

        var result = _.extend({"tail": tail}, position, {"velocity": velocity} /*, {"orientation": orientation} */)

        if (tail != "OWNSHIP") {
            var sensorSettings = _.pick(this.attributes, 'adsb', 'radar', 'tcas')
            result = _.extend(result, {"sensors": sensorSettings})
        }
        return result
    },

    checkpoints: function() {
        return this.maneuvers.buildNodes(this)
    }

}, {
    tailSequence: sequenceGenerator(),
    findRandomAltitude: () => randomIntInclusive(0, 50000/250) * 250,
});

export default Aircraft