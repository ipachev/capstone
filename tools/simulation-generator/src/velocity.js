import _ from "underscore"
import VelocityBuilder from './velocity-builder.js'

const _north = Symbol('north');
const _east = Symbol('east');
const _down = Symbol('down');

export default class Velocity {

    constructor(north=0, east=0, down=0) {
        let err = this.validateArgs(north, east, down)
        if (err) {
            throw err
        }
        this[_north] = north
        this[_east] = east
        this[_down] = down
    }

    validateArgs(north, east, down) {
        var result = {}
        let arr = _.zip(["north", "east", "down"], [north, east, down])
        arr = _.filter(arr, (x) => !_.isFinite(x[1]))
        _.each(arr, (x) => {
            let [name, val] = x
            result[name] = val
        })
        if (!_.isEmpty(result)) {
            return result
        }
    }


    get north() {
        return this[_north]
    }

    get east() {
        return this[_east]
    }

    get down() {
        return this[_down]
    }

    get speed() {
        var n = this.north
        var e = this.east
        var d = this.down
        return Math.sqrt(n*n + e*e + d*d)
    }

    get builder() {
        var result = new VelocityBuilder({
            north: this.north,
            east: this.east,
            down: this.down
        })
        return result
    }

    toJSON() {
        var n = this.north
        var e = this.east
        var d = this.down
        return {
            "north": n,
            "east": e,
            "down": d
        }
    }
}
