import "babel-polyfill"
import _ from "underscore"
import bb from "backbone"
import Velocity from './velocity.js'
import VelocityBuilder from './velocity-builder'
import {OrientationBuilder} from './orientation.js'

function distanceGoingClockwise(startBearing, endBearing) {
    if (endBearing >= startBearing) {
        return endBearing - startBearing
    }
    else {
        endBearing += 360
        return endBearing - startBearing
    }
}

function distanceGoingCounterClockwise(startBearing, endBearing) {
    return 360 - distanceGoingClockwise(startBearing, endBearing)
}

var Maneuver = bb.Model.extend({
    defaults: {
        duration: 5,
        endVelocity: new Velocity(1, 0, 0),
        steps: 10
    },

    initialize: function(attributes, options) {
        if (!'start_time' in attributes) {
            attributes['start_time'] = this.collection.next_maneuver_time();
        }
    },

    buildNodes: function(tail, startTime, startVelocity) {
        var steps = this.get('steps')
        var endVelocity = this.get('endVelocity')

        var times = Maneuver.linSpace(startTime, startTime + this.get('duration'), steps)

        var initVelB = startVelocity.builder
        var endVelB = this.get('endVelocity').builder

        var [bStart, bEnd] = [initVelB.get('bearing'), endVelB.get('bearing')]
        var bearings
        // shorter to go clockwise
        if (distanceGoingClockwise(bStart, bEnd) < distanceGoingCounterClockwise(bStart, bEnd)) {
            if  (bEnd < bStart) {
                bEnd += 360
            }
            bearings = Maneuver.linSpace(bStart, bEnd, steps)
        }
        else {
            if  (bStart < bEnd) {
                bStart += 360
            }
            bearings = Maneuver.linSpace(bStart, bEnd, steps)
        }


        var pitches = Maneuver.linSpace(initVelB.get('pitch'), endVelB.get('pitch'), steps)
        var speeds = Maneuver.linSpace(initVelB.get('speed'), endVelB.get('speed'), steps)

        var velocities = _.map(_.zip(bearings, pitches, speeds), function(data) {
            var [b, p, s] = data
            var {north:n, east:e, down:d} = VelocityBuilder.toNED(b % 360, p, s)
            var v = new Velocity(n, e, d)
            return v
        })

        var orientations = _.map(velocities, function(v) {
            return OrientationBuilder.fromVelocity(v).build()
        })

        var jsonData = _.map(_.zip(times, velocities, orientations), function(data) {
            var [time, velocity, orientation] = data
            var jsonVelocity = {"velocity": velocity.toJSON()}
            var jsonOrientation = {"orientation": orientation.toJSON()}
            return _.extend({"tail": tail, "time": time}, jsonVelocity, jsonOrientation)
        })
        return jsonData
    }
}, {
    linSpace: function(start, end, steps) {
        if (typeof steps === "undefined") {
            steps = Math.max(Math.round(end - start) + 1, 1);
        }

        if (steps < 2) {
            return steps === 1 ? [start] : [];
        }

        var result = Array(steps);
        for (var i = --steps; i >= 0; --i) {
            result[i] = (i * end + (steps - i) * start) / steps;
        }
        return result;
    }
});

export default Maneuver