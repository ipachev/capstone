function start(intruders) {
    return `<test name="test team test"
      description="test from test team"
      time = "120"
      sensorError = "false">
`
}
function intruderXML(intruder) {
    return `<plane tag="${intruder.tail}">
      <ads-b enabled="true"/>
      <tcas enabled="true"/>
      <radar enabled="true"/>
      <movement type="linear">
         <position x="${intruder.north}" y="${intruder.east}" z="${intruder.down}"/> <!--NED in Nautical Miles-->
         <direction x="${intruder.velocity.north}" y="${intruder.velocity.east}" z="${intruder.velocity.down}"/> <!--Feet/second-->
      </movement>
   </plane>
`
}


function ownshipXML(ownship) {
    var {latitude:lat, longitude:lng, altitude:alt, velocity:{north:n, east:e, down:d}} = ownship
    return `<ownship tag="ABC123">
      <tcas enabled="true" error="1.5"/>
      <radar enabled="true"/>
      <ads-b enabled="true" error="0.2"/>
      <movement type="linear">
         <position x="0" y="0" z="0"/>
         <direction x="${ownship.velocity.north}" y="${ownship.velocity.east}" z="${ownship.velocity.down}"/>
      </movement>
   </ownship>
`
}

function deltaDegToNmi(degrees) {
    return degrees*60
}

export default function(sim) {
    var [ownship, ...intruders] = sim

    var result = start(intruders) + ownshipXML(ownship)
    for (var i = 0; i < intruders.length; ++i) {
        var plane = intruders[i]
        plane.north = deltaDegToNmi(plane.latitude - ownship.latitude)
        plane.east = deltaDegToNmi(plane.longitude - ownship.longitude)
        plane.down = -1/6076.12* (plane.altitude - ownship.altitude)
        result += intruderXML(plane)
    }
    result += '</test>\n'
    return result
}
