function intruderXML(intruder) {
    return `<intruder>
    <tail>${intruder.tail}</tail>
    <latitude>${intruder.latitude}</latitude>
    <longitude>${intruder.longitude}</longitude>
    <altitude>${intruder.altitude}</altitude>
    <north>${intruder.velocity.north}</north>
    <east>${intruder.velocity.east}</east>
    <down>${intruder.velocity.down}</down>
</intruder>
`
}


function ownshipXML(ownship) {
    var {latitude:lat, longitude:lng, altitude:alt, velocity:{north:n, east:e, down:d}} = ownship
    return `
<ownship>
    <latitude>${lat}</latitude>
    <longitude>${lng}</longitude>
    <altitude>${alt}</altitude>
    <north>${n}</north>
    <east>${e}</east>
    <down>${d}</down>
</ownship>
`
}

export default function(sim) {
    var [ownship, ...intruders] = sim
    var result = ownshipXML(ownship)
    for (var i = 0; i < intruders.length; ++i) {
        result += intruderXML(intruders[i])
    }
    return result
}
