export default function(simulation) {
    var result = {'initial-conditions': simulation, "checkpoints": []}
    return JSON.stringify(result, null, 3)
}