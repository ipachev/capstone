import "babel-polyfill"
import * as _ from "underscore"
import * as bb from "backbone"
import Maneuver from "./maneuver.js"
import {OrientationBuilder} from './orientation.js'

var ManeuverCollection = bb.Collection.extend({
    model: Maneuver,
    buildNodes: function(aircraft) {
        var tail  = aircraft.get('tail')
        var currentTime = 0
        var currentVelocity = aircraft.get('initialVelocity')

        var nodes = []
        this.each((maneuver) => {
            nodes = nodes.concat(maneuver.buildNodes(tail, currentTime, currentVelocity))
            currentVelocity = maneuver.get('endVelocity')
            currentTime += maneuver.get('duration')
        })
        // for (var i = 0; i < this.length; ++i) {
        //     var maneuver = this.at(i)
        //
        // }
        var orientation = OrientationBuilder.fromVelocity(currentVelocity).build()
        var jsonVelocity = {"velocity": currentVelocity.toJSON()}
        var jsonOrientation = {"orientation": orientation.toJSON()}
        var finalNode = _.extend({"tail": tail, "time": currentTime}, jsonVelocity, jsonOrientation)
        nodes = Array.prototype.concat(nodes, [finalNode])
        return nodes;
    }
})

export default ManeuverCollection
