import _ from 'underscore'
import bb from 'backbone'
import Aircraft from './aircraft.js'
import Position from './position.js'
import VelocityBuilder from './velocity-builder.js'
import Velocity from './velocity.js'

const natMiPerDeg = 60


function create_scenario(numberPlanes, regionSize, numberManeuvers, ownshipLocation= {lat: 35.3050053, lng: -120.66249419999997}) {
    var aircraftList = new bb.Collection([{"tail": "OWNSHIP"}], {
        model: Aircraft
    })
    var velBuilder = new VelocityBuilder()
    var alt = Aircraft.findRandomAltitude()
    var {lat:ownLat, lng:ownLng} = ownshipLocation
    var degress = regionSize/natMiPerDeg/2
    var northWestLimit = {lat: ownshipLocation.lat + degress, lng: ownshipLocation.lng - degress}
    var southEastLimit = {lat: ownshipLocation.lat - degress, lng: ownshipLocation.lng + degress}
    var lat = randomLat()
    var lng = randomLong()
    aircraftList.get('OWNSHIP').set('initialPosition', new Position(ownLat, ownLng, alt))
    for (var x = 0; x < numberPlanes; x++) {
        var tail = Aircraft.tailSequence()
        aircraftList.add({tail: tail})
        var aircraft = aircraftList.get(tail)
        alt = Aircraft.findRandomAltitude()
        lat = randomLat()
        lng = randomLong()
        velBuilder.setRandomBearing()
        velBuilder.setRandomPitch()
        velBuilder.setRandomSpeed()
        var velTmp = VelocityBuilder.toNED(velBuilder.get('bearing'), velBuilder.get('pitch'), velBuilder.get('speed'))
        var vel = new Velocity(velTmp.north, velTmp.east, velTmp.down)
        aircraft.set({
            'initialPosition': new Position(lat, lng, alt),
            'initialVelocity': vel
        })
        var lastVelocity = vel.builder
        for (var y = 0; y < numberManeuvers; ++y) {
            velBuilder.setRandomBearing()
            velBuilder.setRandomPitch()
            velBuilder.setRandomSpeed()
            velTmp = VelocityBuilder.toNED(velBuilder.get('bearing'), velBuilder.get('pitch'), velBuilder.get('speed'))
            var nextVelocity = new Velocity(velTmp.north, velTmp.east, velTmp.down)
            var nextVelocityBuilder = nextVelocity.builder
            var duration = findDuration(lastVelocity.get('speed'), nextVelocityBuilder.get('speed'), lastVelocity.get('bearing'), nextVelocityBuilder.get('bearing'))
            var steps = Math.round(duration * 2)
            aircraft.maneuvers.add({duration: duration, endVelocity: nextVelocity, steps: steps})
            lastVelocity = nextVelocityBuilder
        }
    }

    var initialCheckpoints = []
    var checkpoints = []
    aircraftList.each(function(aircraft) {
        initialCheckpoints.push(aircraft.initialCheckpoint())
        var cp = aircraft.checkpoints()
        cp.shift()
        _.each(cp, function(checkpoint) {checkpoints.push(checkpoint)})
    })
    return {'initial-conditions': initialCheckpoints, 'checkpoints': checkpoints}

    function randomLat() {
        var max = northWestLimit.lat
        var min = southEastLimit.lat
        return randRange(min, max)
    }

    function randomLong() {
        return randRange(southEastLimit.lng, northWestLimit.lng)
    }
}

function randRange(a, b) {
    var max = Math.max(a, b)
    var min = Math.min(a, b)
    var r = max - min
    return Math.random()*r + min
}

function findDuration(speedStart, speedEnd, bearingStart, bearingEnd) {
    var avgSpeed = (speedStart + speedEnd) / 2
    var [bStart, bEnd] = [bearingStart, bearingEnd]
    var angleChangeRate = findRateOfAngleChange(avgSpeed)
    // console.log('angle rate of change', angleChangeRate, 'bstart', bStart, 'bend', bEnd)
    var deltaAngle = Math.min(distanceGoingClockwise(bStart, bEnd), distanceGoingCounterClockwise(bStart, bEnd))
    var result = Math.round(Math.max(2, deltaAngle/angleChangeRate*3))
    // console.log('deltaAngle', deltaAngle, 'speed', avgSpeed, 'maneuver duration', result)
    return result
    // return Math.random()*5 + 2
}


function distanceGoingClockwise(startBearing, endBearing) {
    if (endBearing >= startBearing) {
        return endBearing - startBearing
    }
    else {
        endBearing += 360
        return endBearing - startBearing
    }
}

function distanceGoingCounterClockwise(startBearing, endBearing) {
    return 360 - distanceGoingClockwise(startBearing, endBearing)
}

function findRateOfAngleChange(speed) {
    return 90*Math.pow(Math.E, -0.0023026*speed)
}

export default create_scenario
