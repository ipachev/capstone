import _ from "underscore"
import bb from "backbone"

const _latitude = Symbol('latitude');
const _longitude = Symbol('longitude');
const _altitude = Symbol('altitude');

export var PositionBuilder = bb.Model.extend({
    build: function() {
        return new Velocity(this.get('latitude'), this.get('longitude'), this.get('altitude'))
    }
})

export default class Position {
    constructor(latitude, longitude, altitude) {
        this[_latitude] = latitude
        this[_longitude] = longitude
        this[_altitude] = altitude
    }

    get latitude() {
        return this[_latitude]
    }

    get longitude() {
        return this[_longitude]
    }

    get altitude() {
        return this[_altitude]
    }

    get builder() {
        return new PositionBuilder({
            latitude: this.latitude,
            longitude: this.longitude,
            altitude: this.altitude
        })
    }

    toJSON() {
        var lat = this.latitude
        var lng = this.longitude
        var alt = this.altitude
        return {
            "latitude": lat,
            "longitude": lng,
            "altitude": alt,
        }
    }
}