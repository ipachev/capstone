import _ from "underscore"
import bb from "backbone"

function toDegrees (angle) {
    return angle * (180 / Math.PI);
}

function validateOrientation(roll, pitch, yaw) {
    var values = [roll, pitch, yaw]
    var names = ["roll", "pitch", "yaw"]
    var invalidArgs = _.filter(_.map(_.zip(values, names), function(x) {
        var [val, name] = x
        if (!_.isFinite(val)) {
            return "the " + name + " component is not valid"
        }
        return false;
    }), Boolean)

    if (invalidArgs.length) {
        return invalidArgs.join('\n')
    }
}

export var OrientationBuilder = bb.Model.extend({
    validate: validateOrientation,
    build: function() {
        return new Orientation(this.get('roll'), this.get('pitch'), this.get('yaw'))
    }
}, {
    fromVelocity: function (velocity) {
        return findOrientationFromVelocity(velocity).builder
    }
})

export function findOrientationFromVelocity(velocity) {
    var n = velocity.north
    var e = velocity.east
    var horizontalSpeed = Math.sqrt(n*n + e*e)
    var yaw = toDegrees(Math.atan2(e, n));
    var pitch = toDegrees(-Math.atan2(velocity.down, horizontalSpeed))
    return new Orientation(0, pitch, yaw)
}

const _roll = Symbol('roll');
const _pitch = Symbol('pitch');
const _yaw = Symbol('yaw');

export default class Orientation {
    constructor(roll, pitch, yaw) {
        this[_roll] = roll
        this[_pitch] = pitch
        this[_yaw] = yaw
    }

    get roll() {
        return this[_roll]
    }

    get pitch() {
        return this[_pitch]
    }

    get yaw() {
        return this[_yaw]
    }

    get builder() {
        return new OrientationBuilder({
            roll: this.roll,
            pitch: this.pitch,
            yaw: this.yaw
        })
    }

    toJSON() {
        var r = this.roll
        var p = this.pitch
        var y = this.yaw
        return {
            "roll": r,
            "pitch": p,
            "yaw": y
        }
    }
}