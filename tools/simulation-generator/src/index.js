import fs from 'fs'
import path from 'path'
import _ from 'underscore'

import createScenario from './simulation'

Math.se
var regionSizes = [5, 10, 20, 40, 80, 160, 320, 640]

var outDir = './generated'
var simsPerRegionSize = 4
var minIntruders = 2
var maxIntruders = 20

var callback = 'json.js'
var tocsv = require('./format/csv.js')['default']
var toRocket  = require('./format/rocket.js')['default']
var toRadar = require('./format/radar.js')['default']

function makeScenariosOfSize (regionSize, callback=callback, minIntruders = 2, maxIntruders = 20, scenariosPerIntruderSize = 4, outDir) {
    for (var numIntruders = minIntruders; numIntruders <= maxIntruders; ++numIntruders) {
        for (var i = 0; i < scenariosPerIntruderSize; ++i) {
            var fileName = createFileName(regionSize, numIntruders)
            var sim = createScenario(numIntruders, regionSize, 0)['initial-conditions']
            var data = callback(sim)
            if (data !== undefined) {
                outputAScenario(fileName, data, outDir)
            }
            data = tocsv(sim)
            if (data !== undefined) {
                outputAScenario(fileName + '.csv', data, outDir)
            }
            data = toRadar(sim)
            if (data !== undefined) {
                outputAScenario(fileName + '.radar.json', data, outDir)
            }
            data = toRocket(sim)
            if (data !== undefined) {
                outputAScenario(fileName + '.rocket.xml', data, outDir)
            }
        }

    }
}

function outputAScenario(fileName, simdata, outputDir) {
    if (!fs.existsSync(outputDir)) {
        fs.mkdirSync(outputDir);
    }
    var outFile = path.resolve(outputDir, fileName)
    var fd = fs.openSync(outFile, 'w+')
    fs.writeSync(fd, simdata, 0)
}

var filesNames = {}
function createFileName(regionSize, numberIntruders, prefix="") {
    var propName = regionSize + '_' + numberIntruders
    if (!_.has(filesNames, propName)) {
        filesNames[propName] = 1;
    }
    if (numberIntruders < 10) {
        numberIntruders = "0" + numberIntruders
    }

    var result =  prefix + numberIntruders + '@' +regionSize + 'x' + regionSize + "-" + filesNames[propName]
    filesNames[propName]++
    return result
}

var [x, y, ...args] = process.argv
args.forEach(function (val, index, array) {
    if (val.match(/^outDir=/)) {
        outDir = val.replace(/^outDir=/, '')
    }
    if (val.match(/^simsPerRegionSize=/)) {
        simsPerRegionSize = Number(val.replace(/^simsPerRegionSize=/, ''))
    }
    if (val.match(/^minIntruders=/)) {
        minIntruders = Number(val.replace(/^minIntruders=/, ''))
    }
    if (val.match(/^maxIntruders=/)) {
        maxIntruders = Number(val.replace(/^maxIntruders=/, ''))
    }
    if (val.match(/^regionSizes=/)) {
        regionSizes = val.replace(/^regionSizes=/, '').split(/,/).map((x) => Number(x))
    }
    if (val.match(/^format=/)) {
        callback = val.replace(/^format=/, '')

    }

});

callback = require('./format/' + callback)['default']

_.each(regionSizes, size => makeScenariosOfSize(size, callback, minIntruders, maxIntruders, simsPerRegionSize, outDir))



