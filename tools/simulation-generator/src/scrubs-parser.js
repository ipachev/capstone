import fs from 'fs'
var toRocket = require('./format/rocket.js')['default']
var toRadar = require('./format/radar.js')['default']
var [x, y, fileName] = process.argv
var str = fs.readFileSync(fileName, 'utf8')
var data = JSON.parse(str)
var sim = []
var {latitude, longitude, altitude, nVelocity, eVelocity, dVelocity} = data.ownship.start
sim.push({tail: "OWNSHIP", latitude: latitude, longitude: longitude, altitude: altitude, velocity:{north: nVelocity, east: eVelocity, down: dVelocity}})
data.targets.map((p)=>p.start).forEach((plane)=>{
    var {tailNumber, latitude, longitude, altitude, nVelocity, eVelocity, dVelocity} = plane
    sim.push({tail: tailNumber, latitude: latitude, longitude: longitude, altitude: altitude, velocity:{north: nVelocity, east: eVelocity, down: dVelocity}})
})
// console.log(sim)
// console.log(toRadar(sim))
console.log(toRocket(sim))