import fs from 'fs'
var toRocket = require('./format/rocket.js')['default']
var [x, y, fileName] = process.argv
var str = fs.readFileSync(fileName, 'utf8')
var lines = str.split('\n')
var sim = []
lines.shift()
// lines.pop()
lines.forEach((l)=>{

    var fieldIndexes = ['tail', 'latitude', 'longitude', 'altitude', 'north', 'east', 'down']
    if (l.trim())
        var plane = {}
        var values = l.replace(/\t/g, ',').split(',')

        for (var i = 0; i < fieldIndexes.length && i < values.length;  ++i) {
            plane[fieldIndexes[i]] = values[i]
        }
        var {north, east, down} = plane
        plane.velocity = {'north':north, 'east':east, down:down}
        sim.push(plane)

})
console.log(toRocket(sim))