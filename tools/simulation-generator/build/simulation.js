'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = require('backbone');

var _backbone2 = _interopRequireDefault(_backbone);

var _aircraft = require('./aircraft.js');

var _aircraft2 = _interopRequireDefault(_aircraft);

var _position = require('./position.js');

var _position2 = _interopRequireDefault(_position);

var _velocityBuilder = require('./velocity-builder.js');

var _velocityBuilder2 = _interopRequireDefault(_velocityBuilder);

var _velocity = require('./velocity.js');

var _velocity2 = _interopRequireDefault(_velocity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var natMiPerDeg = 60;

function create_scenario(numberPlanes, regionSize, numberManeuvers) {
    var ownshipLocation = arguments.length <= 3 || arguments[3] === undefined ? { lat: 35.3050053, lng: -120.66249419999997 } : arguments[3];

    var aircraftList = new _backbone2.default.Collection([{ "tail": "OWNSHIP" }], {
        model: _aircraft2.default
    });
    var velBuilder = new _velocityBuilder2.default();
    var alt = _aircraft2.default.findRandomAltitude();
    var ownLat = ownshipLocation.lat;
    var ownLng = ownshipLocation.lng;

    var degress = regionSize / natMiPerDeg / 2;
    var northWestLimit = { lat: ownshipLocation.lat + degress, lng: ownshipLocation.lng - degress };
    var southEastLimit = { lat: ownshipLocation.lat - degress, lng: ownshipLocation.lng + degress };
    var lat = randomLat();
    var lng = randomLong();
    aircraftList.get('OWNSHIP').set('initialPosition', new _position2.default(ownLat, ownLng, alt));
    for (var x = 0; x < numberPlanes; x++) {
        var tail = _aircraft2.default.tailSequence();
        aircraftList.add({ tail: tail });
        var aircraft = aircraftList.get(tail);
        alt = _aircraft2.default.findRandomAltitude();
        lat = randomLat();
        lng = randomLong();
        velBuilder.setRandomBearing();
        velBuilder.setRandomPitch();
        velBuilder.setRandomSpeed();
        var velTmp = _velocityBuilder2.default.toNED(velBuilder.get('bearing'), velBuilder.get('pitch'), velBuilder.get('speed'));
        var vel = new _velocity2.default(velTmp.north, velTmp.east, velTmp.down);
        aircraft.set({
            'initialPosition': new _position2.default(lat, lng, alt),
            'initialVelocity': vel
        });
        var lastVelocity = vel.builder;
        for (var y = 0; y < numberManeuvers; ++y) {
            velBuilder.setRandomBearing();
            velBuilder.setRandomPitch();
            velBuilder.setRandomSpeed();
            velTmp = _velocityBuilder2.default.toNED(velBuilder.get('bearing'), velBuilder.get('pitch'), velBuilder.get('speed'));
            var nextVelocity = new _velocity2.default(velTmp.north, velTmp.east, velTmp.down);
            var nextVelocityBuilder = nextVelocity.builder;
            var duration = findDuration(lastVelocity.get('speed'), nextVelocityBuilder.get('speed'), lastVelocity.get('bearing'), nextVelocityBuilder.get('bearing'));
            var steps = Math.round(duration * 2);
            aircraft.maneuvers.add({ duration: duration, endVelocity: nextVelocity, steps: steps });
            lastVelocity = nextVelocityBuilder;
        }
    }

    var initialCheckpoints = [];
    var checkpoints = [];
    aircraftList.each(function (aircraft) {
        initialCheckpoints.push(aircraft.initialCheckpoint());
        var cp = aircraft.checkpoints();
        cp.shift();
        _underscore2.default.each(cp, function (checkpoint) {
            checkpoints.push(checkpoint);
        });
    });
    return { 'initial-conditions': initialCheckpoints, 'checkpoints': checkpoints };

    function randomLat() {
        var max = northWestLimit.lat;
        var min = southEastLimit.lat;
        return randRange(min, max);
    }

    function randomLong() {
        return randRange(southEastLimit.lng, northWestLimit.lng);
    }
}

function randRange(a, b) {
    var max = Math.max(a, b);
    var min = Math.min(a, b);
    var r = max - min;
    return Math.random() * r + min;
}

function findDuration(speedStart, speedEnd, bearingStart, bearingEnd) {
    var avgSpeed = (speedStart + speedEnd) / 2;
    var bStart = bearingStart;
    var bEnd = bearingEnd;

    var angleChangeRate = findRateOfAngleChange(avgSpeed);
    // console.log('angle rate of change', angleChangeRate, 'bstart', bStart, 'bend', bEnd)
    var deltaAngle = Math.min(distanceGoingClockwise(bStart, bEnd), distanceGoingCounterClockwise(bStart, bEnd));
    var result = Math.round(Math.max(2, deltaAngle / angleChangeRate * 3));
    // console.log('deltaAngle', deltaAngle, 'speed', avgSpeed, 'maneuver duration', result)
    return result;
    // return Math.random()*5 + 2
}

function distanceGoingClockwise(startBearing, endBearing) {
    if (endBearing >= startBearing) {
        return endBearing - startBearing;
    } else {
        endBearing += 360;
        return endBearing - startBearing;
    }
}

function distanceGoingCounterClockwise(startBearing, endBearing) {
    return 360 - distanceGoingClockwise(startBearing, endBearing);
}

function findRateOfAngleChange(speed) {
    return 90 * Math.pow(Math.E, -0.0023026 * speed);
}

exports.default = create_scenario;