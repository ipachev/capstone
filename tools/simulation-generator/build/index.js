'use strict';

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _simulation = require('./simulation');

var _simulation2 = _interopRequireDefault(_simulation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

Math.se;
var regionSizes = [5, 10, 20, 40, 80, 160, 320, 640];

var outDir = './generated';
var simsPerRegionSize = 4;
var minIntruders = 2;
var maxIntruders = 20;

var callback = 'json.js';
var tocsv = require('./format/csv.js')['default'];
var toRocket = require('./format/rocket.js')['default'];
var toRadar = require('./format/radar.js')['default'];

function makeScenariosOfSize(regionSize) {
    var callback = arguments.length <= 1 || arguments[1] === undefined ? callback : arguments[1];
    var minIntruders = arguments.length <= 2 || arguments[2] === undefined ? 2 : arguments[2];
    var maxIntruders = arguments.length <= 3 || arguments[3] === undefined ? 20 : arguments[3];
    var scenariosPerIntruderSize = arguments.length <= 4 || arguments[4] === undefined ? 4 : arguments[4];
    var outDir = arguments[5];

    for (var numIntruders = minIntruders; numIntruders <= maxIntruders; ++numIntruders) {
        for (var i = 0; i < scenariosPerIntruderSize; ++i) {
            var fileName = createFileName(regionSize, numIntruders);
            var sim = (0, _simulation2.default)(numIntruders, regionSize, 0)['initial-conditions'];
            var data = callback(sim);
            if (data !== undefined) {
                outputAScenario(fileName, data, outDir);
            }
            data = tocsv(sim);
            if (data !== undefined) {
                outputAScenario(fileName + '.csv', data, outDir);
            }
            data = toRadar(sim);
            if (data !== undefined) {
                outputAScenario(fileName + '.radar.json', data, outDir);
            }
            data = toRocket(sim);
            if (data !== undefined) {
                outputAScenario(fileName + '.rocket.xml', data, outDir);
            }
        }
    }
}

function outputAScenario(fileName, simdata, outputDir) {
    if (!_fs2.default.existsSync(outputDir)) {
        _fs2.default.mkdirSync(outputDir);
    }
    var outFile = _path2.default.resolve(outputDir, fileName);
    var fd = _fs2.default.openSync(outFile, 'w+');
    _fs2.default.writeSync(fd, simdata, 0);
}

var filesNames = {};
function createFileName(regionSize, numberIntruders) {
    var prefix = arguments.length <= 2 || arguments[2] === undefined ? "" : arguments[2];

    var propName = regionSize + '_' + numberIntruders;
    if (!_underscore2.default.has(filesNames, propName)) {
        filesNames[propName] = 1;
    }
    if (numberIntruders < 10) {
        numberIntruders = "0" + numberIntruders;
    }

    var result = prefix + numberIntruders + '@' + regionSize + 'x' + regionSize + "-" + filesNames[propName];
    filesNames[propName]++;
    return result;
}

var _process$argv = _toArray(process.argv);

var x = _process$argv[0];
var y = _process$argv[1];

var args = _process$argv.slice(2);

args.forEach(function (val, index, array) {
    if (val.match(/^outDir=/)) {
        outDir = val.replace(/^outDir=/, '');
    }
    if (val.match(/^simsPerRegionSize=/)) {
        simsPerRegionSize = Number(val.replace(/^simsPerRegionSize=/, ''));
    }
    if (val.match(/^minIntruders=/)) {
        minIntruders = Number(val.replace(/^minIntruders=/, ''));
    }
    if (val.match(/^maxIntruders=/)) {
        maxIntruders = Number(val.replace(/^maxIntruders=/, ''));
    }
    if (val.match(/^regionSizes=/)) {
        regionSizes = val.replace(/^regionSizes=/, '').split(/,/).map(function (x) {
            return Number(x);
        });
    }
    if (val.match(/^format=/)) {
        callback = val.replace(/^format=/, '');
    }
});

callback = require('./format/' + callback)['default'];

_underscore2.default.each(regionSizes, function (size) {
    return makeScenariosOfSize(size, callback, minIntruders, maxIntruders, simsPerRegionSize, outDir);
});