"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

require("babel-polyfill");

var _underscore = require("underscore");

var _ = _interopRequireWildcard(_underscore);

var _backbone = require("backbone");

var bb = _interopRequireWildcard(_backbone);

var _maneuver = require("./maneuver.js");

var _maneuver2 = _interopRequireDefault(_maneuver);

var _orientation = require("./orientation.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var ManeuverCollection = bb.Collection.extend({
    model: _maneuver2.default,
    buildNodes: function buildNodes(aircraft) {
        var tail = aircraft.get('tail');
        var currentTime = 0;
        var currentVelocity = aircraft.get('initialVelocity');

        var nodes = [];
        this.each(function (maneuver) {
            nodes = nodes.concat(maneuver.buildNodes(tail, currentTime, currentVelocity));
            currentVelocity = maneuver.get('endVelocity');
            currentTime += maneuver.get('duration');
        });
        // for (var i = 0; i < this.length; ++i) {
        //     var maneuver = this.at(i)
        //
        // }
        var orientation = _orientation.OrientationBuilder.fromVelocity(currentVelocity).build();
        var jsonVelocity = { "velocity": currentVelocity.toJSON() };
        var jsonOrientation = { "orientation": orientation.toJSON() };
        var finalNode = _.extend({ "tail": tail, "time": currentTime }, jsonVelocity, jsonOrientation);
        nodes = Array.prototype.concat(nodes, [finalNode]);
        return nodes;
    }
});

exports.default = ManeuverCollection;