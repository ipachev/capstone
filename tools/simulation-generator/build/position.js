"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.PositionBuilder = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _underscore = require("underscore");

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = require("backbone");

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _latitude = Symbol('latitude');
var _longitude = Symbol('longitude');
var _altitude = Symbol('altitude');

var PositionBuilder = exports.PositionBuilder = _backbone2.default.Model.extend({
    build: function build() {
        return new Velocity(this.get('latitude'), this.get('longitude'), this.get('altitude'));
    }
});

var Position = function () {
    function Position(latitude, longitude, altitude) {
        _classCallCheck(this, Position);

        this[_latitude] = latitude;
        this[_longitude] = longitude;
        this[_altitude] = altitude;
    }

    _createClass(Position, [{
        key: "toJSON",
        value: function toJSON() {
            var lat = this.latitude;
            var lng = this.longitude;
            var alt = this.altitude;
            return {
                "latitude": lat,
                "longitude": lng,
                "altitude": alt
            };
        }
    }, {
        key: "latitude",
        get: function get() {
            return this[_latitude];
        }
    }, {
        key: "longitude",
        get: function get() {
            return this[_longitude];
        }
    }, {
        key: "altitude",
        get: function get() {
            return this[_altitude];
        }
    }, {
        key: "builder",
        get: function get() {
            return new PositionBuilder({
                latitude: this.latitude,
                longitude: this.longitude,
                altitude: this.altitude
            });
        }
    }]);

    return Position;
}();

exports.default = Position;