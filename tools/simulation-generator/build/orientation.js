"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.OrientationBuilder = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.findOrientationFromVelocity = findOrientationFromVelocity;

var _underscore = require("underscore");

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = require("backbone");

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function toDegrees(angle) {
    return angle * (180 / Math.PI);
}

function validateOrientation(roll, pitch, yaw) {
    var values = [roll, pitch, yaw];
    var names = ["roll", "pitch", "yaw"];
    var invalidArgs = _underscore2.default.filter(_underscore2.default.map(_underscore2.default.zip(values, names), function (x) {
        var _x = _slicedToArray(x, 2);

        var val = _x[0];
        var name = _x[1];

        if (!_underscore2.default.isFinite(val)) {
            return "the " + name + " component is not valid";
        }
        return false;
    }), Boolean);

    if (invalidArgs.length) {
        return invalidArgs.join('\n');
    }
}

var OrientationBuilder = exports.OrientationBuilder = _backbone2.default.Model.extend({
    validate: validateOrientation,
    build: function build() {
        return new Orientation(this.get('roll'), this.get('pitch'), this.get('yaw'));
    }
}, {
    fromVelocity: function fromVelocity(velocity) {
        return findOrientationFromVelocity(velocity).builder;
    }
});

function findOrientationFromVelocity(velocity) {
    var n = velocity.north;
    var e = velocity.east;
    var horizontalSpeed = Math.sqrt(n * n + e * e);
    var yaw = toDegrees(Math.atan2(e, n));
    var pitch = toDegrees(-Math.atan2(velocity.down, horizontalSpeed));
    return new Orientation(0, pitch, yaw);
}

var _roll = Symbol('roll');
var _pitch = Symbol('pitch');
var _yaw = Symbol('yaw');

var Orientation = function () {
    function Orientation(roll, pitch, yaw) {
        _classCallCheck(this, Orientation);

        this[_roll] = roll;
        this[_pitch] = pitch;
        this[_yaw] = yaw;
    }

    _createClass(Orientation, [{
        key: "toJSON",
        value: function toJSON() {
            var r = this.roll;
            var p = this.pitch;
            var y = this.yaw;
            return {
                "roll": r,
                "pitch": p,
                "yaw": y
            };
        }
    }, {
        key: "roll",
        get: function get() {
            return this[_roll];
        }
    }, {
        key: "pitch",
        get: function get() {
            return this[_pitch];
        }
    }, {
        key: "yaw",
        get: function get() {
            return this[_yaw];
        }
    }, {
        key: "builder",
        get: function get() {
            return new OrientationBuilder({
                roll: this.roll,
                pitch: this.pitch,
                yaw: this.yaw
            });
        }
    }]);

    return Orientation;
}();

exports.default = Orientation;