'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (sim) {
    var _sim = _toArray(sim);

    var ownship = _sim[0];

    var intruders = _sim.slice(1);

    var result = start(intruders) + ownshipXML(ownship);
    for (var i = 0; i < intruders.length; ++i) {
        var plane = intruders[i];
        plane.north = deltaDegToNmi(plane.latitude - ownship.latitude);
        plane.east = deltaDegToNmi(plane.longitude - ownship.longitude);
        plane.down = -1 / 6076.12 * (plane.altitude - ownship.altitude);
        result += intruderXML(plane);
    }
    result += '</test>\n';
    return result;
};

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

function start(intruders) {
    return '<test name="test team test"\n      description="test from test team"\n      time = "120"\n      sensorError = "false">\n';
}
function intruderXML(intruder) {
    return '<plane tag="' + intruder.tail + '">\n      <ads-b enabled="true"/>\n      <tcas enabled="true"/>\n      <radar enabled="true"/>\n      <movement type="linear">\n         <position x="' + intruder.north + '" y="' + intruder.east + '" z="' + intruder.down + '"/> <!--NED in Nautical Miles-->\n         <direction x="' + intruder.velocity.north + '" y="' + intruder.velocity.east + '" z="' + intruder.velocity.down + '"/> <!--Feet/second-->\n      </movement>\n   </plane>\n';
}

function ownshipXML(ownship) {
    var lat = ownship.latitude;
    var lng = ownship.longitude;
    var alt = ownship.altitude;
    var _ownship$velocity = ownship.velocity;
    var n = _ownship$velocity.north;
    var e = _ownship$velocity.east;
    var d = _ownship$velocity.down;

    return '<ownship tag="ABC123">\n      <tcas enabled="true" error="1.5"/>\n      <radar enabled="true"/>\n      <ads-b enabled="true" error="0.2"/>\n      <movement type="linear">\n         <position x="0" y="0" z="0"/>\n         <direction x="' + ownship.velocity.north + '" y="' + ownship.velocity.east + '" z="' + ownship.velocity.down + '"/>\n      </movement>\n   </ownship>\n';
}

function deltaDegToNmi(degrees) {
    return degrees * 60;
}