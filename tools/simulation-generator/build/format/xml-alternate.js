'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (simulation) {
    var _simulation = _toArray(simulation);

    var ownship = _simulation[0];

    var intruders = _simulation.slice(1);

    return ownshipToXML(ownship) + "\n" + intruders.map(intruderToXML).join('\n');
};

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

var xml = require('xml');

function ownshipToXML(ownship) {
    var _ownship = ownship;
    var latitude = _ownship.latitude;
    var longitude = _ownship.longitude;
    var altitude = _ownship.altitude;
    var _ownship$velocity = ownship.velocity;
    var north = _ownship$velocity.north;
    var east = _ownship$velocity.east;
    var down = _ownship$velocity.down;

    ownship = {
        ownship: [{ latitude: latitude }, { longitude: longitude }, { altitude: altitude }, { north: north }, { east: east }, { down: down }]
    };
    return xml([ownship], true);
}

function intruderToXML(intruder) {
    var _intruder = intruder;
    var tail = _intruder.tail;
    var lat = _intruder.latitude;
    var lng = _intruder.longitude;
    var alt = _intruder.altitude;
    var _intruder$velocity = _intruder.velocity;
    var n = _intruder$velocity.north;
    var e = _intruder$velocity.east;
    var d = _intruder$velocity.down;

    intruder = {
        intruder: [{ tail: tail }, { latitude: lat }, { longitude: lng }, { altitude: alt }, { north: n }, { east: e }, { down: d }]
    };
    return xml([intruder], true);
}