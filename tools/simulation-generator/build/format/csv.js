"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (sim) {
    var result = "tail, latitude, longitude, altitude, north, east, down\n";
    for (var i = 0; i < sim.length; ++i) {
        var plane = sim[i];
        var line = plane.tail + ", " + plane.latitude + ", " + plane.longitude + ", " + plane.altitude + ", " + plane.velocity.north + ", " + plane.velocity.east + ", " + plane.velocity.down + "\n";
        result += line;
    }
    return result;
};