"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (simulation) {
    return JSON.stringify(simulation, null, 3);
};