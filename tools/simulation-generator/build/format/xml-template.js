"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (sim) {
    var _sim = _toArray(sim);

    var ownship = _sim[0];

    var intruders = _sim.slice(1);

    var result = ownshipXML(ownship);
    for (var i = 0; i < intruders.length; ++i) {
        result += intruderXML(intruders[i]);
    }
    return result;
};

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

function intruderXML(intruder) {
    return "<intruder>\n    <tail>" + intruder.tail + "</tail>\n    <latitude>" + intruder.latitude + "</latitude>\n    <longitude>" + intruder.longitude + "</longitude>\n    <altitude>" + intruder.altitude + "</altitude>\n    <north>" + intruder.velocity.north + "</north>\n    <east>" + intruder.velocity.east + "</east>\n    <down>" + intruder.velocity.down + "</down>\n</intruder>\n";
}

function ownshipXML(ownship) {
    var lat = ownship.latitude;
    var lng = ownship.longitude;
    var alt = ownship.altitude;
    var _ownship$velocity = ownship.velocity;
    var n = _ownship$velocity.north;
    var e = _ownship$velocity.east;
    var d = _ownship$velocity.down;

    return "\n<ownship>\n    <latitude>" + lat + "</latitude>\n    <longitude>" + lng + "</longitude>\n    <altitude>" + alt + "</altitude>\n    <north>" + n + "</north>\n    <east>" + e + "</east>\n    <down>" + d + "</down>\n</ownship>\n";
}