"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (simulation) {
    var result = { 'initial-conditions': simulation, "checkpoints": [] };
    return JSON.stringify(result, null, 3);
};