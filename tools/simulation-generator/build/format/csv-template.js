"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (sim) {
    var result = "lat, lng, alt\n";
    for (var i = 0; i < sim.length; ++i) {
        var plane = sim[i];
        var line = plane.latitude + ", " + plane.longitude + ", " + plane.altitude + "\n";
        result += line;
    }
    return result;
};