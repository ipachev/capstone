'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

var _velocityBuilder = require('./velocity-builder.js');

var _velocityBuilder2 = _interopRequireDefault(_velocityBuilder);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _north = Symbol('north');
var _east = Symbol('east');
var _down = Symbol('down');

var Velocity = function () {
    function Velocity() {
        var north = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];
        var east = arguments.length <= 1 || arguments[1] === undefined ? 0 : arguments[1];
        var down = arguments.length <= 2 || arguments[2] === undefined ? 0 : arguments[2];

        _classCallCheck(this, Velocity);

        var err = this.validateArgs(north, east, down);
        if (err) {
            throw err;
        }
        this[_north] = north;
        this[_east] = east;
        this[_down] = down;
    }

    _createClass(Velocity, [{
        key: 'validateArgs',
        value: function validateArgs(north, east, down) {
            var result = {};
            var arr = _underscore2.default.zip(["north", "east", "down"], [north, east, down]);
            arr = _underscore2.default.filter(arr, function (x) {
                return !_underscore2.default.isFinite(x[1]);
            });
            _underscore2.default.each(arr, function (x) {
                var _x4 = _slicedToArray(x, 2);

                var name = _x4[0];
                var val = _x4[1];

                result[name] = val;
            });
            if (!_underscore2.default.isEmpty(result)) {
                return result;
            }
        }
    }, {
        key: 'toJSON',
        value: function toJSON() {
            var n = this.north;
            var e = this.east;
            var d = this.down;
            return {
                "north": n,
                "east": e,
                "down": d
            };
        }
    }, {
        key: 'north',
        get: function get() {
            return this[_north];
        }
    }, {
        key: 'east',
        get: function get() {
            return this[_east];
        }
    }, {
        key: 'down',
        get: function get() {
            return this[_down];
        }
    }, {
        key: 'speed',
        get: function get() {
            var n = this.north;
            var e = this.east;
            var d = this.down;
            return Math.sqrt(n * n + e * e + d * d);
        }
    }, {
        key: 'builder',
        get: function get() {
            var result = new _velocityBuilder2.default({
                north: this.north,
                east: this.east,
                down: this.down
            });
            return result;
        }
    }]);

    return Velocity;
}();

exports.default = Velocity;