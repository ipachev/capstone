'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var toRocket = require('./format/rocket.js')['default'];

var _process$argv = _slicedToArray(process.argv, 3);

var x = _process$argv[0];
var y = _process$argv[1];
var fileName = _process$argv[2];

var str = _fs2.default.readFileSync(fileName, 'utf8');
var lines = str.split('\n');
var sim = [];
lines.shift();
// lines.pop()
lines.forEach(function (l) {

    var fieldIndexes = ['tail', 'latitude', 'longitude', 'altitude', 'north', 'east', 'down'];
    if (l.trim()) var plane = {};
    var values = l.replace(/\t/g, ',').split(',');

    for (var i = 0; i < fieldIndexes.length && i < values.length; ++i) {
        plane[fieldIndexes[i]] = values[i];
    }
    var north = plane.north;
    var east = plane.east;
    var down = plane.down;

    plane.velocity = { 'north': north, 'east': east, down: down };
    sim.push(plane);
});
console.log(toRocket(sim));