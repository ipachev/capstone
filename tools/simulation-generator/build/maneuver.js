"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

require("babel-polyfill");

var _underscore = require("underscore");

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = require("backbone");

var _backbone2 = _interopRequireDefault(_backbone);

var _velocity = require("./velocity.js");

var _velocity2 = _interopRequireDefault(_velocity);

var _velocityBuilder = require("./velocity-builder");

var _velocityBuilder2 = _interopRequireDefault(_velocityBuilder);

var _orientation = require("./orientation.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function distanceGoingClockwise(startBearing, endBearing) {
    if (endBearing >= startBearing) {
        return endBearing - startBearing;
    } else {
        endBearing += 360;
        return endBearing - startBearing;
    }
}

function distanceGoingCounterClockwise(startBearing, endBearing) {
    return 360 - distanceGoingClockwise(startBearing, endBearing);
}

var Maneuver = _backbone2.default.Model.extend({
    defaults: {
        duration: 5,
        endVelocity: new _velocity2.default(1, 0, 0),
        steps: 10
    },

    initialize: function initialize(attributes, options) {
        if (!'start_time' in attributes) {
            attributes['start_time'] = this.collection.next_maneuver_time();
        }
    },

    buildNodes: function buildNodes(tail, startTime, startVelocity) {
        var steps = this.get('steps');
        var endVelocity = this.get('endVelocity');

        var times = Maneuver.linSpace(startTime, startTime + this.get('duration'), steps);

        var initVelB = startVelocity.builder;
        var endVelB = this.get('endVelocity').builder;

        var bStart = initVelB.get('bearing');
        var bEnd = endVelB.get('bearing');

        var bearings;
        // shorter to go clockwise
        if (distanceGoingClockwise(bStart, bEnd) < distanceGoingCounterClockwise(bStart, bEnd)) {
            if (bEnd < bStart) {
                bEnd += 360;
            }
            bearings = Maneuver.linSpace(bStart, bEnd, steps);
        } else {
            if (bStart < bEnd) {
                bStart += 360;
            }
            bearings = Maneuver.linSpace(bStart, bEnd, steps);
        }

        var pitches = Maneuver.linSpace(initVelB.get('pitch'), endVelB.get('pitch'), steps);
        var speeds = Maneuver.linSpace(initVelB.get('speed'), endVelB.get('speed'), steps);

        var velocities = _underscore2.default.map(_underscore2.default.zip(bearings, pitches, speeds), function (data) {
            var _data = _slicedToArray(data, 3);

            var b = _data[0];
            var p = _data[1];
            var s = _data[2];

            var _VelocityBuilder$toNE = _velocityBuilder2.default.toNED(b % 360, p, s);

            var n = _VelocityBuilder$toNE.north;
            var e = _VelocityBuilder$toNE.east;
            var d = _VelocityBuilder$toNE.down;

            var v = new _velocity2.default(n, e, d);
            return v;
        });

        var orientations = _underscore2.default.map(velocities, function (v) {
            return _orientation.OrientationBuilder.fromVelocity(v).build();
        });

        var jsonData = _underscore2.default.map(_underscore2.default.zip(times, velocities, orientations), function (data) {
            var _data2 = _slicedToArray(data, 3);

            var time = _data2[0];
            var velocity = _data2[1];
            var orientation = _data2[2];

            var jsonVelocity = { "velocity": velocity.toJSON() };
            var jsonOrientation = { "orientation": orientation.toJSON() };
            return _underscore2.default.extend({ "tail": tail, "time": time }, jsonVelocity, jsonOrientation);
        });
        return jsonData;
    }
}, {
    linSpace: function linSpace(start, end, steps) {
        if (typeof steps === "undefined") {
            steps = Math.max(Math.round(end - start) + 1, 1);
        }

        if (steps < 2) {
            return steps === 1 ? [start] : [];
        }

        var result = Array(steps);
        for (var i = --steps; i >= 0; --i) {
            result[i] = (i * end + (steps - i) * start) / steps;
        }
        return result;
    }
});

exports.default = Maneuver;