"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _underscore = require("underscore");

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = require("backbone");

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PI = Math.PI;
var sin = Math.sin;
var cos = Math.cos;
var sqrt = Math.sqrt;
var atan = Math.atan;
var atan2 = Math.atan2;
var random = Math.random;
var floor = Math.floor;

var RADIANS_PER_DEGREE = PI / 180;
var radians = function radians(d) {
    return d * RADIANS_PER_DEGREE;
};
var degrees = function degrees(r) {
    return r / RADIANS_PER_DEGREE;
};
var randomIntInclusive = function randomIntInclusive(min, max) {
    return floor(random() * (max - min + 1)) + min;
};

var staticMethods = {
    toNED: function toNED(bearing, pitch, speed) {
        bearing = radians(bearing);
        pitch = radians(pitch);
        var n = cos(bearing) * cos(pitch) * speed;
        var e = sin(bearing) * cos(pitch) * speed;
        var d = -sin(pitch) * speed;
        return { "north": n, "east": e, "down": d };
    },

    fromNED: function fromNED(n, e, d) {
        var speed = sqrt(n * n + e * e + d * d);
        var longitudinalSpeed = sqrt(n * n + e * e);
        var pitch = -degrees(atan(d / longitudinalSpeed));
        var bearing = degrees(atan2(e, n));
        if (bearing < 0) {
            bearing = 360 + bearing;
        }
        return { 'bearing': bearing, 'pitch': pitch, 'speed': speed };
    },

    parseNEDString: function parseNEDString(raw) {
        return _underscore2.default.map(raw.replace(/,/g, ' ').trim().split(/\s+/), function (x) {
            return Number(x);
        });
    },

    findRandomSpeed: function findRandomSpeed() {
        return randomIntInclusive(120 / 2.5, 870 / 2.5) * 2.5;
    },
    findRandomBearing: function findRandomBearing() {
        return randomIntInclusive(0 / 0.25, 360 / 0.25) * 0.25;
    },
    findRandomPitch: function findRandomPitch() {
        return randomIntInclusive(-90 / 0.1, 90 / 0.1) * 0.1;
    }
};

exports.default = _backbone2.default.Model.extend({
    defaults: {
        'bearing': 0,
        'pitch': 0,
        'speed': 0
    },

    initialize: function initialize() {
        if (this.has('north') && this.has('east') && this.has('down')) {
            var data = staticMethods.fromNED(this.get('north'), this.get('east'), this.get('down'));
            this.set(data);
        }
    },

    validate: function validate(attributes, options) {
        var bearing = attributes.bearing;
        var pitch = attributes.pitch;
        var speed = attributes.speed;

        var errors = {
            'bearing': _underscore2.default.isFinite(bearing) && bearing < 360 && 0 <= bearing,
            'pitch': _underscore2.default.isFinite(pitch) && pitch <= 90 && -90 <= bearing,
            'speed': _underscore2.default.isFinite(speed) && 0 <= speed
        };
        if (_underscore2.default.some(_underscore2.default.values(errors), function (x) {
            return x;
        })) {
            return errors;
        }
    },

    build: function build() {
        var _toNED = this.toNED();

        var _toNED2 = _slicedToArray(_toNED, 3);

        var n = _toNED2[0];
        var e = _toNED2[1];
        var d = _toNED2[2];

        return new Velocity(n, e, d);
    },

    setRandomSpeed: function setRandomSpeed() {
        this.set('speed', staticMethods.findRandomSpeed());
    },

    setRandomBearing: function setRandomBearing() {
        this.set('bearing', staticMethods.findRandomBearing());
    },

    setRandomPitch: function setRandomPitch() {
        this.set('pitch', staticMethods.findRandomPitch());
    }
}, staticMethods);