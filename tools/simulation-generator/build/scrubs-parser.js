'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var toRocket = require('./format/rocket.js')['default'];
var toRadar = require('./format/radar.js')['default'];

var _process$argv = _slicedToArray(process.argv, 3);

var x = _process$argv[0];
var y = _process$argv[1];
var fileName = _process$argv[2];

var str = _fs2.default.readFileSync(fileName, 'utf8');
var data = JSON.parse(str);
var sim = [];
var _data$ownship$start = data.ownship.start;
var latitude = _data$ownship$start.latitude;
var longitude = _data$ownship$start.longitude;
var altitude = _data$ownship$start.altitude;
var nVelocity = _data$ownship$start.nVelocity;
var eVelocity = _data$ownship$start.eVelocity;
var dVelocity = _data$ownship$start.dVelocity;

sim.push({ tail: "OWNSHIP", latitude: latitude, longitude: longitude, altitude: altitude, velocity: { north: nVelocity, east: eVelocity, down: dVelocity } });
data.targets.map(function (p) {
    return p.start;
}).forEach(function (plane) {
    var tailNumber = plane.tailNumber;
    var latitude = plane.latitude;
    var longitude = plane.longitude;
    var altitude = plane.altitude;
    var nVelocity = plane.nVelocity;
    var eVelocity = plane.eVelocity;
    var dVelocity = plane.dVelocity;

    sim.push({ tail: tailNumber, latitude: latitude, longitude: longitude, altitude: altitude, velocity: { north: nVelocity, east: eVelocity, down: dVelocity } });
});
// console.log(sim)
// console.log(toRadar(sim))
console.log(toRocket(sim));