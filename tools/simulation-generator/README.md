# Simulation Generator #
A tool to randomly generate aircraft simulations

### How do I get set up? ###

* Install [NodeJs](https://nodejs.org/en/download/)
* Clone this repo
* cd into the project
* Install project dependencies: `npm install`
* Build the project: `npm run build`
* Run the project: `npm run generate`
* See generated simulations in the `./generated` directory

## Command Line Usage
This example shows all possible command line arguments with their default values:

    npm run generate \
       regionSizes=5,10,20,40,80,160,320,640 \
       minIntruders=2 \
       maxIntruders=20 \
       simsPerRegionSize=4 \
       outDir=./generated \
       format=json.js

All parameters except the `outDir` and `format` parameters tell the program *what* to generate and *how many*. Here is Pseudocode of what the program does:

    for regionSize in regionSizes:
        for numIntruders in [minIntruders, minIntruders + 1, ..., maxIntruders]:
            for i in [1, 2, 3, ..., simsPerRegion]:
               create a simulation, call it sim, in a square region of size regionSize by regionSize nautical miles that has intruders randomly placed throughout.
               format sim into a string called data.
               if data:
                  write data to a file named "<numIntruders>@<regionSize>x<regionSize>-<i>" in outDir
Using its default values, the tool will create 608 simulations. This is because 608 equals regionSizes.length=8 * simsPerRegionSize=4 * maxIntruders-minIntruders+1=19.
Each simulation gets written to a file named using this convention:

    <number_planes>@<regionSize>x<regionSize>-<number>


#### Example
You want 1000 simulations with 50 intruders pacted into 25 square nautical miles:

    npm run generate \
       regionSizes=5 \
       minIntruders=50 \
       maxIntruders=50 \
       simsPerRegionSize=1000

#### Program Arguments
`regionSizes` list each regionSize. A regionSize specifies the dimension of a square in nautical miles. This square is centered around the ownship and generated aircraft can only be found these bounds. DO NOT put spaces between the numbers; only commas.

`minIntruders` and `maxIntruders` define how many intruders are going to be in the simulations this tool generates.

`simsPerRegionSize` is the number of simulations to create for each combination of regionSize and some number of intruders

`outDir` is the directory to write simulations in.

`format` the JavaScript file in the `src/format` directory that contains the desired format function. This is described more in the next section.

## A config that Generates Only One
You can use `npm run generate-one` as a shortcut for:

    npm run generate \
        simsPerRegionSize=1 \
        regionSizes=5 \
        minIntruders=1 \
        maxIntruders=1


## Guide to Creating Output Formats
The `format=file` argument specifies a JavaScript file in the `src/format` directory. This file must `export default` a JavaScript function.

This function is passed one parameter: an `Array` of plain old JavaScript Objects (POJSOs). Each object in that array describes the initial conditions of one aircraft in the simulation. The function is expected to either not return a value or to return a string that formats this data for a particular simulator.

Here is the simplest possible example of whats expected:

    export default function(sim) {

    }


 The array argument holds all the initial conditions for a simulation. The first object in this array contains the initial conditions for ownship. Here is an example value of ownship:

    {
      "tail": "OWNSHIP",
      "latitude": 35.3050053,
      "longitude": -120.66249419999997,
      "altitude": 15000,
      "velocity": {
         "north": 1,
         "east": 0,
         "down": 0
      }
    }

The remaining objects contain initial conditions for intruders. Here is an example:

    {
      "tail": "S61LT",
      "latitude": 35.27520511164097,
      "longitude": -120.66461329327963,
      "altitude": 16750,
      "velocity": {
         "north": -214.8421392846718,
         "east": 116.64976404712753,
         "down": 722.2478367813469
      },
      "sensors": {
         "adsb": true,
         "radar": true,
         "tcas": true
      }
    }

This is an example of how to separate the ownship from the intruders:

    export default function(sim) {
        var [ownship, ...intruders] = sim

        console.log("ownship reference (of type Object):")
        console.log(ownship)
        console.log("intruders reference (of type Array):")
        console.log(intruders)
    }

if you put this into the file `src/format/example.js`, then you could test by entering this command:

    npm -s run generate-one minIntruders=2 maxIntruders=2 format=example.js

If you did that, you should expect console output like the following:

    ownship reference (of type Object):
    { tail: 'OWNSHIP',
      latitude: 35.3050053,
      longitude: -120.66249419999997,
      altitude: 38750,
      velocity: { north: 1, east: 0, down: 0 } }


    intruders reference (of type Array):
    [ { tail: 'R44SI',
        latitude: 35.29249602042604,
        longitude: -120.65545321644895,
        altitude: 32000,
        velocity:
         { north: -96.69329295729011,
           east: 58.67493054019484,
           down: 683.2012219128263 },
        sensors: { adsb: true, radar: true, tcas: true } },
      { tail: 'S39XU',
        latitude: 35.28913295904019,
        longitude: -120.64087633252055,
        altitude: 5750,
        velocity:
         { north: 648.2428988575408,
           east: 239.14943128668244,
           down: 382.9995999946063 },
        sensors: { adsb: true, radar: true, tcas: true } } ]

This output shows all the data for a single simulation that was generated with two intruders. Note: `npm run generate-one` is included to make development easier.

The tool is designed to make generating hundreds of simulations as easy as possible. The tool expects our function to return a `String` and every time we return a string, a file containing that string get created in the `outDir=` (i.e. `./generated`).

Template syntax is one way of creating a formatted string. The following example shows how a `.csv` could be programmed with template syntax:

    export default function(sim) {
        var result = "lat, lng, alt\n"
        for (var i = 0; i < sim.length; ++i) {
            let plane = sim[i]
            var line = `${plane.latitude}, ${plane.longitude}, ${plane.altitude}\n`
            result += line
        }
        return result
    }

And this longer example puts simulation data into an `.xml` format using template syntax:

    function intruderXML(intruder) {
        return `<intruder>
        <tail>${intruder.tail}</tail>
        <latitude>${intruder.latitude}</latitude>
        <longitude>${intruder.longitude}</longitude>
        <altitude>${intruder.altitude}</altitude>
        <north>${intruder.velocity.north}</north>
        <east>${intruder.velocity.east}</east>
        <down>${intruder.velocity.down}</down>
    </intruder>
    `
    }


    function ownshipXML(ownship) {
        var {latitude:lat, longitude:lng, altitude:alt, velocity:{north:n, east:e, down:d}} = ownship
        return `
    <ownship>
        <latitude>${lat}</latitude>
        <longitude>${lng}</longitude>
        <altitude>${alt}</altitude>
        <north>${n}</north>
        <east>${e}</east>
        <down>${d}</down>
    </ownship>
    `
    }

    export default function(sim) {
        var [ownship, ...intruders] = sim
        var result = ownshipXML(ownship)
        for (var i = 0; i < intruders.length; ++i) {
            result += intruderXML(intruders[i])
        }
        return result
    }

Once you are happy with your format function, you can create simulations in bulk using:

    npm run generate format=<name-of-my-file.js>

### Tips for Developing ###
* Don't manually build and run the project over and over. Open another terminal and auto build the project when a file changes using `npm run watch`. Check the output for build errors.
* If you use [WebStorm](https://www.jetbrains.com/webstorm/) to make changes, open preferences and set the `Javascript language version` to `EMCAScript 6`. Also if WebStorm asks you to setup a babel file watcher, click dismiss.